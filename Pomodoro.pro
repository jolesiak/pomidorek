TEMPLATE = app

# The directory with the executable file and the name of this file
TARGET = Pomodoro
DESTDIR = bin
RCC_DIR = bin

QMAKE_CXXFLAGS += -std=c++0x -I/usr/include/python2.7
QMAKE_LIBS += -lpython2.7
QT += core gui xml #phonon

# Paths' definition
SRC = src/
ENGINE = $$SRC/engine
DATABASE = $$SRC/database
STATS = $$SRC/statistics
UI = $$SRC/ui
MAINWINDOW = $$UI/MainWindow
NEW_TASK_DIALOG = $$UI/NewTaskDialog
SETTINGS_WINDOW = $$UI/SettingsWindow

RESOURCES += $$UI/resources/app.qrc

HEADERS += $$DATABASE/database.h \
    $$DATABASE/structs.h \
    $$DATABASE/data.h \
    $$DATABASE/file_operations.h \
    $$ENGINE/basic_functions.h \
    $$ENGINE/google_drive/drive.h \
    $$MAINWINDOW/mainwindow.h \
    $$MAINWINDOW/mainwindownamespace.h \
    $$MAINWINDOW/task_timer/tasktimerwidget.h \
    $$MAINWINDOW/task_info/taskinfowidget.h \
    $$MAINWINDOW/tasks_list/tasklistwidget.h \
    $$MAINWINDOW/tasks_list/tasklistitem.h \
    $$MAINWINDOW/tasktabwidget.h \
    $$MAINWINDOW/tasks_list/tasksuperlistwidget.h \
    $$MAINWINDOW/task_table_widget/tasktablewidget.h \
    $$MAINWINDOW/task_table_model/tasktablemodel.h \
    $$MAINWINDOW/task_proxy_model/taskproxymodel.h \
    $$MAINWINDOW/tag_widget/tagwidget.h \
    $$NEW_TASK_DIALOG/dialognewtask.h \
    $$SETTINGS_WINDOW/settings.h \
    $$SETTINGS_WINDOW/managetagdialog.h \
    $$SETTINGS_WINDOW/gd_prompt_no_buttons.h \
    $$SETTINGS_WINDOW/gd_input_dialog.h \
    $$SETTINGS_WINDOW/gd_thread.h \
    $$SETTINGS_WINDOW/namespaces.h \
    $$MAINWINDOW/edittaskdialog.h \
    $$MAINWINDOW/all_task_table/alltasktable.h \
    $$MAINWINDOW/deleted_task_table/deletedtasktable.h \
    $$MAINWINDOW/active_task_table/activetasktable.h \
    $$MAINWINDOW/finished_task_table/finishedtasktable.h \
    $$MAINWINDOW/ctrlWidget/ctrlwidget.h \
    $$MAINWINDOW/selecttaskdialog.h \
    $$MAINWINDOW/spinboxdelegate.h \
    src/ui/AboutWindow/aboutwindow.h

SOURCES += $$DATABASE/database.cpp \
    $$DATABASE/file_operations.cpp \
    $$ENGINE/basic_functions.cpp \
    $$ENGINE/google_drive/drive.cpp \
    $$MAINWINDOW/mainwindow.cpp \
    $$MAINWINDOW/task_timer/tasktimerwidget.cpp \
    $$MAINWINDOW/task_info/taskinfowidget.cpp \
    $$MAINWINDOW/tasks_list/tasklistwidget.cpp \
    $$MAINWINDOW/tasks_list/tasklistitem.cpp \
    $$MAINWINDOW/tasktabwidget.cpp \
    $$MAINWINDOW/tasks_list/tasksuperlistwidget.cpp \
    $$MAINWINDOW/task_table_widget/tasktablewidget.cpp \
    $$MAINWINDOW/task_table_model/tasktablemodel.cpp \
    $$MAINWINDOW/task_proxy_model/taskproxymodel.cpp \
    $$MAINWINDOW/tag_widget/tagwidget.cpp \
    $$NEW_TASK_DIALOG/dialognewtask.cpp \
    $$SETTINGS_WINDOW/settings.cpp \
    $$SETTINGS_WINDOW/managetagdialog.cpp \
    $$SETTINGS_WINDOW/gd_prompt_no_buttons.cpp \
    $$SETTINGS_WINDOW/gd_input_dialog.cpp \
    $$SETTINGS_WINDOW/gd_thread.cpp \
    $$SRC/main.cpp \
    $$MAINWINDOW/edittaskdialog.cpp \
    $$MAINWINDOW/all_task_table/alltasktable.cpp \
    $$MAINWINDOW/deleted_task_table/deletedtasktable.cpp \
    $$MAINWINDOW/active_task_table/activetasktable.cpp \
    $$MAINWINDOW/finished_task_table/finishedtasktable.cpp \
    $$MAINWINDOW/ctrlWidget/ctrlwidget.cpp \
    $$MAINWINDOW/selecttaskdialog.cpp \
    $$MAINWINDOW/spinboxdelegate.cpp \
    src/ui/AboutWindow/aboutwindow.cpp

FORMS += $$MAINWINDOW/mainwindow.ui \
    $$MAINWINDOW/tasks_list/TaskListItem.ui \
    $$MAINWINDOW/tasks_list/TaskListHeader.ui \
    $$MAINWINDOW/task_info/TaskInfoDisplayForm.ui \
    $$MAINWINDOW/task_timer/TaskTimerForm.ui \
    $$MAINWINDOW/tasktabwidget.ui \
    $$MAINWINDOW/tasks_list/tasksuperlistwidget.ui \
    $$MAINWINDOW/task_table_widget/tasktablewidget.ui \
    $$MAINWINDOW/tag_widget/tagwidget.ui \
    $$NEW_TASK_DIALOG/dialognewtask.ui \
    $$SETTINGS_WINDOW/settings.ui \
    $$SETTINGS_WINDOW/managetagdialog.ui \
    $$SETTINGS_WINDOW/gd_prompt_no_buttons.ui \
    $$SETTINGS_WINDOW/gd_input_dialog.ui \
    $$MAINWINDOW/edittaskdialog.ui \
    $$MAINWINDOW/ctrlWidget/ctrlwidget.ui \
    $$MAINWINDOW/selecttaskdialog.ui \
    src/ui/AboutWindow/aboutwindow.ui

# The directory with all headers generated from forms
UI_DIR = $$UI/ui_headers

HEADERS_DIRS = $$ENGINE \
    $$MAINWINDOW \
    $$NEW_TASK_DIALOG \
    $$SETTINGS_WINDOW \
    $$DATABASE \

# Paths to include
INCLUDEPATH += UI_DIR $$HEADERS_DIRS
