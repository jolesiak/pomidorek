#include "aboutwindow.h"
#include "ui_aboutwindow.h"
#include <QPixmap>


AboutWindow::AboutWindow(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::AboutWindow)
{
  ui->setupUi(this);
  QPixmap item(":/logo");
  ui->logoLabel->setScaledContents(true);
  ui->logoLabel->setPixmap(item);

}

AboutWindow::~AboutWindow()
{
  delete ui;
}
