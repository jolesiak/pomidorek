#ifndef DIALOGNEWTASK_H
#define DIALOGNEWTASK_H

#include "ui_dialognewtask.h"
#include "database.h"
#include <QStandardItemModel>

class DialogNewTask : public QDialog, private Ui::DialogNewTask
{
  Q_OBJECT
  
public:
  explicit DialogNewTask(QWidget *parent = 0);

private:
  void setDefaultDateTime();

  enum ItemType {
      listItemT,
      comboBoxItemT
  };

  QStandardItem * makeItem(const db::Tag& tag, ItemType itype);

  QMap<db::ID, QStandardItem* > listItems;
  QMap<db::ID, QStandardItem* > cbItems;

  db::Database db;

  QStandardItemModel * tagListModel;
  QStandardItemModel * mainTagModel;



signals:
  void sendNewTask(db::Task &);

private slots:
  void createTask();
  void slotChanged(const QModelIndex& topLeft, const QModelIndex /* &bottomRight */);
};

#endif // DIALOGNEWTASK_H
