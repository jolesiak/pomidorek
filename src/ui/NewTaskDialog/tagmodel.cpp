#include "tagmodel.h"

#include <QStandardItemModel>

TagModel::TagModel()
{
}

Qt::ItemFlags TagModel::flags( const QModelIndex & index) const
{
    Qt::ItemFlags result = QStandardItemModel::flags(index);
    if(index.column() == 1)
        result |= Qt::ItemIsUserCheckable;
    return result;
}
