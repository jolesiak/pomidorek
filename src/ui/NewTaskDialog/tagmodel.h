#ifndef TAGMODEL_H
#define TAGMODEL_H

#include <QStandardItemModel>

class TagModel : public QStandardItemModel
{
public:
    TagModel();
    virtual Qt::ItemFlags flags( const QModelIndex & index) const;
};
#endif // TAGMODEL_H
