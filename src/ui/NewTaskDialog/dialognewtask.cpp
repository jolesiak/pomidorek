#include <QDateTime>
#include <QDebug>
#include <QStandardItemModel>

#include "dialognewtask.h"
#include "database.h"
#include "tagmodel.h"
#include <QListView>
#include <QSharedPointer>
#include "basic_functions.h"

DialogNewTask::DialogNewTask(QWidget *parent) : QDialog(parent) {
  setupUi(this);

  setDefaultDateTime();
  QList<db::Tag> tl = db.TagList();

  int i=0;

  tagListModel = new QStandardItemModel();
  mainTagModel = new QStandardItemModel();

  QStandardItem * no_mt = new QStandardItem("<SELECT MAIN TAG>");
  no_mt->setData(db.IDToQVariant(db::NO_ID), Qt::UserRole+1);
  mainTagModel->insertRow(0, no_mt);

  connect(tagListModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
          this, SLOT(slotChanged(QModelIndex,QModelIndex)));

  /* tag list */
  foreach(const db::Tag tag, tl)
  {
      QStandardItem * item = makeItem(tag, listItemT);
      listItems.insert(tag.id, item);
      tagListModel->insertRow(i++, item);
  }

  mainTagComboBox->setModel(mainTagModel);
  listView->setModel(tagListModel);

  connect(buttonsBox, SIGNAL(accepted()), this, SLOT(createTask()));
}

/* Set default values for the time field. */
void DialogNewTask::setDefaultDateTime() {
  editDateTime->setMinimumDateTime(QDateTime::currentDateTime());
  editDateTime->setDateTime(QDateTime::currentDateTime().addDays(1));
}

QStandardItem * DialogNewTask::makeItem(const db::Tag &tag, DialogNewTask::ItemType itype)
{
    QIcon icon = createUserIcon(50, tag.color, true);
    QStandardItem * item = new QStandardItem(icon, tag.name);

    if (itype == listItemT) {
        item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
        item->setData(Qt::Unchecked, Qt::CheckStateRole);
    }

    item->setData(db.IDToQVariant(tag.id), Qt::UserRole+1);

    return item;
}


/* Called when the accepting button clicked. Prepares a structure with data and emits it. */
void DialogNewTask::createTask() {
  if (!editName->text().isEmpty()) {

    db::Task newTask;
    newTask.name = editName->text();
    newTask.description = editDesc->toPlainText();
    newTask.assigned_pomodoros = spinNumOfPomo->value();
    newTask.completed_pomodoros = 0;
    newTask.deadline = editDateTime->dateTime().toMSecsSinceEpoch();

    QString combo = mainTagComboBox->itemText(mainTagComboBox->currentIndex());
    //qDebug() << "Main tag name: " << combo;


    QStandardItem * sel_item = mainTagModel->item(mainTagComboBox->currentIndex());
    db::ID mt_id = db.QVariantToID(sel_item->data(Qt::UserRole+1));

    db::ID t_id;
    QList<db::ID> tag_list;
    for(int i=0; i < tagListModel->rowCount() ; i++) {
        QStandardItem * item = tagListModel->item(i);
        //qDebug() << item->data(Qt::CheckStateRole);
        if (item->data(Qt::CheckStateRole).toInt() == Qt::Checked) {
            t_id = db.QVariantToID(item->data(Qt::UserRole+1));
            //qDebug() << "tag: " << t_id;
            tag_list.push_back(t_id);
            if (db.IsNotValid()) {
                //qDebug() << "Nie udalo sie dodac relacji\n"
                         //<< db.LastErrorDescription();
            }
        }
    }
    db.TaskAdd(newTask,tag_list, mt_id);
  }
  close();
}

void DialogNewTask::slotChanged(const QModelIndex &topLeft, const QModelIndex /* &bottomRight */)
{
    //qDebug() << "id check" << topLeft.data(Qt::UserRole+1);

    db::ID id = db.QVariantToID(topLeft.data(Qt::UserRole+1));
    db::Tag tag = db.TagGet(id);

    QMap<db::ID, QStandardItem* >::iterator it = cbItems.find(id);

    if(it == cbItems.end()) { //new tag checked
        QStandardItem * item = makeItem(tag, comboBoxItemT);
        cbItems.insert(tag.id, item);
        mainTagModel->appendRow(item);
    }
    else { //tag unchecked
        cbItems.erase(it);

        mainTagModel->clear();

        QStandardItem * no_mt = new QStandardItem("<SELECT MAIN TAG>");
        no_mt->setData(db.IDToQVariant(db::NO_ID), Qt::UserRole+1);
        mainTagModel->insertRow(0, no_mt);

        db::Tag tag2;
        QList<db::ID> keys = cbItems.keys();
        for(int i = 0; i < keys.size(); i++) {

            tag2 = db.TagGet(keys.at(i));
            QStandardItem * item2 = makeItem(tag2, comboBoxItemT);
            cbItems.insert(tag2.id, item2);
            mainTagModel->appendRow(item2);
        }

    } // end if

}

