#include <QDebug>
#include <QMessageBox>

#include "namespaces.h"
#include "google_drive/drive.h"
#include "gd_thread.h"
#include "database.h"

GD_Thread::GD_Thread():
    active(false)
{
}

void GD_Thread::run(){
  active = true;

  db::Database db;
  db.Synchronize();

  if (db.IsNotValid()) {
    emit syncFail(db.LastErrorDescription());
  }
  else {
    emit syncOk();
  }
//    Py_Errors::Error ret_val;
//
//    ret_val = download_from_GD(QString("Raport.pdf"), QString("/home/kuba/Pulpit/plik"));
//
//    // Przyklad obslugi bledow
//    if(ret_val == Py_Errors::BUILD_FAIL){
//        qDebug("Connection Fail!");
//    }
//    else if(ret_val == Py_Errors::SUCCESS){
//        qDebug("Download successful!");
//    }
//
//    // TU MODYFIKACJA PLIKU
//
//    // // // //
//    // // // //
//
//
//    // ZMIENIC SCIEZKI I NAZWY
//    ret_val = upload_to_GD(QString("/home/kuba/Pulpit/plik"), QString("Raport.pdf"));

  active = false;
}

bool GD_Thread::is_active(){
    return active;
}
