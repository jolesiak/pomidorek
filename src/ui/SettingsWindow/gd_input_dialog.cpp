#include "gd_input_dialog.h"
#include "ui_gd_input_dialog.h"

GD_Input_Dialog::GD_Input_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GD_Input_Dialog),
    input(NULL)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(send_signal()));
}

GD_Input_Dialog::~GD_Input_Dialog()
{
    delete ui;
}

void GD_Input_Dialog::send_signal(){
    input = ui->textEdit->toPlainText().toAscii().data();
    emit submited();
    close();
}

char* GD_Input_Dialog::get_input(){
    return input;
}
