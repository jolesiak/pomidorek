#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>

#include "mainwindow.h"
#include "namespaces.h"
#include "ui_settings.h"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT
    
public:
    explicit Settings(QWidget *parent = 0, MainWindow *_main_window = 0);
    ~Settings();

  private slots:
    void on_pushButton_clicked();

  private:
    Ui::Settings *ui;
    MainWindow *main_window;
};

#endif // SETTINGS_H
