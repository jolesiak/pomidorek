#ifndef NAMESPACES_H
#define NAMESPACES_H

namespace Settings_Namespace {
    enum GD_Command_Type {DWNL, UPLD};
}

namespace Py_Errors {
    enum Error{SUCCESS,        // everything ok
               BUILD_FAIL,     // possibly connection fail
               INSERT_FAIL,    // problem with inserting
               DELETE_FAIL,    // problem with deleting
               STATUS_ERROR,   // wrong status while downloading
               DOWNLOAD_FAIL,  // problem with downloading
               NO_FILE,        // no file on drive
               SCRIPT_FAIL     // error in running the script
              };
}

#endif // NAMESPACES_H
