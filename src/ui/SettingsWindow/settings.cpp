#include <QDebug>
#include "settings.h"
#include "basic_functions.h"

Settings::Settings(QWidget *parent, MainWindow *_main_window) :
    QDialog(parent),
    ui(new Ui::Settings),
    main_window(_main_window)
{
    ui->setupUi(this);
    QSettings settings;

    if (settings.contains(TIMER_DEFAULT_TIME)) {
      ui->taskSpinBox->setValue(
            settings.value(TIMER_DEFAULT_TIME).toInt());
    }
    else {
      ui->taskSpinBox->setValue(25);
    }

    if (settings.contains(TIMER_SHORT_BREAK)) {
      ui->shortBreatSpinBox->setValue(
            settings.value(TIMER_SHORT_BREAK).toInt());
    }
    else {
      ui->shortBreatSpinBox->setValue(5);
    }

    if (settings.contains(TIMER_LONG_BREAK)) {
      ui->longBreakSpinBox->setValue(
            settings.value(TIMER_LONG_BREAK).toInt());
    }
    else {
      ui->longBreakSpinBox->setValue(20);
    }
}

Settings::~Settings(){
    delete ui;
}

void Settings::on_pushButton_clicked() {
  QSettings settings("PomodareTeam", "Pomodare");
  settings.setValue(TIMER_DEFAULT_TIME,
                    QString("%1").arg(ui->taskSpinBox->value()));
  settings.setValue(TIMER_SHORT_BREAK,
                    QString("%1").arg(ui->shortBreatSpinBox->value()));
  settings.setValue(TIMER_LONG_BREAK,
                    QString("%1").arg(ui->longBreakSpinBox->value()));
}
