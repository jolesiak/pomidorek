#ifndef GD_PROMPT_NO_BUTTONS_H
#define GD_PROMPT_NO_BUTTONS_H

#include <QDialog>

namespace Ui {
class GD_Prompt_No_Buttons;
}

class GD_Prompt_No_Buttons : public QDialog
{
    Q_OBJECT
    
public:
    explicit GD_Prompt_No_Buttons(QWidget *parent = 0);
    ~GD_Prompt_No_Buttons();
    void hide_button();
    void show_button();
    void set_label(QString s);

//    public slots:
//        void close_window();

//    signals:
//        void done();

    
private:
    Ui::GD_Prompt_No_Buttons *ui;
};

#endif // GD_PROMPT_NO_BUTTONS_H
