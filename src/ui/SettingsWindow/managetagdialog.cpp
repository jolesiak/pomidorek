#include <QColorDialog>
#include <QMessageBox>
#include <QDebug>

#include "managetagdialog.h"
#include "ui_managetagdialog.h"
#include "database.h"
#include "structs.h"

ManageTagDialog::ManageTagDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManageTagDialog)
{
    ui->setupUi(this);

    colorDial = new QColorDialog();
    colorDial -> setOption(QColorDialog::NoButtons);

    QVBoxLayout *layout = new QVBoxLayout(ui->widget);
    layout->addWidget(colorDial);
    ui->widget->setLayout(layout);

    tag_id = db::NO_ID;
}

ManageTagDialog::~ManageTagDialog()
{
    delete ui;
}

void ManageTagDialog::focusOnTag(db::ID tid)
{
    tag_id = tid;
    db::Tag temp_tag = db.TagGet(tid);
    ui->lineEdit->setText(temp_tag.name);
    colorDial -> setCurrentColor(QColor(temp_tag.color));
}

void ManageTagDialog::delFocusOnTag()
{
   tag_id = db::NO_ID;
   ui->lineEdit->setText(QString());
}

void ManageTagDialog::on_pushButton_2_clicked()
{
    QMessageBox msbBox;
    QString trimTagName = ui->lineEdit->text().trimmed();
    if(trimTagName.isEmpty()) {
        msbBox.setText(tr("Wpisz nazwe tagu"));
        msbBox.exec();
    }
    else {
        db::Tag newTag;
        QColor tColor = colorDial->currentColor();

        newTag.name = trimTagName;
        newTag.color = tColor.name();
        //qDebug() << tColor.name();

        if (tag_id == db::NO_ID) {
            db.TagAdd(newTag);
            //qDebug()<<"Add tag: "<< newTag.name;
        }
        else {
            newTag.id = tag_id;
            db.TagEdit(newTag);
            //qDebug()<<"Edit tag: "<< newTag.id << " " << newTag.name;
        }
        tag_id = db::NO_ID;

        emit tagListChanged();
        close();
    }
}

void ManageTagDialog::on_pushButton_clicked()
{
    close();
}
