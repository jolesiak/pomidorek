#ifndef GD_INPUT_DIALOG_H
#define GD_INPUT_DIALOG_H

#include <QDialog>

namespace Ui {
class GD_Input_Dialog;
}

class GD_Input_Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit GD_Input_Dialog(QWidget *parent = 0);
    ~GD_Input_Dialog();
    char* get_input();

    public slots:
        void send_signal();

    signals:
        void submited();
    
private:
    Ui::GD_Input_Dialog *ui;
    char* input;
};

#endif // GD_INPUT_DIALOG_H
