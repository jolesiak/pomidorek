#include "gd_prompt_no_buttons.h"
#include "ui_gd_prompt_no_buttons.h"

GD_Prompt_No_Buttons::GD_Prompt_No_Buttons(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GD_Prompt_No_Buttons)
{
    ui->setupUi(this);
//    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(close_window()));
}

GD_Prompt_No_Buttons::~GD_Prompt_No_Buttons()
{
    delete ui;
}

void GD_Prompt_No_Buttons::show_button(){
    ui->pushButton->show();
}

void GD_Prompt_No_Buttons::hide_button(){
    ui->pushButton->hide();
}

void GD_Prompt_No_Buttons::set_label(QString s){
    ui->label->setText(s);
}

//void GD_Prompt_No_Buttons::close_window(){
//    close();
//    emit done();
//}
