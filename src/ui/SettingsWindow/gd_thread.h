#ifndef GD_THREAD_H
#define GD_THREAD_H

#include <QtCore>
#include <QtGui>

#include "namespaces.h"

class GD_Thread : public QThread
{
    Q_OBJECT
public:
    GD_Thread();
    void run();
    bool is_active();

private:
    bool active;

signals:
    void syncFail(QString err);
    void syncOk();
};

#endif // GD_THREAD_H
