#ifndef MANAGETAGDIALOG_H
#define MANAGETAGDIALOG_H

#include <QDialog>
#include <QColorDialog>
#include "database.h"

namespace Ui {
class ManageTagDialog;
}

class ManageTagDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ManageTagDialog(QWidget *parent = 0);
    ~ManageTagDialog();

    void focusOnTag(db::ID tid);
    void delFocusOnTag();
    
private:
    Ui::ManageTagDialog *ui;
    QColorDialog * colorDial;
    db::Database db;
    db::ID tag_id;

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

signals:
    void tagListChanged();


};

#endif // MANAGETAGDIALOG_H
