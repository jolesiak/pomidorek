#include "finishedtasktable.h"

FinishedTaskTable::FinishedTaskTable(QWidget *parent):
    TaskTableWidget(parent)
{
    baseModel = new TaskTableModel(ModelType::COMPLETED, this);
    proxyModel = new TaskProxyModel(this);

    proxyModel -> setSourceModel(baseModel);

    ui->tableView->setModel(proxyModel);

    ui->tableView->resizeColumnToContents(0);
    ui->tableView->resizeColumnToContents(1);
    ui->tableView->resizeColumnToContents(5);
    ui->tableView->resizeColumnToContents(6);
    ui->tableView->resizeColumnToContents(7);

    createActions();
}

void FinishedTaskTable::createActions()
{
  deleteAction = new QAction(tr("&Delete tasks"), this);
  deleteAction->setStatusTip(tr("Delete tasks"));
  addAction(deleteAction);
  deleteAction->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_D);
  connect(deleteAction, SIGNAL(triggered()), this, SLOT(aDelete()));
}

void FinishedTaskTable::table_customContexMenu(QPoint /* pos */){
  //qDebug() << " DeletedTaskTable::table_customContexMenu";
  if(isSelectionModel(ui->tableView)) {
      QMenu menu(this);
      menu.addAction(deleteAction);

      menu.exec(QCursor::pos());
  }
}
