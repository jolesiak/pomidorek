#ifndef FINISHEDTASKTABLE_H
#define FINISHEDTASKTABLE_H

#include <QWidget>
#include <QAction>
#include "task_table_widget/tasktablewidget.h"

class FinishedTaskTable : public TaskTableWidget
{
    Q_OBJECT
public:
    explicit FinishedTaskTable(QWidget *parent = 0);
protected:
  void createActions();

protected slots:
  void table_customContexMenu(QPoint /* pos */);
private:
  QAction *deleteAction;
};

#endif // FINISHEDTASKTABLE_H
