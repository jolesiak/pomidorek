#include <QDebug>
#include <QFont>
#include <QSettings>
#include <QTime>
#include <QVector>
#include <QString>
#include <QList>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "basic_functions.h"
#include "settings.h"
#include "mainwindownamespace.h"
#include "dialognewtask.h"
#include "task_table_widget/tasktablewidget.h"
#include "database.h"
#include "src/ui/AboutWindow/aboutwindow.h"

#include "selecttaskdialog.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);

  db.Init();

  QFontDatabase::addApplicationFont(":/fonts/entypo.ttf");
  QFontDatabase::addApplicationFont(":/CenturyFont");
  QFontDatabase::addApplicationFont(":/fonts/Ubuntu-C.ttf");
  QFontDatabase::addApplicationFont(":/PrimeFont");
  QFontDatabase::addApplicationFont(":/FoodFont");

  /* Restore window position if any available. If not - set default one */
  QSettings settings;
  if (settings.contains(MAIN_WINDOW_GEOMETRY_SETTINGS)) {
    restoreGeometry(settings.value(MAIN_WINDOW_GEOMETRY_SETTINGS).toByteArray());
  }
  else {
    centerWindow(this, MainWindowDisplay::DEFAULT_SCREEN_SCALE);
  }
  setMinimumSize(MainWindowDisplay::MIN_WIDTH, MainWindowDisplay::MIN_HEIGHT);

  /* Create all widgets essential for the MainWindow display */
  buildMainWindowWidgets();

  /* Create the menu bar */
  createMenuBar();

  /* Set the central widget for the MainWindow */
  setCentralWidget(ui->MainSplitter);

  createConnections();
  initStatusBarRight();
}

MainWindow::~MainWindow() {
  delete ui;
}

/* Create all essential widgets for the MainWindow. Position them correctly. */
void MainWindow::buildMainWindowWidgets() {
  taskTabWidget = new TaskTabWidget(this);
  taskListWidget = new TaskListWidget(this);
  taskSListWidget = new TaskSuperListWidget(this);

  scrollArea = taskSListWidget->getScrollArea();
  scrollArea->setWidgetResizable(true);
  scrollArea->setWidget(taskListWidget);
  scrollArea->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);

  QVBoxLayout* leftLayout = new QVBoxLayout();
  QWidget * leftWidget = new QWidget(this);

  ctrlWidget = new CtrlWidget(this);

  leftLayout->addWidget(taskSListWidget);
  leftLayout->addWidget(ctrlWidget);
  leftWidget->setLayout(leftLayout);

  ui->MainSplitter->addWidget(leftWidget);
  ui->MainSplitter->addWidget(taskTabWidget);
  leftWidget->setMaximumWidth(500);

  QSettings setting;
  if(setting.contains(SPLITTER_GEOMETRY_SETTINGS)) {
    ui->MainSplitter->restoreState(
          setting.value(SPLITTER_GEOMETRY_SETTINGS).toByteArray());
  }
}

void MainWindow::createConnections() {
  /* Notify the widget responsible for displaying info about an item selection */
  connect(taskListWidget, SIGNAL(displayTaskInfo(db::ID)),
          taskTabWidget->getInfoWidget(), SLOT(displayTask(db::ID)));

  /* buttons under the lists */
  connect(ctrlWidget->getRunButton(), SIGNAL(clicked()),
          this, SLOT(runTimer()));
  connect(ctrlWidget->getRmButton(), SIGNAL(clicked()),
          taskListWidget, SLOT(constrictActiveAndClear()));
  connect(ctrlWidget->getClearCmplButton(), SIGNAL(clicked()),
          taskListWidget, SLOT(removeAllCompleted()));

  /* reset */
  connect(this, SIGNAL(resetHardSig()),
          taskListWidget, SLOT(resetHard()));
  connect(this, SIGNAL(resetHardSig()),
          taskTabWidget->getTaskAllWidget()->getBaseModel(), SLOT(refreshData()));
  connect(this, SIGNAL(resetHardSig()),
          taskTabWidget->getTaskActiveWidget()->getBaseModel(), SLOT(refreshData()));
  connect(this, SIGNAL(resetHardSig()),
          taskTabWidget->getTaskDeletedWidget()->getBaseModel(), SLOT(refreshData()));
  connect(this, SIGNAL(resetHardSig()),
          taskTabWidget->getTaskFinishedWidget()->getBaseModel(), SLOT(refreshData()));
  connect(this, SIGNAL(resetHardSig()),
          taskTabWidget->getTagWidget(), SLOT(refreshList()));
  connect(this, SIGNAL(resetHardSig()),
          taskTabWidget->getInfoWidget(), SLOT(reset()));

  connect(ctrlWidget, SIGNAL(syncFinished()),
          this, SLOT(syncFinished()));
  connect(ctrlWidget, SIGNAL(syncFailed()),
          this, SLOT(syncFailed()));
  connect(ctrlWidget->getSyncButton(), SIGNAL(clicked()),
          this, SLOT(setSynchroText()));

  /* connect with Database */
  connect(&db::DatabaseRefresher::Instance(), SIGNAL(RefreshData()),
          this, SLOT(resetHard()));

}

/*  ============================  MENU BAR INITIALIZATION ======================== */

void MainWindow::createMenuBar() {
  menu_bar = menuBar()->addMenu(tr("File"));
  createNewTaskAction();
  createQuitAction();

  menu_bar = menuBar()->addMenu(tr("Tools"));
  createSettingsAction();

  menu_bar = menuBar()->addMenu(tr("Help"));
  createAboutAction();
}

void MainWindow::createNewTaskAction() {
  QAction *newTaskAction = new QAction(tr("New Task"), this);
  newTaskAction->setShortcut(Qt::CTRL + Qt::Key_N);
  connect(newTaskAction, SIGNAL(triggered()), this, SLOT(openNewTaskWindow()));
  menu_bar->addAction(newTaskAction);
}

void MainWindow::createQuitAction() {
  QAction *quitAction = new QAction(tr("Exit"), this);
  connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
  menu_bar->addAction(quitAction);
}

void MainWindow::createSettingsAction() {
  QAction *settingsAction = new QAction(tr("Settings"), this);
  settingsAction->setShortcut(Qt::CTRL + Qt::Key_S);
  connect(settingsAction, SIGNAL(triggered()), this,
          SLOT(openSettingsWindow()));
  menu_bar->addAction(settingsAction);
}

void MainWindow::createAboutAction() {
  QAction *aboutAction = new QAction(tr("About"), this);
  connect(aboutAction, SIGNAL(triggered()), this,
          SLOT(openAboutWindow()));
  menu_bar->addAction(aboutAction);
}

/* ================================  INNE SLOTY  ================================== */

void MainWindow::openNewTaskWindow() {
  DialogNewTask * dialog = new DialogNewTask(this);
  dialog->show();
}

void MainWindow::openSettingsWindow() {
  Settings *s = new Settings(this, this);
  s->show();
}

void MainWindow::openAboutWindow()
{
    AboutWindow *a = new AboutWindow(this);
    a->show();
}

void MainWindow::runTimer()
{
  //////qDebug() << taskListWidget->getFirstNotCompletedTaskID();
  if (taskListWidget->getFirstNotCompletedTaskID() != db::NO_ID) {
    taskListWidget->disableAllItems();

    timerWidget = new TaskTimerWidget(this);
    timerWidget->setWindowFlags(Qt::Window);

    connect(timerWidget, SIGNAL(enableAllItems()),
            taskListWidget, SLOT(enableAllItems()));
    connect(timerWidget, SIGNAL(extendItemOnTheList(db::ID)),
            taskListWidget, SLOT(extendItemID(db::ID)));

    timerWidget->setAttribute(Qt::WA_DeleteOnClose);
    timerWidget->show();
  }
  else {
    QMessageBox mb;
    //mb.setWindowTitle("title");
    mb.setText(tr("Please select some tasks before running timer"));
    mb.exec();
  }
}

void MainWindow::setSynchroText()
{
  setStatusBarLeft("Synchronizing...", 0);
}

/* ================================= SETTINGS =======================================*/

/* Write current MainWindow geometry to the application settings */
void MainWindow::writeSettings() {
  QSettings settings("PomodareTeam", "Pomodare");
  settings.setValue(MAIN_WINDOW_GEOMETRY_SETTINGS, saveGeometry());
  settings.setValue(SPLITTER_GEOMETRY_SETTINGS, ui->MainSplitter->saveState());
}


/* ================================  OBSŁUGA EVENTÓW  =============================== */

/* Set the maximum width of the tasks list (depends on the MainWindow width) */
void MainWindow::resizeEvent(QResizeEvent *) {
  scrollArea->setMaximumWidth(centralWidget()->width() * MainWindowDisplay::MAX_TASK_LIST_WIDTH);
}

void MainWindow::closeEvent(QCloseEvent *) {
    writeSettings();

}

void MainWindow::resetHard()
{
    ////////qDebug() << "MainWindow::resetHard()";
    emit resetHardSig();
}

void MainWindow::syncFinished()
{
  QDateTime dateTime = QDateTime::currentDateTime();
  QString dateTimeString = dateTime.toString();
  setStatusBarRight("Last synchronization: "+dateTimeString);
  setStatusBarLeft("Synchronization ended successfully", 3000);
}

void MainWindow::syncFailed()
{
  setStatusBarLeft("Synchronization failed!", 3000);
}


void MainWindow::setStatusBarRight(const QString text)
{
  statusBarRightLabel->setText(text);
  statusBarRightLabel->setFrameStyle(QFrame::Panel | QFrame::Sunken);
}

void MainWindow::initStatusBarRight()
{
  statusBarRightLabel = new QLabel(this);
 // statusBarRightLabel->setText("You haven't perform any synchronization yet");
  ui->statusBar->addPermanentWidget(statusBarRightLabel);
}

void MainWindow::setStatusBarLeft(const QString text, const int timeout)
{
  ui->statusBar->showMessage(text, timeout);
}
