#include "activetasktable.h"

ActiveTaskTable::ActiveTaskTable(QWidget *parent) :
    TaskTableWidget(parent)
{
    baseModel = new TaskTableModel(ModelType::ACTIVE, this);
    proxyModel = new TaskProxyModel(this);

    proxyModel -> setSourceModel(baseModel);

    ui->tableView->setModel(proxyModel);

    ui->tableView->resizeColumnToContents(0);
    ui->tableView->resizeColumnToContents(1);
    ui->tableView->resizeColumnToContents(5);
    ui->tableView->resizeColumnToContents(6);
    ui->tableView->resizeColumnToContents(7);

    createActions();
}

void ActiveTaskTable::createActions(){
    addTaskAction = new QAction(tr("&Select tasks"), this);
    addTaskAction->setStatusTip(tr("Add selected tasks to Task List"));
    addAction(addTaskAction);
    addTaskAction->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_S);
    connect(addTaskAction, SIGNAL(triggered()), this, SLOT(aAdd()));

    deleteAction = new QAction(tr("&Delete tasks"), this);
    deleteAction->setStatusTip(tr("Delete tasks"));
    addAction(deleteAction);
    deleteAction->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_D);
    connect(deleteAction, SIGNAL(triggered()), this, SLOT(aDelete()));

    editAction = new QAction(tr("&Edit task"), this);
    editAction->setStatusTip(tr("Edit task"));
    addAction(editAction);
    editAction->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_E);
    connect(editAction, SIGNAL(triggered()), this, SLOT(aEdit()));
}

void ActiveTaskTable::table_customContexMenu(QPoint /* pos */) {
  //qDebug() << "ActiveTaskTable::table_customContexMenu";
  if(isSelectionModel(ui->tableView))
  {
      QMenu menu(this);
      menu.addAction(addTaskAction);
      menu.addAction(deleteAction);
      menu.addSeparator();
      menu.addAction(editAction);
      if(isSingleSelection())
          editAction->setEnabled(true);
      else
          editAction->setEnabled(false);

      menu.exec(QCursor::pos());
  }
}
