#ifndef ACTIVETASKTABLE_H
#define ACTIVETASKTABLE_H

#include <QWidget>
#include "task_table_widget/tasktablewidget.h"

class ActiveTaskTable : public TaskTableWidget
{
    Q_OBJECT
public:
     explicit ActiveTaskTable(QWidget *parent = 0);
protected:
  void createActions();

protected slots:
  void table_customContexMenu(QPoint /* pos */);

private:
  QAction *addTaskAction;
  QAction *deleteAction;
  QAction *editAction;
};

#endif // ACTIVETASKTABLE_H
