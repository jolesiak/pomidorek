#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QAction>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTimer>
#include <QMenuBar>
#include <QString>
#include <QList>
#include <QScrollArea>
#include <QVector>

#include "task_timer/tasktimerwidget.h"
#include "task_info/taskinfowidget.h"
#include "tasks_list/tasklistwidget.h"
#include "ctrlWidget/ctrlwidget.h"

#include "tasktabwidget.h"
#include "tasks_list/tasksuperlistwidget.h"
#include "tag_widget/tagwidget.h"

#include "database.h"

// Identyfikator rozmiarów głównego okna w ustawieniach
const QString MAIN_WINDOW_GEOMETRY_SETTINGS = "MainWindowGeometry";
const QString SPLITTER_GEOMETRY_SETTINGS = "SplitterWindowGeometry";

namespace Ui {
  class MainWindow;
}

class MainWindow: public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private:
  Ui::MainWindow *ui;
  QMenu *menu_bar;
  QScrollArea *scrollArea;

  TaskSuperListWidget *taskSListWidget;
  TaskTimerWidget *timerWidget;
  TaskListWidget *taskListWidget;
  TaskTabWidget *taskTabWidget;

  CtrlWidget *ctrlWidget;

  QLabel *statusBarRightLabel;

  // Tworzenie menu
  void createMenuBar();
  void createNewTaskAction();
  void createQuitAction();
  void createSettingsAction();
  void createAboutAction();

  void switchTimeView();
  void buildMainWindowWidgets();
  void createConnections();
  void createEmptyListInfo();
  void writeSettings();

  void refreshTaskList();
  void setStatusBarRight(const QString text);
  void initStatusBarRight();
  void setStatusBarLeft(const QString text, const int timeout);

  db::Database db;

signals:
  void resetHardSig();

public slots:
  void resetHard();
  void syncFinished();
  void syncFailed();

private slots:
  void openNewTaskWindow();
  void openSettingsWindow();
  void openAboutWindow();
  void runTimer();
  void setSynchroText();

protected:
  void resizeEvent(QResizeEvent * );
  void closeEvent(QCloseEvent *);
};

#endif // MAINWINDOW_H
