#include "tasktabwidget.h"
#include "ui_tasktabwidget.h"

#include <QDebug>

TaskTabWidget::TaskTabWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskTabWidget)
{
    setObjectName("TaskTabWidget");
    ui->setupUi(this);

    /* Creating tab's widgets */
    infoWidget = new TaskInfoWidget(this);
    taskAllWidget = new AllTaskTable(this);
    tagWidget = new TagWidget(this);
    taskActiveWidget = new ActiveTaskTable(this);
    taskFinishedWidget = new FinishedTaskTable(this);
    taskDeletedWidget = new DeletedTaskTable(this);

    addTaskTab(infoWidget, tr("Details"));
    addTaskTab(taskActiveWidget, tr("Active"));
    addTaskTab(taskFinishedWidget, tr("Completed"));
    addTaskTab(taskDeletedWidget, tr("Trash"));
    addTaskTab(taskAllWidget, tr("All"));
    addTaskTab(tagWidget, tr("Tags"));

    ui->tabWidget->setCurrentIndex(1);

}


void TaskTabWidget::addTaskTab(QWidget *page, const QString &label)
{
  setMargins(page);
  ui->tabWidget->addTab(page, label);
}

void TaskTabWidget::setMargins(QWidget *w)
{
  w->setContentsMargins(1,1,1,0);
  w->layout()->setContentsMargins(1,1,1,0);
}


TaskTabWidget::~TaskTabWidget()
{
    delete ui;
}
