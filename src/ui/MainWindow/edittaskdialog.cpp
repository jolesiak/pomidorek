#include "edittaskdialog.h"
#include "ui_edittaskdialog.h"
#include <QDateTime>
#include <QDebug>
#include <QStandardItemModel>

#include "database.h"
#include "tagmodel.h"
#include <QListView>
#include <QSharedPointer>
#include "basic_functions.h"

EditTaskDialog::EditTaskDialog(db::Task &task, QWidget *parent):
    QDialog(parent){
  setupUi(this);

  editedTask = task;

  setDefaultDateTime();
  QList<db::Tag> tl = db.TagList();

  tagListModel = new QStandardItemModel();
  mainTagModel = new QStandardItemModel();

  /* create default item */
  QStandardItem * no_mt = new QStandardItem("<SELECT MAIN TAG>");
  no_mt->setData(db.IDToQVariant(db::NO_ID), Qt::UserRole+1);
  mainTagModel->insertRow(0, no_mt);

  QList<db::Tag> taskTags = db.TaskTags(editedTask.id);
  int i=0;
  foreach(const db::Tag tag, tl){
      QStandardItem * item = makeItem(tag, listItemT);
      listItems.insert(tag.id, item);
      tagListModel->insertRow(i++, item);
  }

  mainTagComboBox->setModel(mainTagModel);
  listView->setModel(tagListModel);

  /* fill all gaps */
  focusOnTask(editedTask);

  connect(tagListModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)),
          this, SLOT(slotChanged(QModelIndex,QModelIndex)));
  connect(buttonsBox, SIGNAL(accepted()), this, SLOT(editTask()));
}

/* Set default values for the time field. */
void EditTaskDialog::setDefaultDateTime() {
  editDateTime->setMinimumDateTime(QDateTime::currentDateTime());
}

void EditTaskDialog::focusOnTask(db::Task &task)
{
    editName->setText(task.name);
    editDesc->setPlainText(task.description);
    editDateTime->setDateTime(QDateTime::fromMSecsSinceEpoch(task.deadline));
    spinNumOfPomo->setMinimum(task.completed_pomodoros+1);
    spinNumOfPomo->setValue(task.assigned_pomodoros);
    QList<db::Tag> taskTags = db.TaskTags(editedTask.id);

    QMap<db::ID, QStandardItem* >::iterator it;
    foreach(db::Tag tag, taskTags) {
        it = listItems.find(tag.id);
        if (it != listItems.end()) {
            QStandardItem *item = it.value();
            item->setCheckState(Qt::Checked);

            QStandardItem * item2 = makeItem(tag, comboBoxItemT);
            cbItems.insert(tag.id, item2);
            mainTagModel->appendRow(item2);
        }
    }

    db::Tag mainTag = db.TaskMainTag(editedTask.id);
    int mainTagIndex = mainTagComboBox->findData(db.IDToQVariant(mainTag.id),
                                      Qt::UserRole+1,
                                      Qt::MatchRecursive);
    mainTagComboBox->setCurrentIndex(mainTagIndex);
}

QStandardItem * EditTaskDialog::makeItem(const db::Tag &tag,
                                         EditTaskDialog::ItemType itype) {
    QIcon icon = createUserIcon(50, tag.color, true);
    QStandardItem * item = new QStandardItem(icon, tag.name);

    if (itype == listItemT) {
        item->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
        item->setData(Qt::Unchecked, Qt::CheckStateRole);
    }

    item->setData(db.IDToQVariant(tag.id), Qt::UserRole+1);

    return item;
}


/* Called when the accepting button clicked. Prepares a structure with data and emits it. */
void EditTaskDialog::editTask() {
  if (!editName->text().isEmpty()) {

    editedTask.name = editName->text();
    editedTask.description = editDesc->toPlainText();
    editedTask.assigned_pomodoros = spinNumOfPomo->value();
    editedTask.deadline = editDateTime->dateTime().toMSecsSinceEpoch();

    db.TaskEdit(editedTask);

    QString combo = mainTagComboBox->itemText(mainTagComboBox->currentIndex());
    //qDebug() << "Main tag name: " << combo;

    QStandardItem * sel_item = mainTagModel->item(mainTagComboBox->currentIndex());
    db::ID mt_id = db.QVariantToID(sel_item->data(Qt::UserRole+1));
    //qDebug() << "main tag id:" << mt_id;

    //usuwanie tagow
    foreach (db::Tag tag, db.TaskTags(editedTask.id)) {
        db.TaskRemoveTag(editedTask.id, tag.id);
    }

    db::ID t_id;
    for(int i=0; i < tagListModel->rowCount() ; i++) {
        QStandardItem * item = tagListModel->item(i);
        //qDebug() << item->data(Qt::CheckStateRole);
        if (item->data(Qt::CheckStateRole).toInt() == Qt::Checked) {
            t_id = db.QVariantToID(item->data(Qt::UserRole+1));
            //qDebug() << "tag: " << t_id;
            db.TaskAddTag(editedTask.id, t_id);
            if (db.IsNotValid()) {
                //qDebug() << "Nie udalo sie dodac relacji\n"
                         //<< db.LastErrorDescription();
            }
        }
    }


    /* set main tag id */
    if(mt_id != db::NO_ID) {
        //qDebug() << "mt_id: " << mt_id;
        //qDebug() << "newTask.id: " << editedTask.id;
        db.TaskSetMainTag(editedTask.id, mt_id);
        if (db.IsNotValid()) {
            //qDebug() << "Nie udalo sie dodac relacji\n"
                     //<< db.LastErrorDescription();
        }
    }
  }
  close();
}

void EditTaskDialog::slotChanged(const QModelIndex &topLeft,
                                 const QModelIndex& /* bottomRight */)
{
    //qDebug() << "id check" << topLeft.data(Qt::UserRole+1);


    db::ID id = db.QVariantToID(topLeft.data(Qt::UserRole+1));
    db::Tag tag = db.TagGet(id);

    QMap<db::ID, QStandardItem* >::iterator it = cbItems.find(id);

    if(it == cbItems.end()) { //new tag checked
        QStandardItem * item = makeItem(tag, comboBoxItemT);
        cbItems.insert(tag.id, item);
        mainTagModel->appendRow(item);
    }
    else { //tag unchecked
        cbItems.erase(it);

        mainTagModel->clear();

        QStandardItem * no_mt = new QStandardItem("<SELECT MAIN TAG>");
        no_mt->setData(db.IDToQVariant(db::NO_ID), Qt::UserRole+1);
        mainTagModel->insertRow(0, no_mt);

        db::Tag tag2;
        QList<db::ID> keys = cbItems.keys();
        for(int i = 0; i < keys.size(); i++) {
            tag2 = db.TagGet(keys.at(i));
            QStandardItem * item2 = makeItem(tag2, comboBoxItemT);
            cbItems.insert(tag2.id, item2);
            mainTagModel->appendRow(item2);
        }

    } // end if

}

