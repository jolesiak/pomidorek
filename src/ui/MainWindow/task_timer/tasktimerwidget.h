#ifndef TASKTIMER_H
#define TASKTIMER_H

#include <QWidget>
#include <QTimer>

#include "ui_TaskTimerForm.h"
#include "database.h"

const qint32 MILISEC_PER_SEC = 1000;
const qint32 SEC_PER_MIN = 60;

const QString TIMER_GEOMETRY = "TimerGeometry";

class TaskTimerWidget: public QWidget, private Ui::task_timer_form {
  Q_OBJECT
  public:
    TaskTimerWidget(QWidget *parent = 0);
    ~TaskTimerWidget();

    bool is_running() const { return is_running_; }

    void prepareToStart();
    void setCurrentTaskID(const db::ID id) {currentTaskID = id;}

  protected:
    virtual void closeEvent(QCloseEvent *e);

  private:
    enum ButtonTypes {
      kStartStopButton,
      kIntteruptButton
    };

    qint32 shortBreakTime;
    qint32 longBreakTime;
    qint32 taskDefaultTime;

    void createConnections(); //
    void updateTimerDisplay(qint32 time);//
    void startTimer();//
    void stopTimer();//
    void focusOnTask(db::ID id);
    void updatePomos();
    void showStopButton();
    void showStartButton();
    void manageStatus();
    void disconnectTimer();
    void clearInterrups();
    void writeSettings();


    db::ID getFirstNotCompletedTaskID() const;
    db::ID getNextNotCompletedTaskID(db::ID id) const;
    // return task id that should be executing next
    db::ID nextTaskIDToExecute(db::ID id) const;


    db::Database db;
    QTimer *taskTimer;//
    qint32 timerTime;//
    qint32 counter;

    bool is_running_;
    bool is_break_;
    int interrupt_num_;

    db::ID currentTaskID;

  signals:
    void disableAllItems();
    void enableAllItems();
    void extendItemOnTheList(db::ID id);
    void taskFinished();//
    void toggleItemClickedConnection(bool);//


  private slots:
    void updateTaskTime();//
    void updateBreakTime();//
    void startTask();//
    void startBreak();//
    void stopCurrentTask();//
    void interruptCurrentTask();//
    void startStopButtonClicked();
};

#endif // TASKTIMER_H
