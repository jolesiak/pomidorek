#include <QVBoxLayout>
#include <QTime>
#include <QSignalMapper>
#include <QDebug>
#include <QMessageBox>
#include <QtGui>

#include "tasktimerwidget.h"
//#include "phonon/phonon"
#include "basic_functions.h"

TaskTimerWidget::TaskTimerWidget(QWidget *parent) : QWidget(parent) {
  setupUi(this);
  setObjectName("TaskTimerWidget");

  currentTaskID = getFirstNotCompletedTaskID();

//  Phonon::MediaObject * mo = Phonon::createPlayer(Phonon::NoCategory,
//                                                  Phonon::MediaSource(":/s1"));
//  mo->play();

  QSettings settings;
  if (settings.contains(TIMER_GEOMETRY)) {
    restoreGeometry(settings.value(TIMER_GEOMETRY).toByteArray());
  }
  else {
    this->setGeometry(
           QStyle::alignedRect(
             Qt::LeftToRight,
             Qt::AlignCenter,
             this->size(),
             qApp->desktop()->availableGeometry()
          ));
  }

  if (settings.contains(TIMER_DEFAULT_TIME)) {
    taskDefaultTime = MILISEC_PER_SEC * SEC_PER_MIN *
        settings.value(TIMER_DEFAULT_TIME).toInt();
  }
  else {
    taskDefaultTime = MILISEC_PER_SEC * SEC_PER_MIN * 25;
  }

  if (settings.contains(TIMER_SHORT_BREAK)) {
    shortBreakTime = MILISEC_PER_SEC * SEC_PER_MIN *
        settings.value(TIMER_SHORT_BREAK).toInt();
  }
  else {
    shortBreakTime = MILISEC_PER_SEC * SEC_PER_MIN * 5;
  }

  if (settings.contains(TIMER_LONG_BREAK)) {
    longBreakTime = MILISEC_PER_SEC * SEC_PER_MIN *
        settings.value(TIMER_LONG_BREAK).toInt();
  }
  else {
    longBreakTime = MILISEC_PER_SEC * SEC_PER_MIN * 20;
  }

  taskTimer = new QTimer(this);
  is_running_ = false;
  is_break_ = false;
  interrupt_num_ = 0;
  counter = 0;
  interrupt_button->setEnabled(false);

  updateTimerDisplay(taskDefaultTime);
  createConnections();
  prepareToStart();
}

TaskTimerWidget::~TaskTimerWidget() {
  //qDebug() << "TaskTimerWidget::~TaskTimerWidget";
  emit enableAllItems();
  delete taskTimer;
}

void TaskTimerWidget::prepareToStart()
{
  is_running_ = false;
  interrupt_num_ = 0;
  interrupt_button->setEnabled(false);
  start_stop_button->setEnabled(true);
  focusOnTask(currentTaskID);
  manageStatus();
  showStartButton();
  updateTimerDisplay(taskDefaultTime);
}

void TaskTimerWidget::closeEvent(QCloseEvent *e)
{
  //qDebug() << "TaskTimerWidget::closeEvent";
  if(is_running_) {
    stopTimer();
    QMessageBox mb;
    mb.setText("The close button was pressed.");
    mb.setInformativeText("Are you really sure to close timer and stop executing task?");
    mb.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    mb.setDefaultButton(QMessageBox::No);
    int ret = mb.exec();
    switch(ret) {
      case QMessageBox::Yes:
        e->accept();
        break;
      case QMessageBox::No:
        startTimer();
        e->ignore();
        break;
    }
  }
  writeSettings();
}


void TaskTimerWidget::createConnections() {
  /* Notify the timer when the user wants to start or stop working */
  connect(start_stop_button, SIGNAL(clicked()),
          this, SLOT(startStopButtonClicked()));

  /* Notify the timer when the user wants to pause the current task */
  connect(interrupt_button, SIGNAL(clicked()),
          this, SLOT(interruptCurrentTask()));
}

/* Parse the current time to the QString. Update the time display. */
void TaskTimerWidget::updateTimerDisplay(qint32 time) {
  qint32 Minutes = time / (MILISEC_PER_SEC * SEC_PER_MIN);
  qint32 Seconds = (time / MILISEC_PER_SEC) % SEC_PER_MIN;
  QString str_time = QTime(0, Minutes, Seconds, 0).toString("mm:ss");
  time_display->display(str_time);
}

/* Update the current time (when task is running).
   Stop the timer if this task is finished. */
void TaskTimerWidget::updateTaskTime() {
  timerTime -= MILISEC_PER_SEC;
  if (timerTime >= 0) {
    updateTimerDisplay(timerTime);
  }
  else {
    stopTimer();
    disconnectTimer();
    interrupt_button->setEnabled(false);
    is_break_ = true;
    ++counter;
    manageStatus();

    if (interrupt_num_ > 0) {
      QMessageBox mb;
      QString text = "Your task was interrupted %1 time(s)";
      mb.setText(text.arg(interrupt_num_));
      mb.setInformativeText("Do you want to do it again or accept?");
      QPushButton *doItAgainButton = mb.addButton(tr("Do it again"),
                                            QMessageBox::ActionRole);
      QPushButton *acceptButton = mb.addButton(tr("Accept"),
                                            QMessageBox::ActionRole);
      QPushButton *startNewTaskButton = mb.addButton(tr("Discard and start new task"),
                                            QMessageBox::ActionRole);
      mb.exec();
      if(mb.clickedButton() == doItAgainButton) {
        //nothing
      }
      else if (mb.clickedButton() == acceptButton) {
        db.SelectedTaskAddPomodoro(currentTaskID);
        focusOnTask(currentTaskID);
        manageStatus();
        currentTaskID = nextTaskIDToExecute(currentTaskID);
      }
      else if(mb.clickedButton() == startNewTaskButton) {
        manageStatus();
        currentTaskID = getNextNotCompletedTaskID(currentTaskID);
      }
    } else { //interrupt_num_ == 0
      db.SelectedTaskAddPomodoro(currentTaskID);
      focusOnTask(currentTaskID);
      manageStatus();
      currentTaskID = nextTaskIDToExecute(currentTaskID);
    }

    if (currentTaskID != db::NO_ID) {
      clearInterrups();
      start_stop_button->setEnabled(false);
      startBreak();
    } else { // currentTaskID == NO_ID
      QMessageBox mb;
      mb.setText("Congratulations! No more tasks to execute");
      mb.exec();
      is_running_ =false;
      this->close();
    }
  } // end timerTime < 0
}

/* Update the current time (when break is active).
 Stop the timer if this break is finished. */
void TaskTimerWidget::updateBreakTime() {
  timerTime -= MILISEC_PER_SEC;
  if (timerTime >= 0) {
    updateTimerDisplay(timerTime);
  }
  else {
    stopTimer();
    disconnectTimer();
    prepareToStart();
  }
}

/* Start next task if any exists */
void TaskTimerWidget::startTask() {
  timerTime = taskDefaultTime;
  updateTimerDisplay(timerTime);
  is_break_ = false;
  manageStatus();
  emit extendItemOnTheList(currentTaskID);
  connect(taskTimer, SIGNAL(timeout()), this, SLOT(updateTaskTime()));
  startTimer();
}

/* Start next break if any task exists */
void TaskTimerWidget::startBreak() {

  timerTime = counter % 4 == 0 ? longBreakTime : shortBreakTime;
  updateTimerDisplay(timerTime);
  connect(taskTimer, SIGNAL(timeout()), this, SLOT(updateBreakTime()));
  startTimer();
}

/* Start the timer with the specified interval. */
void TaskTimerWidget::startTimer() {
  taskTimer->start(MILISEC_PER_SEC);
}

void TaskTimerWidget::stopTimer() {
  taskTimer->stop();
}

void TaskTimerWidget::focusOnTask(db::ID id)
{
    db::SelectedTask stask = db.SelectedTaskGet(id);
    db::Task task = db.TaskGet(stask.task_id);

    task_name_label->setText(task.name);
    pomos_label->setText(QString("%1/%2")
                         .arg(stask.completed_pomodoros)
                         .arg(stask.assigned_pomodoros));
}

void TaskTimerWidget::updatePomos()
{
    db::SelectedTask stask = db.SelectedTaskGet(currentTaskID);
    pomos_label->setText(QString("%1/%2")
                         .arg(stask.completed_pomodoros)
                         .arg(stask.assigned_pomodoros));
}

void TaskTimerWidget::showStopButton()
{
    start_stop_button->setText(QString::fromUtf8("◼").append(" Stop"));
}

void TaskTimerWidget::showStartButton()
{
  start_stop_button->setText(QString::fromUtf8("▶").append(" Start"));
}

void TaskTimerWidget::manageStatus() {
  if(is_running_) {
    if(is_break_) {
      QString statusText = "Next task: %1";
      db::ID id = nextTaskIDToExecute(currentTaskID);
      if (id == db::NO_ID) {
        statusLabel->setText("No next task!");
      }
      else if(id == currentTaskID) {
        statusLabel->setText(counter % 4 == 0 ? "Long Break" : "Short Break");
      }
      else {
        db::SelectedTask stask = db.SelectedTaskGet(id);
        db::Task task = db.TaskGet(stask.task_id);
        statusLabel->setText(statusText.arg(task.name));
      }
    }
    else {
      statusLabel->setText("Running");
    }
  }
  else {
    statusLabel->setText("Click Start to run timer");
  }
}

void TaskTimerWidget::disconnectTimer(){
  disconnect(taskTimer, SIGNAL(timeout()), this, SLOT(updateTaskTime()));
  disconnect(taskTimer, SIGNAL(timeout()), this, SLOT(updateBreakTime()));
}

void TaskTimerWidget::clearInterrups()
{
  interrupt_num_ = 0;
  interrupt_button->setText(QString("%1 Interrupt(%2)")
                              .arg(QString::fromUtf8("☢"))
                            .arg(QString::number(interrupt_num_)));
}

void TaskTimerWidget::writeSettings()
{
  QSettings settings("PomodareTeam", "Pomodare");
  settings.setValue(TIMER_GEOMETRY, saveGeometry());
}

db::ID TaskTimerWidget::getFirstNotCompletedTaskID() const
{
  db::ID returnID = db::NO_ID;
  QList<db::SelectedTask> stl = db.SelectedTaskList();
  QList<db::SelectedTask>::iterator it = stl.begin();

  while(it != stl.end()) {
    if (it->assigned_pomodoros > it->completed_pomodoros) {
      returnID = it->id;
      break;
    }
    ++it;
  }
  return returnID;
}

db::ID TaskTimerWidget::getNextNotCompletedTaskID(db::ID id) const
{
  bool afterID = false;
  db::ID returnID = db::NO_ID;
  QList<db::SelectedTask> stl = db.SelectedTaskList();
  QList<db::SelectedTask>::iterator it = stl.begin();
  while(it != stl.end()) {
    if (afterID) {
      if (it->assigned_pomodoros > it->completed_pomodoros) {
        returnID = it->id;
        break;
      }
    }
    else {
      afterID = (it->id == id);
    }
    ++it;
  }
  return returnID;
}

db::ID TaskTimerWidget::nextTaskIDToExecute(db::ID id) const
{
  db::SelectedTask stask = db.SelectedTaskGet(id);
  qint32 ap = stask.assigned_pomodoros;
  qint32 cp = stask.completed_pomodoros;
  if (ap > cp){
    //qDebug() << "TaskTimerWidget::nextTaskIDToExecute1" <<  stask.task_id;
    return id;
  }
  else {
    //qDebug() << "TaskTimerWidget::nextTaskIDToExecute2" <<  getNextNotCompletedTaskID(id);
    return getNextNotCompletedTaskID(id);
  }
}


void TaskTimerWidget::stopCurrentTask() {
  stopTimer();
  disconnectTimer();
  clearInterrups();
  updateTimerDisplay(taskDefaultTime);
}

void TaskTimerWidget::interruptCurrentTask()
{
    ++interrupt_num_;
    interrupt_button->setText(QString("%1 Interrupt(%2)")
                                .arg(QString::fromUtf8("☢"))
                                .arg(QString::number(interrupt_num_)));
    //qDebug() << "Interupt";
}

/* ========================================================================== */
/* ===                           BUTTONS MANAGEMENT                       === */
/* ========================================================================== */

void TaskTimerWidget::startStopButtonClicked()
{
    //qDebug() << "switch from " << (is_running_? "start to stop" : "stop to start");
    if (is_running_) { // stop timer
        //qDebug() << "Stop clicked" ;
        interrupt_button->setEnabled(false);
        showStartButton();
        stopCurrentTask();
    } else { // start timer
        //qDebug() << "Start clicked" ;
        showStopButton();
        interrupt_button->setEnabled(true);
        startTask();
    }
    is_running_ = !is_running_;
    manageStatus();
}
