#include "deletedtasktable.h"

DeletedTaskTable::DeletedTaskTable(QWidget *parent) :
    TaskTableWidget(parent)
{
    baseModel = new TaskTableModel(ModelType::DELETED, this);
    proxyModel = new TaskProxyModel(this);

    proxyModel -> setSourceModel(baseModel);

    ui->tableView->setModel(proxyModel);

    ui->tableView->resizeColumnToContents(0);
    ui->tableView->resizeColumnToContents(1);
    ui->tableView->resizeColumnToContents(5);
    ui->tableView->resizeColumnToContents(6);
    ui->tableView->resizeColumnToContents(7);

    createActions();
}

void DeletedTaskTable::createActions(){
  restoreAction = new QAction(tr("&Restore tasks"), this);
  restoreAction->setStatusTip(tr("Restore selected tasks"));
  addAction(restoreAction);
  restoreAction->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_Q);
  connect(restoreAction, SIGNAL(triggered()), this, SLOT(aRestore()));

  removeAction = new QAction(tr("&Remove tasks"), this);
  removeAction->setStatusTip(tr("Permanently remove selected tasks"));
  addAction(removeAction);
  removeAction->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_R);
  connect(removeAction, SIGNAL(triggered()), this, SLOT(aRemove()));
}

void DeletedTaskTable::table_customContexMenu(QPoint /* pos */){
  //qDebug() << " DeletedTaskTable::table_customContexMenu";
  if(isSelectionModel(ui->tableView)) {
      QMenu menu(this);
      menu.addAction(removeAction);
      menu.addSeparator();
      menu.addAction(restoreAction);

      menu.exec(QCursor::pos());
  }
}
