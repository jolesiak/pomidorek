#ifndef DELETEDTASKTABLE_H
#define DELETEDTASKTABLE_H

#include <QWidget>
#include <QAction>
#include "task_table_widget/tasktablewidget.h"

class DeletedTaskTable : public TaskTableWidget
{
    Q_OBJECT
public:
    explicit DeletedTaskTable(QWidget *parent = 0);
protected:
  void createActions();

protected slots:
  void table_customContexMenu(QPoint /* pos */);

private:
  QAction *removeAction;
  QAction *restoreAction;

};

#endif // DELETEDTASKTABLE_H
