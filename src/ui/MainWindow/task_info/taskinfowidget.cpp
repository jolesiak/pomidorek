#include <QGridLayout>
#include <QDebug>

#include "taskinfowidget.h"
#include "basic_functions.h"
#include <QStandardItemModel>

#include <QPainter>

TaskInfoWidget::TaskInfoWidget(QWidget *parent) : QWidget(parent) {
  setupUi(this);
  setObjectName("TaskInfoWidget");
  displayName(tr("Please, select task from list to see details"));
  isset = false;
  currentlyDisplayed = db::NO_TASK;
}

void TaskInfoWidget::displayDescription(const QString &desc)
{
  contentDescription->setPlainText(desc);
}

void TaskInfoWidget::displayName(const QString &name)
{
    contentName->setText(name);
}

void TaskInfoWidget::resizeEvent(QResizeEvent *)
{
  if (isset) {
    displayName(currentlyDisplayed.name);
  }
}

static QString pomodorosProgress(qint32 l, qint32 r) {
  return QString::number(l) + " / " + QString::number(r);
}

void TaskInfoWidget::displayTask(db::ID id) {
  db::Task task = db.TaskGet(id);
  if (task.id != db::NO_ID) {
    isset = true;
    currentlyDisplayed = task;
    displayName(currentlyDisplayed.name);
    displayDescription(currentlyDisplayed.description);
    contentDeadLine->setText(QDateTime::fromMSecsSinceEpoch(
                     currentlyDisplayed.deadline).toString("dd-MM-yyyy HH:mm"));
    contentPomoNum->setText(pomodorosProgress(
                              currentlyDisplayed.completed_pomodoros,
                              currentlyDisplayed.assigned_pomodoros));
    QStandardItemModel *model = new QStandardItemModel(this);
    tagsListView->setModel(model);
    QList<db::Tag> tag_list = db.TaskTags(id);

    foreach(db::Tag tag, tag_list) {
      QIcon icon = createUserIcon(50, tag.color, true);
      QStandardItem *item = new QStandardItem(icon, tag.name);
      model->appendRow(item);
    }
  }
  else {
    isset = false;
    reset();
  }
}

void TaskInfoWidget::reset() {
//  db::Task task = db.TaskGet(currentlyDisplayed.id);
  QList<db::Task> tl = db.TaskList();
  bool exists = false;

  foreach(db::Task task, tl) {
    if (task.id == currentlyDisplayed.id)
      exists = true;
  }

  if(!exists) {
    isset = false;
    displayName(tr("Please, select task from list to see details"));
    contentDescription->clear();
    contentDeadLine->clear();
    contentPomoNum->clear();
    QStandardItemModel *model = new QStandardItemModel(this);
    tagsListView->setModel(model);
    contentDeadLine->clear();
  }
}

