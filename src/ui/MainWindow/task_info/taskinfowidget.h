#ifndef TASKINFOWIDGET_H
#define TASKINFOWIDGET_H

#include <QWidget>

#include "ui_TaskInfoDisplayForm.h"
#include "database.h"

class TaskInfoWidget: public QWidget, private Ui::formInfoDisplay {
  Q_OBJECT
  public:
  explicit TaskInfoWidget(QWidget *parent = 0);

  private:
    db::Database db;
    db::Task currentlyDisplayed;

    bool isset;

    void displayDescription(QString const& desc);
    void displayName(QString const& name);

  protected:
    void resizeEvent(QResizeEvent *);

  public slots:
    void displayTask(db::ID);
    void reset();

};

#endif // TASKINFOWIDGET_H
