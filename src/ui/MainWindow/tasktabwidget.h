#ifndef TASKTABWIDGET_H
#define TASKTABWIDGET_H

#include <QWidget>
#include <QContextMenuEvent>
#include "task_info/taskinfowidget.h"
#include "tag_widget/tagwidget.h"

#include "active_task_table/activetasktable.h"
#include "all_task_table/alltasktable.h"
#include "deleted_task_table/deletedtasktable.h"
#include "finished_task_table/finishedtasktable.h"

namespace Ui {
class TaskTabWidget;
}

class TaskTabWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit TaskTabWidget(QWidget *parent = 0);
    ~TaskTabWidget();

    void addTaskTab(QWidget * page, const QString & label);

    AllTaskTable * getTaskAllWidget() { return taskAllWidget; }
    ActiveTaskTable *getTaskActiveWidget() { return taskActiveWidget; }
    DeletedTaskTable *getTaskDeletedWidget() { return taskDeletedWidget; }
    FinishedTaskTable *getTaskFinishedWidget() { return taskFinishedWidget; }

    TaskInfoWidget * getInfoWidget() { return infoWidget; }
    TagWidget * getTagWidget() { return tagWidget; }
    
private:
    void setMargins(QWidget *w);

    Ui::TaskTabWidget *ui;
    AllTaskTable *taskAllWidget;
    ActiveTaskTable *taskActiveWidget;
    FinishedTaskTable *taskFinishedWidget;
    TaskInfoWidget *infoWidget;
    DeletedTaskTable *taskDeletedWidget;
    TagWidget *tagWidget;
};

#endif // TASKTABWIDGET_H
