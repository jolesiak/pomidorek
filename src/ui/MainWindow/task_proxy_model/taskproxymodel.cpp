#include "taskproxymodel.h"
#include <QDebug>
#include "task_table_model/tasktablemodel.h"

TaskProxyModel::TaskProxyModel(QObject *parent )
    :QSortFilterProxyModel(parent)
{}

void TaskProxyModel::setFilterMinDeadline(const QDate &date)
{
    minDeadline = date;
    invalidateFilter();
}

void TaskProxyModel::setFilterMaxDeadline(const QDate &date)
{
    maxDeadline = date;
    invalidateFilter();
}

bool TaskProxyModel::filterAcceptsRow(int source_row,
                                      const QModelIndex &source_parent) const
{
    QModelIndex index0 = sourceModel()->index(source_row, NAME,
                                              source_parent);
    QModelIndex index1 = sourceModel()->index(source_row, DESCRIPTION,
                                              source_parent);
    QModelIndex index2 = sourceModel()->index(source_row, POMODOROS,
                                              source_parent);
    QModelIndex index3 = sourceModel()->index(source_row, TAG,
                                              source_parent);
    QModelIndex index4 = sourceModel()->index(source_row, DEADLINE,
                                              source_parent);

    return (sourceModel()->data(index0).toString().contains(filterRegExp())
            || sourceModel()->data(index1).toString().contains(filterRegExp())
            || sourceModel()->data(index2).toString().contains(filterRegExp())
            || sourceModel()->data(index3).toString().contains(filterRegExp()))
            && dateInRange(sourceModel()->data(index4).toDate());
}

bool TaskProxyModel::lessThan(const QModelIndex &left,
                              const QModelIndex &right) const
{
    QVariant leftData = sourceModel() -> data(left);
    QVariant rightData = sourceModel() -> data(right);

    if(leftData.type() == QVariant::DateTime) {
        return leftData.toDateTime() < rightData.toDateTime();
    }
    else if (left.column() == PRIORITY) {
        if (leftData.toString() == " ") {
          return true;
        }
        return (left.data(Qt::UserRole+1).toInt() <
                right.data(Qt::UserRole+1).toInt());
    }
    else {
        return (QString::localeAwareCompare(leftData.toString(),
                                            rightData.toString()) < 0);
    }
}

bool TaskProxyModel::dateInRange(const QDate &date) const
{
    return (!minDeadline.isValid() || date > minDeadline)
            && (!maxDeadline.isValid() || date < maxDeadline);
}
