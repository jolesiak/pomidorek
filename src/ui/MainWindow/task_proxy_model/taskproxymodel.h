#ifndef TASKPROXYMODEL_H
#define TASKPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QDate>
#include <QVariant>

class TaskProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    TaskProxyModel(QObject * parent = 0);

    QDate filterMinDeadline() const { return minDeadline; }
    void setFilterMinDeadline(const QDate& date);

    QDate filterMaxDeadline() const { return maxDeadline; }
    void setFilterMaxDeadline(const QDate& date);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;

private:
    bool dateInRange(const QDate & date) const;

    QDate minDeadline;
    QDate maxDeadline;
};

#endif // TASKPROXYMODEL_H
