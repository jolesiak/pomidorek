#include "selecttaskdialog.h"
#include "ui_selecttaskdialog.h"
#include "spinboxdelegate.h"
#include <QStandardItemModel>
#include <QDebug>
#include "database.h"
#include <QMap>

SelectTaskDialog::SelectTaskDialog(QList<db::ID> id_list, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::SelectTaskDialog),
  id_lists_(id_list)
{
  ui->setupUi(this);

  SpinBoxDelegate *delegate;
  delegate = new SpinBoxDelegate(parent);

  QStandardItemModel *model = new QStandardItemModel(id_lists_.size(), 2);
  ui->tableView->setModel(model);
  ui->tableView->setItemDelegateForColumn(1, delegate);
  model->setHeaderData(0, Qt::Horizontal, "task name");
  model->setHeaderData(1, Qt::Horizontal, "pomodoros in session");

  Qt::ItemFlags flags;
  db::Task task;
  int row = 0;

  QMap<db::ID, qint32> pomos_map = getNumPomodorosMap();
  foreach(db::ID id, id_lists_) {
      QModelIndex index = model->index(row, 0, QModelIndex());
      QStandardItem *item = model->itemFromIndex(index);
      flags = item->flags();
      flags &= ~Qt::ItemIsEditable;
      item->setFlags(flags);
      task = db.TaskGet(id);
      model->setData(index, QVariant(task.name));


      QModelIndex index2 = model->index(row, 1, QModelIndex());
      qint32 maxVal = task.assigned_pomodoros-task.completed_pomodoros;
      if (pomos_map.contains(task.id))
        maxVal -= pomos_map[task.id];

      model->setData(index2, QVariant(maxVal<=0? 0 : 1));
      model->setData(index2, QVariant(maxVal), Qt::UserRole);
      model->setData(index2, db.IDToQVariant(task.id), Qt::UserRole+1);
      row++;
  }

  ui->tableView->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
}

SelectTaskDialog::~SelectTaskDialog()
{
  delete ui;
}

void SelectTaskDialog::refresh()
{
  ui->tableView->reset();
}


void SelectTaskDialog::on_buttonBox_accepted()
{
  ui->tableView->selectAll();
  QItemSelectionModel * model = ui->tableView->selectionModel();
  QModelIndexList index_list = model->selectedRows(1);

  db::SelectedTask stask;
  QList<db::SelectedTask> stl;

  foreach(const QModelIndex &md, index_list) {
      db::ID id = db.QVariantToID(md.data(Qt::UserRole+1));
      qint32 pomos_ses = md.data(Qt::EditRole).toInt();
      if (pomos_ses > 0) {
        //qDebug() << "id " << id << " pomos " << pomos_ses;

        stask.assigned_pomodoros = pomos_ses;
        stask.completed_pomodoros = 0;
        stask.task_id = id;

        //db.SelectedTaskAdd(stask, db.SelectedTaskLastPos());
        stl.push_back(stask);
      }
  }
  if (!stl.empty())
    db.SelectedTaskAddMany(stl, db.SelectedTaskLastPos());
}

/* ID -> task id */
QMap<db::ID, qint32> SelectTaskDialog::getNumPomodorosMap() const {
  QMap<db::ID, qint32> map;
  QList<db::SelectedTask> stl = db.SelectedTaskList();
  foreach(db::SelectedTask st, stl) {
    if (map.contains(st.task_id)) {
      map[st.task_id] += st.assigned_pomodoros;
    }
    else {
      map.insert(st.task_id, st.assigned_pomodoros);
    }
  }
  return map;
}
