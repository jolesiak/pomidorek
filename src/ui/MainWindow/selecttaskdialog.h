#ifndef SELECTTASKDIALOG_H
#define SELECTTASKDIALOG_H

#include <QDialog>
#include "database.h"

namespace Ui {
  class SelectTaskDialog;
}

class SelectTaskDialog : public QDialog
{
    Q_OBJECT
    
  public:
    explicit SelectTaskDialog(QList<db::ID> id_list, QWidget *parent = 0);
    ~SelectTaskDialog();

    void refresh();
    
  private slots:
    void on_buttonBox_accepted();

  private:

    QMap<db::ID, qint32>getNumPomodorosMap() const;

    Ui::SelectTaskDialog *ui;
    QList<db::ID> id_lists_;
    db::Database db;
};

#endif // SELECTTASKDIALOG_H
