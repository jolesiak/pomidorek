#ifndef TASKLISTWIDGET_H
#define TASKLISTWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QList>
#include <QSize>
#include <QPropertyAnimation>
#include <QMap>

#include "tasklistitem.h"
#include "database.h"


namespace Ui {
    class ListHeaderForm;
}

const qint8 headerStretchFactor = 1;

class TaskListWidget: public QWidget {
  Q_OBJECT
  public:
    explicit TaskListWidget(QWidget *parent = 0);

    void prepareItemToInsert(TaskListItem *newItem);
    void createNewItem(db::SelectedTask& task);

    db::ID getTaskIDAt(qint32 pos);
    db::ID getFirstNotCompletedTaskID() const;
    db::ID getNextNotCompletedTaskID(db::ID id) const;

  private:
    class PositionManager {
    public:
      static const qint32 NOTHING_COMPLETED;
      static const qint32 NOTHING_ACTIVE;
      static qint32 activeItemPos;
      static qint32 lastCompletedPos;
      static qint32 nextFreePos;
    };

    enum ConstrictActions {
      DoNothing,
      ClearAll,
      RemoveSingle
    };

    QList<TaskListItem *> taskList;
    Ui::ListHeaderForm *ui;
    QWidget *widgetListHeader;
    db::Database db;
    db::ID lastOpenID;
    bool enabled;

    QPropertyAnimation *animateRemoving(qint32 pos);

    bool isPositionInRange(qint32 pos);
    void animateConstriction(qint32 pos, ConstrictActions act = DoNothing);
    void animateExtension(qint32 pos);

  signals:
    void displayTaskInfo(db::ID);
    void databaseChanged();

  private slots:
    void constrictItem(qint32 pos, ConstrictActions act);
    void extendItem(qint32 pos);
    void extendItemID(db::ID id);
    void sendTaskToDisplay(qint32 pos);
    void changeItemPos(qint32 spos, qint32 dpos);
    void hideAndRemove(int pos);
    void removeAllTasks();
    void clearList();
    void removeItem(int pos);

  public slots:
    void constrictActiveAndClear();
    void removeAllCompleted();
    void deleteTaskFromList(qint32 pos);

    void resetHard();
    void disableAllItems();
    void enableAllItems();
};

#endif // TASKLISTWIDGET_H
