#ifndef TASKSUPERLISTWIDGET_H
#define TASKSUPERLISTWIDGET_H

#include <QWidget>
#include <QScrollArea>

namespace Ui {
class TaskSuperListWidget;
}

class TaskSuperListWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit TaskSuperListWidget(QWidget *parent = 0);
    ~TaskSuperListWidget();
    QScrollArea* getScrollArea();

private:
    Ui::TaskSuperListWidget *ui;
};

#endif // TASKSUPERLISTWIDGET_H
