#include "tasksuperlistwidget.h"
#include "ui_tasksuperlistwidget.h"
#include <QDebug>

TaskSuperListWidget::TaskSuperListWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskSuperListWidget)
{
    ui->setupUi(this);
}

TaskSuperListWidget::~TaskSuperListWidget()
{
    delete ui;
}

QScrollArea* TaskSuperListWidget::getScrollArea()
{
    return ui->scrollArea;
}
