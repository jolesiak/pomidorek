#include <QVBoxLayout>
#include <QPropertyAnimation>
#include <QScrollArea>
#include <QTimer>
#include <QFont>
#include <QFontDialog>
#include "ui_TaskListHeader.h"
#include <QDebug>
#include <QSignalMapper>

#include "tasklistwidget.h"

/* Initialize PositionManager static fields */
qint32 TaskListWidget::PositionManager::activeItemPos;
qint32 TaskListWidget::PositionManager::lastCompletedPos;
qint32 TaskListWidget::PositionManager::nextFreePos;
const qint32 TaskListWidget::PositionManager::NOTHING_COMPLETED = -1;
const qint32 TaskListWidget::PositionManager::NOTHING_ACTIVE = -1;

TaskListWidget::TaskListWidget(QWidget *parent) :
  QWidget(parent)
{
  setObjectName("TaskListWidget");

  /* Add main layout for this widget */
  QVBoxLayout *layout = new QVBoxLayout(this);
  layout->setSpacing(0);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->setSizeConstraint(QLayout::SetMinAndMaxSize);
  setLayout(layout);
  PositionManager::activeItemPos = PositionManager::NOTHING_ACTIVE;
  PositionManager::lastCompletedPos = PositionManager::NOTHING_COMPLETED;
  PositionManager::nextFreePos = 0;
  lastOpenID = db::NO_ID;
  enabled = true;

  QList<db::SelectedTask> tasks = db.SelectedTaskList();
  foreach(db::SelectedTask task, tasks) {
    createNewItem(task);
  }
}

void TaskListWidget::prepareItemToInsert(TaskListItem *newItem) {
  /* Create all necessary connections */
  connect(newItem, SIGNAL(clicked(qint32)),
          this, SLOT(extendItem(qint32)));
//  connect(newItem, SIGNAL(clicked(qint32)),
//          this, SLOT(sendTaskToDisplay(qint32)));
  connect(newItem, SIGNAL(itemPosChanged(qint32,qint32)),
          this, SLOT(changeItemPos(qint32,qint32)));
  connect(newItem, SIGNAL(deleteTask(qint32)),
          this, SLOT(deleteTaskFromList(qint32)));

  /* Give the next free position and append item to the list */
  newItem->set_position(PositionManager::nextFreePos++);
  if (newItem->get_stask_id() == lastOpenID) {
    PositionManager::activeItemPos = newItem->get_position();
    newItem->setMinimumHeight(TaskItemNamespace::MaxItemHeight);
    newItem->setMaximumHeight(TaskItemNamespace::MaxItemHeight);
    emit displayTaskInfo(newItem->get_task_id());
  }
  taskList.append(newItem);


}

/* Create a new item and add it to the list layout */
void TaskListWidget::createNewItem(db::SelectedTask& task) {
  TaskListItem* newItem = new TaskListItem(task, this);
  newItem->initializeInnerWidgets();
  prepareItemToInsert(newItem);
  layout()->addWidget(newItem);
  if (!enabled)
    newItem->disableItem();
}

void TaskListWidget::resetHard() {
  foreach(TaskListItem *item, taskList) {
    layout()->removeWidget(item);
    item->deleteLater();
  }
  taskList.clear();

  PositionManager::nextFreePos = 0;
  QList<db::SelectedTask> stl = db.SelectedTaskList();
  foreach(db::SelectedTask st, stl) {
    createNewItem(st);
  }
}

void TaskListWidget::disableAllItems()
{
  //qDebug() << "disable";
  enabled = false;
  foreach(TaskListItem * item, taskList)
    item->disableItem();
}

void TaskListWidget::enableAllItems()
{
  //qDebug() << "enable";
  enabled = true;
  foreach(TaskListItem * item, taskList)
    item->enableItem();
}

void TaskListWidget::deleteTaskFromList(qint32 pos)
{
  disableAllItems();
  hideAndRemove(pos);
}

db::ID TaskListWidget::getTaskIDAt(qint32 pos) {
  if (isPositionInRange(pos)){
    return taskList.at(pos)->get_task_id();
  }
  else {
    //qDebug() << "ERROR: TaskListWidget::getTaskIDAt";
    return db::NO_ID;
  }
}

db::ID TaskListWidget::getFirstNotCompletedTaskID() const
{
  db::ID returnID = db::NO_ID;
  QList<db::SelectedTask> stl = db.SelectedTaskList();
  QList<db::SelectedTask>::iterator it = stl.begin();

  while(it != stl.end()) {
    if (it->assigned_pomodoros > it->completed_pomodoros) {
      returnID = it->id;
      break;
    }
    ++it;
  }
  return returnID;
}

db::ID TaskListWidget::getNextNotCompletedTaskID(db::ID id) const
{
  bool afterID = false;
  db::ID returnID = db::NO_ID;
  QList<db::SelectedTask> stl = db.SelectedTaskList();
  QList<db::SelectedTask>::iterator it = stl.begin();
  while(it != stl.end()) {
    if (afterID) {
      if (it->assigned_pomodoros > it->completed_pomodoros) {
        returnID = it->id;
        break;
      }
    }
    else {
      afterID = (it->id == id);
    }
    ++it;
  }
  return returnID;
}

/* Emits a singal after an item choice */
void TaskListWidget::sendTaskToDisplay(qint32 pos) {
  emit displayTaskInfo(getTaskIDAt(pos));
}

void TaskListWidget::changeItemPos(qint32 spos, qint32 dpos) {
  //change position on the list
  if (isPositionInRange(dpos)) {
    taskList.swap(spos, dpos);
    //change position on the items
    taskList.at(spos)->set_position(spos);
    taskList.at(dpos)->set_position(dpos);

    //niestety narazie tak
    foreach (TaskListItem *t, taskList) {
      this->layout()->removeWidget(t);
    }
    foreach (TaskListItem *t, taskList) {
      this->layout()->addWidget(t);
    }
    PositionManager::activeItemPos = dpos;
  }
  else {
    //qDebug() << "wrong dpos" << taskList.count() << dpos;
  }
}

void TaskListWidget::removeItem(int pos) {
  if (isPositionInRange(pos))
    db.SelectedTaskRemove(taskList.at(pos)->get_stask_id());
  enableAllItems();
}

/* ================================================================================= */
/* ===                               ITEMS ANIMATIONS                            === */
/* ================================================================================= */

/* Local class for animations creation */
class AnimValues {
  public:
    qint32 duration;
    qint32 startVal;
    qint32 endVal;
    QEasingCurve curve;
};

/* Local function for animations initialization */
static void setAnimationProperties(QPropertyAnimation *anim, AnimValues &vals) {
  anim->setEasingCurve(vals.curve);
  anim->setDuration(vals.duration);
  anim->setStartValue(vals.startVal);
  anim->setEndValue(vals.endVal);
}

/* Run animations responsible for an item extension */
void TaskListWidget::animateExtension(qint32 pos) {
  if (isPositionInRange(pos)) {
    bool enabled_cp = enabled;
    if(enabled_cp)
      disableAllItems();
    TaskListItem *itemExtension = taskList.at(pos);

    QPropertyAnimation *animExtension = new QPropertyAnimation(itemExtension,
        "minimumHeight");
    QPropertyAnimation *animExtension2 = new QPropertyAnimation(itemExtension,
        "maximumHeight");

    /* Prepare animation properties */
    AnimValues animationVals;
    animationVals.curve = QEasingCurve::InCirc;
    animationVals.duration = TaskItemNamespace::AnimTime;
    animationVals.startVal = TaskItemNamespace::MinItemHeight;
    animationVals.endVal = TaskItemNamespace::MaxItemHeight;
    setAnimationProperties(animExtension, animationVals);
    setAnimationProperties(animExtension2, animationVals);

    /* Start both animations */
    animExtension->start(QAbstractAnimation::DeleteWhenStopped);
    animExtension2->start(QAbstractAnimation::DeleteWhenStopped);
    if(enabled_cp)
      connect(animExtension, SIGNAL(finished()),
            this, SLOT(enableAllItems()));
  }
}

/* Run animations responsible for an item constriction */
void TaskListWidget::animateConstriction(qint32 pos, ConstrictActions act) {
  if (isPositionInRange(pos)) {
    if (PositionManager::activeItemPos == pos)
      PositionManager::activeItemPos = PositionManager::NOTHING_ACTIVE;

    TaskListItem *itemConstriction = taskList.at(pos);

    QPropertyAnimation *animConstriction = new QPropertyAnimation(
        itemConstriction, "minimumHeight");
    QPropertyAnimation *animConstriction2 = new QPropertyAnimation(
        itemConstriction, "maximumHeight");

    /* Prepare animation properties */
    AnimValues animationVals;
    animationVals.curve = QEasingCurve::InCirc;
    animationVals.duration = TaskItemNamespace::AnimTime;
    animationVals.startVal = TaskItemNamespace::MaxItemHeight;
    if(act == RemoveSingle)
      animationVals.endVal = 0;
    else
      animationVals.endVal = TaskItemNamespace::MinItemHeight;
    setAnimationProperties(animConstriction, animationVals);
    setAnimationProperties(animConstriction2, animationVals);

    /* Start both animations */
    animConstriction->start(QAbstractAnimation::DeleteWhenStopped);
    animConstriction2->start(QAbstractAnimation::DeleteWhenStopped);

    if (act == ClearAll) {
      connect(animConstriction, SIGNAL(finished()), this, SLOT(removeAllTasks()));
    }
    else if (act == RemoveSingle) {
      QSignalMapper *signalMapper = new QSignalMapper(this);
      signalMapper->setMapping(animConstriction, qint32(pos));

      connect(animConstriction, SIGNAL(finished()),
              signalMapper, SLOT(map()));
      connect(signalMapper, SIGNAL(mapped(int)),
              this, SLOT(animateRemoving(int)));
    }
  }
}

/* Run animations responsible for an item removing */
QPropertyAnimation * TaskListWidget::animateRemoving(qint32 pos) {
  TaskListItem *itemToRemove = taskList.at(pos);

  QPropertyAnimation *animRemoving = new QPropertyAnimation(itemToRemove,
      "minimumWidth");
  QPropertyAnimation *animRemoving2 = new QPropertyAnimation(itemToRemove,
      "maximumWidth");

  /* Prepare animation properties */
  AnimValues animationVals;
  animationVals.curve = QEasingCurve::InCirc;
  animationVals.duration = TaskItemNamespace::AnimTime;
  animationVals.startVal = itemToRemove->width();
  animationVals.endVal = 0;
  setAnimationProperties(animRemoving, animationVals);
  setAnimationProperties(animRemoving2, animationVals);

  /* Start both animations */
  animRemoving->start(QPropertyAnimation::DeleteWhenStopped);
  animRemoving2->start(QPropertyAnimation::DeleteWhenStopped);

  return animRemoving2;
}

/* ================================================================================= */
/* ===                             ANIMATIONS LOGIC                              === */
/* ================================================================================= */

/* Hides all items but without removing */
void TaskListWidget::removeAllTasks() {
  QPropertyAnimation *anim = NULL;
  foreach (TaskListItem *item, taskList) {
    anim = animateRemoving(item->get_position());
  }
  connect(anim, SIGNAL(finished()), this, SLOT(clearList()));
}

void TaskListWidget::clearList() {
  QList<db::ID> stask_id_list;
  foreach(const TaskListItem *tli, taskList) {
    stask_id_list.push_back(tli->get_stask_id());
  }

  db.SelectedTaskRemoveMany(stask_id_list);
}

/* The slot executed when a user pushes "Clean list" button" */
void TaskListWidget::constrictActiveAndClear() {
  if (PositionManager::activeItemPos != PositionManager::NOTHING_ACTIVE) {
    constrictItem(PositionManager::activeItemPos, ClearAll);
  }
  else {
    removeAllTasks();
  }
}

/* Extend item if the given position does not refer to the active item */
void TaskListWidget::extendItem(qint32 pos) {
  if (isPositionInRange(pos) && PositionManager::activeItemPos != pos) {
    animateExtension(pos);
    if (PositionManager::activeItemPos != PositionManager::NOTHING_ACTIVE) {
      animateConstriction(PositionManager::activeItemPos);
    }
    PositionManager::activeItemPos = pos;
    lastOpenID = taskList.at(pos)->get_stask_id();
    sendTaskToDisplay(pos);
  }

}

void TaskListWidget::extendItemID(db::ID id)
{
  //qDebug() << "extednItemID" << id;
  qint32 pos = -1;
  foreach(TaskListItem *tli, taskList) {
    if(tli->get_stask_id() == id)
       pos = tli->get_position();
  }
  extendItem(pos);
}

/* Constrict item if the given position refers to the active item */
void TaskListWidget::constrictItem(qint32 pos,
                                   ConstrictActions act = DoNothing) {
  if (isPositionInRange(pos) && PositionManager::activeItemPos == pos) {
    animateConstriction(PositionManager::activeItemPos, act);
  }
}

/* ================================================================================= */
/* ===                             NEXT TASK`S CHOICE                            === */
/* ================================================================================= */

/* Delay for extending the next item (to give enough time for updating item display) */
static const qint32 SINGLE_SHOT_TIME = 1000;

/* Check if this position is in range */
bool TaskListWidget::isPositionInRange(qint32 pos) {
  return (pos >= 0 && pos < taskList.count());
}

void TaskListWidget::removeAllCompleted()
{
   QList<db::ID> stask_id_list;
   foreach(const TaskListItem *tli, taskList) {
     if (tli->isCompleted())
      stask_id_list.push_back(tli->get_stask_id());
   }

   db.SelectedTaskRemoveMany(stask_id_list);
}

void TaskListWidget::hideAndRemove(qint32 pos) {
  if (isPositionInRange(pos)) {
    if (PositionManager::activeItemPos == pos)
      PositionManager::activeItemPos = PositionManager::NOTHING_ACTIVE;

    TaskListItem *itemConstriction = taskList.at(pos);

    QPropertyAnimation *animConstriction = new QPropertyAnimation(
        itemConstriction, "minimumHeight");
    QPropertyAnimation *animConstriction2 = new QPropertyAnimation(
        itemConstriction, "maximumHeight");

    /* Prepare animation properties */
    AnimValues animationVals;
    animationVals.curve = QEasingCurve::InCirc;
    animationVals.duration = TaskItemNamespace::AnimTime;
    animationVals.startVal = itemConstriction->minimumHeight();
    animationVals.endVal = 0;
    setAnimationProperties(animConstriction, animationVals);
    setAnimationProperties(animConstriction2, animationVals);

    /* Start both animations */
    animConstriction->start(QAbstractAnimation::DeleteWhenStopped);
    animConstriction2->start(QAbstractAnimation::DeleteWhenStopped);

    QSignalMapper *signalMapper = new QSignalMapper(this);
    signalMapper->setMapping(animConstriction, qint32(pos));

    connect(animConstriction, SIGNAL(finished()),
            signalMapper, SLOT(map()));
    connect(signalMapper, SIGNAL(mapped(int)),
            this, SLOT(removeItem(int)));
  }
  else {
     enableAllItems();
  }
}
