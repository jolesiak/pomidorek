#ifndef TASKSLISTITEM_H
#define TASKSLISTITEM_H

#include <QWidget>
#include <QLabel>
#include <QDebug>
#include <QMenu>
#include <QPushButton>
#include "database.h"

namespace TaskItemNamespace {
  enum ItemHeight {
    MinItemHeight = 40,
    MaxItemHeight = 200
  };

  enum ItemDuration {
    AnimTime = 1000
  };
}

namespace Ui {
    class TaskItemForm;
}

class TaskListItem : public QWidget
{
  Q_OBJECT

public:
  TaskListItem(db::SelectedTask &stask, QWidget *parent = 0);
  ~TaskListItem();

  void initializeInnerWidgets();
  void getProgressStyle(QString &style);
  bool isSessionCompleted();

  QPushButton *getUpButton() const;
  QPushButton *getDownButton() const;
  qint32 get_position() const { return position_; }
  db::ID get_task_id() const {return task_id_;}
  db::ID get_stask_id() const {return stask_id_;}

  void set_position(qint32 pos) { position_ = pos; }
  bool isCompleted() const { return assigned_pomodoros_ == completed_pomodoros_; }

protected:
  void mousePressEvent(QMouseEvent *e);
  void resizeEvent(QResizeEvent *e);
  void showEvent(QShowEvent */* e */);

private:
  void setHeaderTagColor();
  void updateStatusLabel();
  void updateProgressChunks();

  QString getShinyGradStyleSheet(QString color);

  Ui::TaskItemForm *ui;
  db::Database db;

  qint32 position_;
  qint32 assigned_pomodoros_;
  qint32 completed_pomodoros_;
  qint32 pbar_cell_width_;
  db::ID task_id_;
  db::ID stask_id_;
  bool is_enabled_;
  bool is_running_;

signals:
  void clicked(qint32);
  void removeSignal(qint32);
  void itemPosChanged(qint32 spos, qint32 dpos);
  void deleteTask(qint32 position_);

public slots:
  void disableItem();
  void enableItem();

private slots:
  void onUpButtonClicked();
  void onDownButtonClicked();
  void deleteButtonClicked();
};

#endif // TASKSLISTITEM_H
