#include "tasklistitem.h"
#include "basic_functions.h"
#include "ui_TaskListItem.h"

TaskListItem::TaskListItem(db::SelectedTask &stask, QWidget *parent) :
    QWidget(parent), ui(new Ui::TaskItemForm)
{
  ui->setupUi(this);
  setObjectName("TaskListItem");

  task_id_ = stask.task_id;
  stask_id_ = stask.id;
  is_enabled_ = true;
  is_running_ = false;
  pbar_cell_width_ = 1;

  /* Set size limits */
  this->setMinimumHeight(TaskItemNamespace::MinItemHeight);
  this->setMaximumHeight(TaskItemNamespace::MinItemHeight);
  ui->DeskWidget->setMinimumHeight(
        TaskItemNamespace::MaxItemHeight - TaskItemNamespace::MinItemHeight);
  ui->DeskWidget->setMinimumHeight(
        TaskItemNamespace::MaxItemHeight - TaskItemNamespace::MinItemHeight);

  /* Set number of pomodoros for the sesion */
  assigned_pomodoros_ = stask.assigned_pomodoros;
  completed_pomodoros_ = stask.completed_pomodoros;

  setHeaderTagColor();
  updateStatusLabel();

  connect(ui->downButton, SIGNAL(clicked()), this, SLOT(onDownButtonClicked()));
  connect(ui->upButton, SIGNAL(clicked()), this, SLOT(onUpButtonClicked()));
  connect(ui->deleteButton, SIGNAL(clicked()), this, SLOT(deleteButtonClicked()));
}

TaskListItem::~TaskListItem() {
    delete ui;
}

void TaskListItem::initializeInnerWidgets() {
  db::Task task = db.TaskGet(task_id_);
  db::SelectedTask stask = db.SelectedTaskGet(stask_id_);
  //makeTextEllipsis(ui->nameLabel, task.name);
  ui->nameLabel->setText( task.name);
  ui->descTextEdit->setText(task.description);
  ui->progressBar->setMaximum(stask.assigned_pomodoros);
  ui->progressBar->setValue(stask.completed_pomodoros);
  ui->statusLabel->setText(QString("%1/%2")
                          .arg(stask.completed_pomodoros)
                          .arg(stask.assigned_pomodoros));
}

QPushButton *TaskListItem::getUpButton() const {
  return ui->upButton;
}
QPushButton *TaskListItem::getDownButton() const {
  return ui->downButton;
}

bool TaskListItem::isSessionCompleted() {
  return assigned_pomodoros_ == completed_pomodoros_;
}

void TaskListItem::disableItem()
{
    is_enabled_ = false;
    ui->downButton->setEnabled(false);
    ui->upButton->setEnabled(false);
    ui->deleteButton->setEnabled(false);
}

void TaskListItem::enableItem()
{
    is_enabled_ = true;
    ui->downButton->setEnabled(true);
    ui->upButton->setEnabled(true);
    ui->deleteButton->setEnabled(true);
}

/* ================================================================================= */
/* ===                          MOUSE PRESS HANDLING                             === */
/* ================================================================================= */

/*
 * Handle mouse press. If left button has been clicked - extend the item, otherwise show
 * the context menu assigned for the clicked item
 */
void TaskListItem::mousePressEvent(QMouseEvent *e) {
    if (is_enabled_) {
      if (e->button() == Qt::LeftButton) {
        emit clicked(position_);
      }
      else if (e->button() == Qt::RightButton) {
        //contextMenu->popup(QCursor::pos());
      }
    }
}

void TaskListItem::onUpButtonClicked() {
  db.SelectedTaskMoveUp(stask_id_);
}

void TaskListItem::onDownButtonClicked() {
  db.SelectedTaskMoveDown(stask_id_);
}

void TaskListItem::deleteButtonClicked() {
   //qDebug()<<"TaskListItem::deleteButtonClicked() " << position_;
   emit deleteTask(position_);
}

/* ================================================================================= */
/* ===                             RESIZE HANDLING                               === */
/* ================================================================================= */

static QString tempStyle;

/* Return a string with modified style sheets for item */
void TaskListItem::getProgressStyle(QString &style) {
  QString form =
      "QProgressBar:horizontal {"
      "border: 1px solid gray;"
      "border-radius: 3px;"
      "background: white;"
      "padding: 1px;"
      "text-align: right;"
      "}"
      "QProgressBar::chunk:horizontal {"
      "background: qlineargradient(spread:reflect, x1:0.00471698,"
      "y1:0.488, x2:0, y2:1,stop:0.660377 rgba(0, 208, 24, 255),"
      "stop:1 rgba(255, 255, 255, 255));"
      "margin-right: 2px;"
      "width: %1 px;}";

  style = form.arg(QString::number(pbar_cell_width_ - 1));
}

/* Update progress bar chunks when widget`s width is changing */
void TaskListItem::updateProgressChunks() {
  if (assigned_pomodoros_ > 0) {
    if (pbar_cell_width_ != (ui->progressBar->width() / assigned_pomodoros_)) {
      pbar_cell_width_ = (ui->progressBar->width() / assigned_pomodoros_);
      getProgressStyle(tempStyle);
      ui->progressBar->setStyleSheet(tempStyle);
    }
  }
}

QString TaskListItem::getShinyGradStyleSheet(QString c){
  QColor color(c);
  QString itemH_bg =
      "background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, "
      "stop:0 rgba(%4, %5, %6, 255), "       // r2 g2 b2
      "stop:0.5 rgba(%7, %8, %9, 255), "     // r3 g3 b3
      "stop:0.51 rgba(%1, %2, %3, 255), "    // r1 g1 b1
      "stop:1 rgba(%4, %5, %6, 255)); "       // r2 g2 b2
      "border-right: 1px solid black;"
      "border-top: 1px solid black;"
      "border-bottom: 1px solid black;";
  int r1, r2, r3, g1, g2, g3, b1, b2, b3, p1, p2, q1, q2;

  r1 = color.red();
  g1 = color.green();
  b1 = color.blue();
  p1 = 3;
  q1 = 2;
  p2 = 2;
  q2 = 7;

  r2 = qRound((p1*r1+q1*255)/(p1+q1));
  g2 = qRound((p1*g1+q1*255)/(p1+q1));
  b2 = qRound((p1*b1+q1*255)/(p1+q1));
  r3 = qRound((p2*r1+q2*r2)/(p2+q2));
  g3 = qRound((p2*g1+q2*g2)/(p2+q2));
  b3 = qRound((p2*b1+q2*b2)/(p2+q2));

  return itemH_bg.arg(QString::number(r1), QString::number(g1),
                      QString::number(b1), QString::number(r2),
                      QString::number(g2), QString::number(b2),
                      QString::number(r3), QString::number(g3),
                      QString::number(b3));
}

void TaskListItem::resizeEvent(QResizeEvent * /* e */) {
  updateProgressChunks();
//  if (e->oldSize().width() > e->size().width())
//  preventTextOverFlow();
}

void TaskListItem::showEvent(QShowEvent * /* e */) {
  updateProgressChunks();
  //  preventTextOverFlow();
}

void TaskListItem::setHeaderTagColor() {
  db::Tag mt = db.TaskMainTag(task_id_);

  if(QColor::isValidColor(mt.color)) {
    ui->ColorWidget->setStyleSheet(getShinyGradStyleSheet(mt.color));
  }
  else {
      ui->ColorWidget->setStyleSheet(getShinyGradStyleSheet("light gray"));
  }
}

void TaskListItem::updateStatusLabel() {
  if(is_running_){
    ui->statusIconLabel->setText(QString::fromUtf8("\360\237\217\206"));
  }
  else if(isSessionCompleted()) {
    ui->statusIconLabel->setText(QString::fromUtf8("\360\237\217\206"));
  }
  else {
    ui->statusIconLabel->setText(QString::fromUtf8("\360\237\225\224"));
  }
}

