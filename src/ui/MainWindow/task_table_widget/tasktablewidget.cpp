#include "tasktablewidget.h"
#include <QFontDatabase>
#include "task_table_model/tasktablemodel.h"
#include "database.h"
#include <QtGui>
#include "edittaskdialog.h"

#include "selecttaskdialog.h"

TaskTableWidget::TaskTableWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TaskTableWidget)
{
  setObjectName("TaskTableWidget");
    ui->setupUi(this);

    filterPatternLineEdit = new QLineEdit;
    filterPatternLineEdit-> setText("");

    filterPatternLabel = new QLabel(tr("&Filter"));
    filterPatternLabel -> setBuddy(filterPatternLineEdit);

    filterSyntaxComboBox = new QComboBox;
    filterSyntaxComboBox -> addItem(tr("Regular expression"), QRegExp::RegExp);
    filterSyntaxComboBox -> addItem(tr("Wildcard"), QRegExp::Wildcard);
    filterSyntaxComboBox -> addItem(tr("Fixed String"), QRegExp::FixedString);

    fromDateTimeEdit = new QDateTimeEdit;
    fromDateTimeEdit -> setDateTime(QDateTime::currentDateTime());
    fromDateTimeEdit -> setCalendarPopup(true);
    fromLabel = new QLabel(tr("F&rom"));
    fromLabel -> setBuddy(fromDateTimeEdit);

    toDateTimeEdit = new QDateTimeEdit;
    toDateTimeEdit -> setDateTime(QDateTime::currentDateTime().addDays(7));
    toDateTimeEdit -> setCalendarPopup(true);
    toLabel = new QLabel(tr("&To"));
    toLabel -> setBuddy(toDateTimeEdit);

    connect (filterPatternLineEdit, SIGNAL(textChanged(QString)),
             this, SLOT(textFilterChanged()));
    connect (filterSyntaxComboBox, SIGNAL(currentIndexChanged(int)),
             this, SLOT(textFilterChanged()));
    connect (fromDateTimeEdit, SIGNAL(dateChanged(QDate)),
             this, SLOT(dateFilterChanged()));
    connect (toDateTimeEdit, SIGNAL(dateChanged(QDate)),
             this, SLOT(dateFilterChanged()));

    QGridLayout *filterLayout = new QGridLayout;
    filterLayout -> addWidget(filterPatternLabel, 0, 0);
    filterLayout -> addWidget(filterPatternLineEdit, 0, 1, 1, 25);
    //filterLayout -> addWidget(filterSyntaxComboBox, 0, 2);
    filterLayout -> addWidget(fromLabel, 1, 0);
    filterLayout -> addWidget(fromDateTimeEdit, 1, 1, 1, 10);
    filterLayout -> addWidget(toLabel, 1, 15);
    filterLayout -> addWidget(toDateTimeEdit, 1, 16, 1, 10);


    filterGroupBox = new QGroupBox;
    filterGroupBox -> setLayout(filterLayout);

    ui->verticalLayout->addWidget(filterGroupBox);

    ui->tableView->setCornerButtonEnabled(false);
    ui->tableView->horizontalHeader()->setHighlightSections(false);

    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(ui->tableView, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(table_customContexMenu(QPoint)));
}

TaskTableWidget::~TaskTableWidget()
{
    delete ui;
}

void TaskTableWidget::textFilterChanged()
{
//    QRegExp::PatternSyntax syntax =
//            QRegExp::PatternSyntax(filterSyntaxComboBox->itemData(
//                     filterSyntaxComboBox->currentIndex()).toInt());
    QRegExp::PatternSyntax syntax = QRegExp::Wildcard;
    QRegExp regExp (filterPatternLineEdit->text(), Qt::CaseInsensitive, syntax);
    proxyModel->setFilterRegExp(regExp);
}

void TaskTableWidget::dateFilterChanged()
{
    proxyModel->setFilterMinDeadline(fromDateTimeEdit->date());
    proxyModel->setFilterMaxDeadline(toDateTimeEdit->date());
}

void TaskTableWidget::table_customContexMenu(QPoint /* pos */)
{
  //qDebug() << "TaskTableWidget::table_customContexMenu(QPoint pos)";
}

void TaskTableWidget::aAdd()
{
  QList<db::ID> id_list = getSelectedIDs();
  SelectTaskDialog *stg = new SelectTaskDialog(id_list, this);
  stg->show();
}

void TaskTableWidget::aDelete()
{
    QList<db::ID> id_list = getSelectedIDs();

    db.TaskRemoveMany(id_list);
}

void TaskTableWidget::aEdit()
{
    QList<db::ID> id_list = getSelectedIDs();
    if(id_list.size() == 1) {
      db::ID task_id = id_list.first();
      //qDebug() << "TaskTableWidget::aEdit()"<<" id: " << task_id;

      if (task_id != db::NO_ID) {
          db::Task task = db.TaskGet(task_id);
          EditTaskDialog * editTG = new EditTaskDialog(task, this);
          editTG->show();
      }
   }
}

void TaskTableWidget::aRemove()
{
  QList<db::ID> id_list = getSelectedIDs();

  foreach(const db::ID& id, id_list) {
      db.TaskRemoveDeleted(id);
  }
}

void TaskTableWidget::aRestore()
{
  QList<db::ID> id_list = getSelectedIDs();

  foreach(const db::ID& id, id_list) {
      db.TaskRestoreDeleted(id);
  }
}

QList<db::ID> TaskTableWidget::getSelectedIDs()
{
   QItemSelectionModel * model = ui->tableView->selectionModel();
   QModelIndexList index_list = model->selectedRows();
   QList<db::ID> id_list;
   foreach(const QModelIndex &md, index_list) {
       //qDebug() << md.data(Qt::UserRole);
       id_list.push_back(db.QVariantToID(md.data(Qt::UserRole)));
   }

   return id_list;
}

bool TaskTableWidget::isSelectionModel(QTableView *table)
{
    QItemSelectionModel * model = table->selectionModel();
    if (model) {
        return true;
    }
    else {
        return false;
    }
}

bool TaskTableWidget::isSingleSelection()
{
    QItemSelectionModel * model = ui->tableView->selectionModel();
    QModelIndexList iList = model->selectedRows();
    return (iList.size() == 1);
}
