#ifndef TASKALLWIDGET_H
#define TASKALLWIDGET_H

#include <QWidget>
#include <QtCore>
#include <QtGui>
#include <QVector>
#include "database.h"
#include "task_proxy_model/taskproxymodel.h"
#include "task_table_model/tasktablemodel.h"
#include "ui_tasktablewidget.h"
#include <QTableView>
#include <QAction>

class TaskTableWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TaskTableWidget(QWidget *parent = 0);
    virtual ~TaskTableWidget();

    TaskTableModel *getBaseModel() { return baseModel; }

protected:
    virtual void createActions() = 0;

    bool isSelectionModel(QTableView * table);
    bool isSingleSelection();

    db::Database db;
    Ui::TaskTableWidget *ui;
    TaskTableModel *baseModel;
    TaskProxyModel *proxyModel;

    QLineEdit *filterPatternLineEdit;
    QLabel *filterPatternLabel;
    QComboBox *filterSyntaxComboBox;
    QDateTimeEdit *fromDateTimeEdit;
    QDateTimeEdit *toDateTimeEdit;
    QLabel *fromLabel;
    QLabel *toLabel;
    QGroupBox *filterGroupBox;

    QVector<db::Task> allTasks;

signals:
    void addTaskTODO(db::SelectedTask & task);

protected slots:
    virtual void table_customContexMenu(QPoint /* pos */) = 0;

private slots:
    void textFilterChanged();
    void dateFilterChanged();

    void aAdd();
    void aDelete();
    void aEdit();
    void aRemove();
    void aRestore();

private:
    QList<db::ID> getSelectedIDs();
};

#endif // TASKALLWIDGET_H
