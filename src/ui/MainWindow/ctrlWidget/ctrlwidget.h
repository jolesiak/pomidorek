#ifndef CTRLWIDGET_H
#define CTRLWIDGET_H

#include <QWidget>
#include <QPushButton>
#include "ui_ctrlwidget.h"
#include "gd_thread.h"
#include "gd_prompt_no_buttons.h"
#include "gd_input_dialog.h"

namespace Ui {
  class CtrlWidget;
}

class CtrlWidget : public QWidget
{
  Q_OBJECT
  
public:
  explicit CtrlWidget(QWidget *parent = 0);
  ~CtrlWidget();
  
  QPushButton* getRunButton() const {return ui->runButton; }
  QPushButton* getRmButton() const {return ui->rmButton; }
  QPushButton* getSyncButton() const {return ui->syncButton; }
  QPushButton* getClearCmplButton() const {return ui->clearCmplButton; }

private:
  Ui::CtrlWidget *ui;

  GD_Thread* thr;

  void GD_call(Settings_Namespace::GD_Command_Type cmd);

signals:
  void syncFinished();
  void syncFailed();

public slots:
  void GD_sync();
  void GD_sync_finish();
  void GD_sync_fail(QString err);
};

#endif // CTRLWIDGET_H
