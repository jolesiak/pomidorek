#include "ctrlwidget.h"
#include "ui_ctrlwidget.h"
#include <QThread>
#include <QDesktopServices>
#include <unistd.h>
#include <cstdio>

CtrlWidget::CtrlWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::CtrlWidget),
  thr(new GD_Thread)
{
  setObjectName("CtrlWidget");
  ui->setupUi(this);
  thr->setParent(this);

  connect(ui->syncButton, SIGNAL(clicked()), this, SLOT(GD_sync()));
  connect(thr, SIGNAL(syncOk()), this, SLOT(GD_sync_finish()));
  connect(thr, SIGNAL(syncFail(QString)), this, SLOT(GD_sync_fail(QString)));
}

CtrlWidget::~CtrlWidget()
{
  delete ui;
}



void CtrlWidget::GD_sync() {
    if(!thr->is_active()){
        thr->start();
    }
}

void CtrlWidget::GD_sync_finish()
{
  emit syncFinished();
}

void CtrlWidget::GD_sync_fail(QString err)
{
  QMessageBox mb;
  mb.setText("Synchronization failed!\nYou may try again if you want");
  mb.setStandardButtons(QMessageBox::Ok);
  mb.setDetailedText(err);
  mb.setWindowTitle("Synchronization failed");
  mb.setIcon(QMessageBox::Critical);
  mb.exec();
  emit syncFailed();
}
