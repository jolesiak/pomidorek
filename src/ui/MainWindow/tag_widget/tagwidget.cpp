#include <QColor>
#include <QMessageBox>
#include <QDebug>

#include "ui_tagwidget.h"
#include "managetagdialog.h"
#include "tagwidget.h"

#include "basic_functions.h"

TagWidget::TagWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TagWidget)
{
    ui->setupUi(this);
    setObjectName("TagWidget");

    model = new QStandardItemModel(this);

    refreshList();

    ui->listView->setModel(model);

    connect(&mtg, SIGNAL(tagListChanged()),  this, SLOT(refreshList()));
}

TagWidget::~TagWidget()
{
    delete ui;
}

void TagWidget::refreshList()
{
    model->clear();

    QVariant newID;
    QList<db::Tag> tl = db.TagList();
    for(const db::Tag &tag : tl) {
        QIcon icon = createUserIcon(50, tag.color, true);

        QStandardItem * newItem = new QStandardItem(icon, tag.name);

        newID = db.IDToQVariant(tag.id);
        newItem->setData(newID);

        model->appendRow(newItem);
    }
}


void TagWidget::on_addTagButton_clicked()
{
    mtg.delFocusOnTag();
    mtg.setWindowTitle("Add tag");
    mtg.show();
}

void TagWidget::on_deleteTagButton_clicked()
{
    QMessageBox msbBox;
    QModelIndexList iList = ui->listView->selectionModel()->selectedIndexes();
    if (iList.size() == 0) {
      msbBox.setText("Please select tag to remove first.");
      msbBox.exec();
    }
    else {
        QStandardItem * item = model->itemFromIndex(iList.first());
        db::ID foc_ID = db.QVariantToID(item->data(Qt::UserRole+1));
        db.TagRemove(foc_ID);
    }
}

void TagWidget::on_editTagButton_clicked()
{
    QModelIndexList iList = ui->listView->selectionModel()->selectedIndexes();
    if (iList.size() == 0) {
        //qDebug() <<  "nic nie zaznaczono";
    }
    else {
        QStandardItem * item = model->itemFromIndex(iList.first());
        db::ID foc_ID = db.QVariantToID(item->data(Qt::UserRole+1));
        //qDebug() << "rozpatruje" << foc_ID;
        mtg.focusOnTag(foc_ID);
        mtg.setWindowTitle("Edit tag");
        mtg.show();
    }
}

