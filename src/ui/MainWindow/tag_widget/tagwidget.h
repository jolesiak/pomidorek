#ifndef TAGWIDGET_H
#define TAGWIDGET_H

#include <QWidget>
#include "database.h"
#include "managetagdialog.h"
#include <QStandardItemModel>

namespace Ui {
class TagWidget;
}

class TagWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit TagWidget(QWidget *parent = 0);
    ~TagWidget();
    
private slots:
    void refreshList();

    void on_deleteTagButton_clicked();
    void on_editTagButton_clicked();
    void on_addTagButton_clicked();

private:
    Ui::TagWidget *ui;
    db::Database db;
    ManageTagDialog mtg;
    QStandardItemModel * model;
};

#endif // TAGWIDGET_H
