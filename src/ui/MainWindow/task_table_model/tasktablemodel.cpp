#include "tasktablemodel.h"
#include <QFont>
#include <QIcon>
#include <QBrush>
#include <QDebug>
#include <QTextCodec>
#include <QStandardItem>

#include "database.h"

TaskTableModel::TaskTableModel(ModelType mt, QObject *parent) :
    QAbstractTableModel(parent)
{
  mt_ = mt;
  switch (mt_) {
    case ALL :
      itemData = db.TaskList();
      break;
    case ACTIVE :
      itemData = db.TaskActiveList();
      break;
    case COMPLETED :
      itemData = db.TaskCompletedList();
      break;
    case DELETED :
      itemData = db.TaskDeletedList();
      break;
    default :
      ;
      //qDebug() << "Wrong model type";
  }
}

int TaskTableModel::rowCount(const QModelIndex &parent) const
{
  if (parent.isValid())
    return 0;
  else
    return itemData.size();
}

int TaskTableModel::columnCount(const QModelIndex &parent) const
{
  if (parent.isValid())
    return 0;
  else
    return 8;
}

QVariant TaskTableModel::data(const QModelIndex &index, int role) const
{
  QString pomosRatio;
  int row = index.row();
  int col = index.column();
  db::Task task = itemData.at(row);
  QVariant tid = db.IDToQVariant(task.id);

  switch (role){
    case Qt::DisplayRole:
      switch (col){
        case STATUS:
          if (task.status.testFlag(task.DELETED) &&
              task.status.testFlag(task.COMPLETED) ) {
            return QString("%1%2")
                    .arg(QString::fromUtf8("\356\234\251")) //trash
                    .arg(QString::fromUtf8("\u2713"));      // tick
          }
          else if (task.status.testFlag(task.DELETED)) {
            return QString::fromUtf8("\356\234\251");       //trash
          }
          else if (task.status.testFlag(task.COMPLETED)) {
            return QString::fromUtf8("\u2713");            //tick
          }
          else if (task.status.testFlag(task.SELECTED)) {
            if (task.status.testFlag(task.SESSION_COMPLETED)){
              return QString::fromUtf8("\360\237\217\206"); // runclock
            }
            else {
              return QString::fromUtf8("\360\237\225\224"); // clock
            }
          }
          else {
            return QString::fromUtf8("\342\236\225");
          }
        case NAME:
          return task.name;

        case DESCRIPTION:
          return task.description;

        case DEADLINE:
          return QDateTime::fromMSecsSinceEpoch(task.deadline);

        case POMODOROS:
          pomosRatio =
                 QString("%1/%2")
                 .arg(task.completed_pomodoros)
                 .arg(task.assigned_pomodoros);
          return pomosRatio;

        case PRIORITY:
          if (task.status.testFlag(task.DELETED) ||
              task.status.testFlag(task.COMPLETED) ) {
            return QString(" ");
          }
          else {
            switch (task.priority) {
                case task.LOW:
                return QString::fromUtf8("\342\226\274");
              case task.NORMAL:
                //return QString::fromUtf8("\342\227\213");
                return QString("-");
              case task.HIGH:
                return QString::fromUtf8("\342\226\262");
              default:
                return QString::fromUtf8("\360\237\230\203");
            }
          }

        case TAG:
          return tagsToStr(task.id);
        }
      break;

    case Qt::FontRole:
      if (col == STATUS) {
        QFont entypoFont("Entypo", 25);
        return entypoFont;
      }
      break;

    case Qt::TextAlignmentRole:
      if (col == STATUS || col == DEADLINE ||
          col == POMODOROS || col == PRIORITY)
        return Qt::AlignCenter;
      else
        return Qt::AlignLeft + Qt::AlignVCenter;

    case Qt::BackgroundRole:
    {
       if (col == COLOR) {
         QColor cellColor = getMainTagColor(task.id);
         cellColor.setAlpha(200);
         return cellColor;
       }
       break;
    }
    case Qt::UserRole:
      return tid;  // task id

    case (Qt::UserRole+1):
      return task.priority;  // task prior
  }
  return QVariant();
}

QVariant TaskTableModel::headerData(int section, Qt::Orientation orientation,
                                    int role) const {
  if (role == Qt::DisplayRole) {
    if (orientation == Qt::Horizontal) {
      switch (section) {
//        case COLOR:
//          return QString::fromUtf8("\360\237\216\250");
        case STATUS:
          return QString::fromUtf8("\342\204\271");
        case NAME:
          return QString("Name");
        case DESCRIPTION:
          return QString("Description");
        case DEADLINE:
          return QString("Deadline");
        case PRIORITY:
          return QString::fromUtf8("\360\237\223\212");
        case POMODOROS:
          return QString("Q");
        case TAG:
          return QString("Tags");
      }
    }
    else if(orientation == Qt::Vertical) {
      return (section + 1);
    }
  }
  else if (role == Qt::FontRole) {
    if (orientation == Qt::Horizontal) {
      QFont entypoFont("Entypo", 20);
      QFont foodFont("wmfood1", 12);
      switch (section) {
//        case COLOR:
//          return entypoFont;
        case STATUS:
          return entypoFont;
        case PRIORITY:
          return entypoFont;
        case POMODOROS:
          return foodFont;
      }
    }
  }

  if (role == Qt::DecorationRole) {
    if(orientation == Qt::Vertical) {
      return (section + 1);
    }
  }

  if (role == Qt::TextAlignmentRole) {
    return Qt::AlignCenter;
  }

  return QVariant();
}

QString TaskTableModel::tagsToStr(db::ID id) const {
  QList<db::Tag> tag_list = db.TaskTags(id);
  QString result = "";

  foreach(const db::Tag tag, tag_list) {
    result.append("#");
    result.append(tag.name);
    result.append(" ");
  }

  return result;
}

QColor TaskTableModel::getMainTagColor(db::ID task_id) const {
  db::Tag mainTag = db.TaskMainTag(task_id);
  QColor tag_color = QColor("white");

  if(mainTag.id != db::NO_ID && QColor::isValidColor(mainTag.color)) {
    tag_color.setNamedColor(mainTag.color);
  }

  return tag_color;
}

void TaskTableModel::refreshData() {
  switch (mt_) {
    case ALL :
      itemData = db.TaskList();
      break;
    case ACTIVE :
      itemData = db.TaskActiveList();
      break;
    case COMPLETED :
      itemData = db.TaskCompletedList();
      break;
    case DELETED :
      itemData = db.TaskDeletedList();
      break;
    default :
      ;
      //qDebug() << "Wrong model type";
  }

  reset();
}
