#ifndef TASKTABLEMODEL_H
#define TASKTABLEMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include <QContextMenuEvent>
#include <QEvent>

#include "database.h"

enum HeaderType
{
  COLOR = 0,
  STATUS = 1,
  NAME = 2,
  DESCRIPTION = 3,
  DEADLINE = 4,
  POMODOROS = 5,
  PRIORITY = 6,
  TAG = 7
};

enum ModelType
{
  ACTIVE,
  ALL,
  COMPLETED,
  DELETED
};

class TaskTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TaskTableModel(ModelType mt, QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    QList<db::Task> itemData;

private:
    QString tagsToStr(db::ID id) const;
    QColor getMainTagColor(db::ID task_id) const;

    db::Database db;
    ModelType mt_;


public slots:
    void refreshData();
};

#endif // TASKTABLEMODEL_H
