#ifndef ALLTASKTABLE_H
#define ALLTASKTABLE_H

#include <QWidget>
#include <QAction>
#include "task_table_widget/tasktablewidget.h"

class AllTaskTable : public TaskTableWidget
{
  Q_OBJECT
public:
  explicit AllTaskTable(QWidget *parent = 0);
  ~AllTaskTable();
    
protected:
  void createActions();

private slots:
  void table_customContexMenu(QPoint /* pos */);

private:
  QAction *addTaskAction;
  QAction *deleteAction;
  QAction *editAction;
  QAction *removeAction;
  QAction *restoreAction;

};

#endif // ALLTASKTABLE_H
