#ifndef MAINWINDOWNAMESPACE_H
#define MAINWINDOWNAMESPACE_H

#include <Qt>

namespace MainWindowDisplay {
  // Stosunek szerokości listy zadań do szerokości głównego okna
  const qreal MAX_TASK_LIST_WIDTH = 0.5;
  const qreal MIN_TASK_LIST_WIDTH = 0.25;

  // Stosunek wielkości głównego okna do rozmiarów ekranu (używany przy początkowym uruchomieniu)
  const qreal DEFAULT_SCREEN_SCALE = 0.75;

  // Minimalny stosunek wielkości głównego okna do rozmiarów ekranu
  const qint32 MIN_WIDTH = 850;
  const qint32 MIN_HEIGHT = 400;
}

#endif // MAINWINDOWNAMESPACE_H
