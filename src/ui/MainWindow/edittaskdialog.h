#ifndef EDITTASKDIALOG_H
#define EDITTASKDIALOG_H

#include "ui_edittaskdialog.h"
#include "database.h"
#include <QStandardItemModel>

class EditTaskDialog : public QDialog, private Ui::EditTaskDialog
{
  Q_OBJECT

public:
  EditTaskDialog(db::Task &task, QWidget *parent = 0);

private:
  void setDefaultDateTime();
  void focusOnTask(db::Task &task);

  enum ItemType {
      listItemT,
      comboBoxItemT
  };

  QStandardItem * makeItem(const db::Tag& tag, ItemType itype);

  QMap<db::ID, QStandardItem* > listItems;
  QMap<db::ID, QStandardItem* > cbItems;

  db::Database db;
  db::Task editedTask;

  QStandardItemModel * tagListModel;
  QStandardItemModel * mainTagModel;
signals:
  void sendNewTask(db::Task&);

private slots:
  void editTask();
  void slotChanged(const QModelIndex& topLeft, const QModelIndex& /* bottomRight */);
};

#endif // EDITTASKDIALOG_H
