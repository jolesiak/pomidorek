#include <QApplication>
#include <QFile>
#include <QDebug>
#include <QTextCodec>
#include "mainwindow.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  QCoreApplication::setOrganizationDomain("www.pomodare.com");
  QCoreApplication::setOrganizationName("PomodareTeam");
  QCoreApplication::setApplicationName("Pomodare");
  QCoreApplication::setApplicationVersion("1.0.0");

  // Ustaw kodowanie
  QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

  MainWindow w;
  QFile qss(":qss/MainStyle.css");
  qss.open(QFile::ReadOnly);
  app.setStyleSheet(qss.readAll());
  qss.close();
  w.show();

  return app.exec();
}
