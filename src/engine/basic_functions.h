#ifndef BASIC_FUNCTIONS_H
#define BASIC_FUNCTIONS_H

#include <QDesktopWidget>
#include <QPoint>
#include <QRect>
#include <QSize>
#include <QPlainTextEdit>
#include <QLineEdit>
#include <QLabel>
#include <QPainter>
#include <QIcon>

QIcon createUserIcon(const int size, const QString color_name, bool border);

const QString TIMER_LONG_BREAK = "TimerLongBreak";
const QString TIMER_SHORT_BREAK = "TimerShortBreak";
const QString TIMER_DEFAULT_TIME = "TimerDefaultTime";

/* Wyśrodkowuje okno względem ekranu, skalują je dodtakowo do podanego rozmiaru */
template<class T>
void centerWindow(T *obj, qreal scale) {
  // Pobierz wymiary ekranu
  QRect screenSize = QDesktopWidget().availableGeometry();

  // Oblicz rozmiar i położenie okna
  QSize size = QSize(screenSize.width() * scale, screenSize.height() * scale);
  QPoint top_left = QPoint((screenSize.width() - size.width()) / 2,
      (screenSize.height() - size.height()) / 2);
  QRect screen_geometry = QRect(top_left, size);

  // Ustaw rozmiary i położenie okna
  obj->setGeometry(screen_geometry);
}

/* Ustawia minimalny rozmiar okna względem ekranu */
template<class T>
void setMinimumSizeToScreen(T *obj, qreal scale) {
  QRect screenSize = QDesktopWidget().availableGeometry();
  obj->setMinimumSize(screenSize.width() * scale, screenSize.height() * scale);
}

/* Ustawia maksymalny rozmiar okna względem ekranu */
template<class T>
void setMaximumSizeToScreen(T *obj, qreal scale) {
  QRect screenSize = QDesktopWidget().availableGeometry();
  obj->setMaximumSize(screenSize.width() * scale, screenSize.height() * scale);
}

/* Prevents the text in a widget from overflow */
void makeTextEllipsis(QPlainTextEdit *widget, const QString &srcText);

void makeTextEllipsis(QLabel *label, const QString &srcText);

#endif // BASIC_FUNCTIONS_H
