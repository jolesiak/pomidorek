#include "basic_functions.h"
#include <QDebug>

static const qint8 MAGIC_SIX = 6;

void makeTextEllipsis(QPlainTextEdit *widget, QString const& srcText) {
  QFontMetrics metrics(widget->font());
  QString dstText = metrics.elidedText(srcText, Qt::ElideRight, widget->width() - MAGIC_SIX);
  widget->setPlainText(dstText);
}

void makeTextEllipsis(QLabel *label, QString const& srcText) {
  QFontMetrics metrics(label->font());
  QString dstText = metrics.elidedText(srcText, Qt::ElideRight, label->width() - MAGIC_SIX);
  label->setText(dstText);
}

QIcon createUserIcon(const int size, const QString color_name, bool border)
{
 QPixmap pixmap(size, size);

 if(QColor::isValidColor(color_name)) {
   QColor temp_color;
   temp_color.setNamedColor(color_name);

   pixmap.fill(temp_color);
 }
 else {
   pixmap.fill(QColor("gray"));
 }

 if(border) {
   QPainter p;
   p.begin(&pixmap);
   QBrush brush(Qt::black);
   QPen pen(brush, 1);
   p.setPen(pen);
   p.drawRect(QRect(0,0,size-1,size-1));
   p.end();
 }

 QIcon returnIcon(pixmap);
 return returnIcon;
}

