#include "drive.h"
#include <QDebug>
#include <cstdio>
#include <cstring>
#include <python2.7/Python.h>

static PyObject* call(PyObject* pFunc, const char* arg1, const char* arg2){
    PyObject* retVal = NULL;
    if(PyCallable_Check(pFunc)){
        PyObject* args = PyTuple_New(2);
        PyObject* py_str;
        py_str = PyString_FromString(arg1);
        PyTuple_SetItem(args, 0, py_str);
        py_str = PyString_FromString(arg2);
        PyTuple_SetItem(args, 1, py_str);
        retVal = PyObject_CallObject(pFunc, args);
    }
    else{
        PyErr_Print();
    }

    return retVal;
}

static PyObject* perform_command(const char* file_name, const char* function_name,
                                 const char* arg1, const char* arg2){

    PyObject *pName, *pModule, *pDict, *pFunc, *pVal;

    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append(\".\")");


    pName = PyString_FromString(file_name);
    pModule = PyImport_Import(pName);
    pDict = PyModule_GetDict(pModule);

    pFunc = PyDict_GetItemString(pDict, function_name);
    pVal = call(pFunc, arg1, arg2);

    // Clean up
    Py_DECREF(pModule);
    Py_DECREF(pName);

    // Finish the Python Interpreter
    Py_Finalize();

    return pVal;
}

Py_Errors::Error message_to_error(char* message){
    if(strcmp(message, "SUCCESS") == 0) return Py_Errors::SUCCESS;
    else if(strcmp(message, "BUILD_FAIL") == 0) return Py_Errors::BUILD_FAIL;
    else if(strcmp(message, "INSERT_FAIL") == 0) return Py_Errors::INSERT_FAIL;
    else if(strcmp(message, "DELETE_FAIL") == 0) return Py_Errors::DELETE_FAIL;
    else if(strcmp(message, "STATUS_ERROR") == 0) return Py_Errors::STATUS_ERROR;
    else if(strcmp(message, "DOWNLOAD_FAIL") == 0) return Py_Errors::DOWNLOAD_FAIL;
    else if(strcmp(message, "NO_FILE") == 0) return Py_Errors::NO_FILE;
    else return Py_Errors::SCRIPT_FAIL;
}

Py_Errors::Error download_from_GD(const QString &GD_name, const QString &local_path){
    PyObject* pVal = perform_command("py_drive", "download",
                                     GD_name.toStdString().c_str(),
                                     local_path.toStdString().c_str());
    if(pVal){
        return message_to_error(PyString_AsString(pVal));
    }
    else{
        return Py_Errors::SCRIPT_FAIL;
    }
}

Py_Errors::Error upload_to_GD(const QString &local_path, const QString &GD_name){
    PyObject* pVal = perform_command("py_drive", "upload",
                                     local_path.toStdString().c_str(),
                                     GD_name.toStdString().c_str());
    if(pVal){
        return message_to_error(PyString_AsString(pVal));
    }
    else{
        return Py_Errors::SCRIPT_FAIL;
    }
}
