#!/usr/bin/python

import httplib2
import pprint

from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.file import Storage
import webbrowser
from oauth2client.tools import run


# Copy your credentials from the APIs Console
CLIENT_ID = '330582374126.apps.googleusercontent.com'
CLIENT_SECRET = 'T6rGQhO8seXvfsgmWIxpGBON'

# Check https://developers.google.com/drive/scopes for all available scopes
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'

# Redirect URI for installed apps
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

CRED_PATH = 'auth_out.txt'

NUM_OF_TRIES = 5 					# number of attempts to connect to drive

# Warning! : changing those constants require changes in cpp part, so messages fit
SUCCESS = "SUCCESS"
BUILD_FAIL = "BUILD_FAIL"			# possibly connection fail
INSERT_FAIL = "INSERT_FAIL"
DELETE_FAIL = "DELETE_FAIL"
STATUS_ERROR = "STATUS_ERROR"
DOWNLOAD_FAIL = "DOWNLOAD_FAIL"
NO_FILE = "NO_FILE"

drive_service = 0


def build_drive(try_num):
	if(try_num > 0):
		try:
			storage = Storage(CRED_PATH)
			credentials = storage.get()
			
			flow = OAuth2WebServerFlow(CLIENT_ID, CLIENT_SECRET, OAUTH_SCOPE, REDIRECT_URI)
		
			if credentials is None or credentials.invalid == True:
				credentials = run(flow, storage)
			
			http = httplib2.Http()
			http = credentials.authorize(http)
			
		except:
			return BUILD_FAIL
			
		global drive_service
		try:
			drive_service = build('drive', 'v2', http=http)
		except:
			return build_drive(try_num - 1)
			
		return SUCCESS
	else:
		return BUILD_FAIL


def insert_file(local_path, GD_name):
	try:
		media_body = MediaFileUpload(local_path, mimetype='text/plain', resumable=True)
		body = {
			'title': GD_name,
			'description': 'A test document',
			'mimeType': 'text/plain'
		}
	
		file = drive_service.files().insert(body=body, media_body=media_body).execute()
	except:
		return INSERT_FAIL
	
	return SUCCESS

def delete_file(local_path, GD_name):
	try:
		query = "title = '" + GD_name + "'" 
		
		poss_files = drive_service.files().list(q=query).execute()['items']
		
		if len(poss_files) == 0:
			return NO_FILE
		
		for item in poss_files:
			drive_service.files().delete(fileId=item[u'id']).execute();	
	except:
		return DELETE_FAIL
	
	return SUCCESS

def dwnld(GD_name, local_path):
	try:
		query = "title = '" + GD_name + "'" 
		poss_files = drive_service.files().list(q=query).execute()['items']
		drive_file = 0
		for item in poss_files:
			drive_file = item	
		
		if drive_file != 0:
			download_url = drive_file.get('downloadUrl')
			if download_url:
				resp, content = drive_service._http.request(download_url)
				if resp.status == 200:
					f = open(local_path, 'wb')
					f.write(content)
					f.close()
					
				else:
					return STATUS_ERROR
				
			else:
				f = open(local_path, 'wb')
				f.write('')
				f.close()
		else:
			return NO_FILE
	except:
		return DOWNLOAD_FAIL
	
	return SUCCESS   

# ------------------------------------------------------------------------------

def download(GD_name, local_path):
	ret_val = build_drive(NUM_OF_TRIES)
	if ret_val == SUCCESS:
		ret_val = dwnld(GD_name, local_path)
	
	return ret_val

def upload(local_path, GD_name):
	ret_val = build_drive(NUM_OF_TRIES)
	if ret_val == SUCCESS:
		ret_val = delete_file(local_path, GD_name)
		if ret_val == SUCCESS or ret_val == NO_FILE:
			ret_val = insert_file(local_path, GD_name)
	
	return ret_val
