#ifndef DRIVE_H
#define DRIVE_H

#include <namespaces.h>
#include <QString>

Py_Errors::Error download_from_GD(const QString &GD_name, const QString &local_path);
Py_Errors::Error upload_to_GD(const QString &local_path, const QString &GD_name);

#endif
