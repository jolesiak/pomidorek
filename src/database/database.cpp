#include <QString>
#include <QList>
#include <QSet>
#include <QMap>
#include <QPair>
#include <QPoint>
#include <QVector>
#include <QStack>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QDomDocument>
#include <QMutex>
#include <QMutexLocker>
#include <QProcess>
#include <QDebug>

#include "database.h"
#include "data.h"
#include "file_operations.h"
#include "namespaces.h"
#include "google_drive/drive.h"

#define CHECK(args...) if (IsNotValid()) return args
#define CHECK_ERR(object, args...) if ((object).IsNotValid()) \
    return Error((object).LastErrorDesc(), ##args)
#define REFRESH() DatabaseRefresher::Refresh()

namespace db {

const QString DATABASE_DIR = "Database";
const QString DATABASE_PACK = DATABASE_DIR + ".zip";
const QString DATABASE_FILES = DATABASE_DIR + "/";

const QString REMOTE_DATABASE_DIR = "RemoteDatabase";
const QString REMOTE_DATABASE_PACK  = REMOTE_DATABASE_DIR + "/" +
                                      DATABASE_PACK;
const QString REMOTE_DATABASE_FILES = REMOTE_DATABASE_DIR + "/" +
                                      DATABASE_FILES;

const QString LAST_SYNCH_DATABASE_DIR = "LastSynchDatabase";
const QString LAST_SYNCH_DATABASE_PACK = LAST_SYNCH_DATABASE_DIR + "/" +
                                         DATABASE_PACK;
const QString LAST_SYNCH_DATABASE_FILES = LAST_SYNCH_DATABASE_DIR + "/" +
                                          DATABASE_FILES;

const QString TASKS_FILE = "Tasks.xml";
const QString DELETED_TASKS_FILE = "Deleted.xml";
const QString TAGS_FILE = "Tags.xml";
const QString TASK_TAG_REL_FILE = "TaskTagRel.xml";
const QString SELECTED_TASKS_FILE = "SelectedTasks.xml";
const QString SELECTED_TASKS_ORDER_FILE = "Order.xml";
const QString DEVICE_ID_FILE = "DeviceId.xml";

QMutex Database::mutex_(QMutex::Recursive);

TaskData Database::tasks_(DATABASE_FILES + TASKS_FILE);

DelTaskData Database::deleted_tasks_(DATABASE_FILES + DELETED_TASKS_FILE);

TagData Database::tags_(DATABASE_FILES + TAGS_FILE);

TaskTagRelData Database::task_tag_rel_(DATABASE_FILES + TASK_TAG_REL_FILE);

SelectedTaskData Database::selected_tasks_(DATABASE_FILES + SELECTED_TASKS_FILE,
                                           DATABASE_FILES
                                           + SELECTED_TASKS_ORDER_FILE);

DeviceIdData Database::device_id_(DEVICE_ID_FILE);

void Database::Init() {
  QMutexLocker lock(&mutex_);
  static bool if_initialized = false;
  if (if_initialized) return Error("Cannot initialize several times");
  if (!QDir().mkpath(DATABASE_DIR))
    return Error("Cannot create database folder");

  device_id_.Init();
  CHECK_ERR(device_id_);
  DeviceId device_id = device_id_.GetDeviceId();
  CHECK_ERR(device_id_);

  tasks_.Init(device_id);
  CHECK_ERR(tasks_);

  deleted_tasks_.Init(device_id);
  CHECK_ERR(deleted_tasks_);

  tags_.Init(device_id);
  CHECK_ERR(tags_);

  task_tag_rel_.Init(device_id);
  CHECK_ERR(task_tag_rel_);

  selected_tasks_.Init(device_id);
  CHECK_ERR(selected_tasks_);

  if_initialized = true;
  REFRESH();
}

Task Database::TaskGet(const ID &id) const {
  QMutexLocker lock(&mutex_);
  item::Task task = tasks_.GetItem(id);
  CHECK_ERR(tasks_, NO_TASK);
  Task result = TaskItemToTask(task);
  CHECK(NO_TASK);
  return Success(result);
}

Tag Database::TaskMainTag(const ID &id) const {
  QMutexLocker lock(&mutex_);
  if (!tasks_.IfItemExists(id) && !deleted_tasks_.IfItemExists(id)) {
    return Error("Task: " + IDtoQString(id) + " doesnt exist", NO_TAG);
  }
  ID main_tag_id = task_tag_rel_.MainTag(id);
  CHECK_ERR(task_tag_rel_, NO_TAG);
  if (main_tag_id == NO_ID) return Success(NO_TAG);
  item::Tag tag = tags_.GetItem(main_tag_id);
  CHECK_ERR(tags_, NO_TAG);
  return Success(TagItemToTag(tag));
}

QList<Tag> Database::TaskTags(const ID &id) const {
  QMutexLocker lock(&mutex_);
  if (!tasks_.IfItemExists(id) && !deleted_tasks_.IfItemExists(id)) {
    return Error("Task: " + IDtoQString(id) + " doesnt exist", QList<Tag>());
  }
  QList<ID> result_ids = task_tag_rel_.TagsList(id);
  CHECK_ERR(task_tag_rel_, QList<Tag>());
  ID main_tag_id = TaskMainTag(id).id;
  if (main_tag_id != NO_ID) {
    int main_tag_index = result_ids.indexOf(main_tag_id);
    if (main_tag_index == -1) {
      return Error("Task " + IDtoQString(id) + " have main tag, but the list "
                   + "of all tags doesnt contain it", QList<Tag>());
    }
    result_ids.removeAt(main_tag_index);
  }
  QList<Tag> result;
  for (const ID &id : result_ids) {
    result.append(TagItemToTag(tags_.GetItem(id)));
    CHECK_ERR(tags_, QList<Tag>());
  }
  qSort(result.begin(), result.end(), CompareTags);
  if (main_tag_id != NO_ID) {
    result.push_front(TagItemToTag(tags_.GetItem(main_tag_id)));
    CHECK_ERR(tags_, QList<Tag>());
  }
  return Success(result);
}

QList<Task> Database::TaskList() const {
  QMutexLocker lock(&mutex_);
  QList<item::Task> tasks_list = tasks_.ItemsList();
  CHECK_ERR(tasks_, QList<Task>());
  QList<Task> result;
  for (const item::Task &item : tasks_list) {
    result.append(TaskItemToTask(item));
    CHECK(QList<Task>());
  }
  return Success(result);
}

QList<Task> Database::TaskActiveList() const {
  QMutexLocker lock(&mutex_);
  QList<Task> result = TaskList();
  CHECK(QList<Task>());
  QList<Task>::iterator it = result.begin();
  while (it != result.end()) {
    if ((*it).completed_pomodoros == (*it).assigned_pomodoros) {
      it = result.erase(it);
    } else {
      ++it;
    }
  }
  return Success(result);
}

QList<Task> Database::TaskCompletedList() const {
  QMutexLocker lock(&mutex_);
  QList<Task> result = TaskList();
  CHECK(QList<Task>());
  QList<Task>::iterator it = result.begin();
  while (it != result.end()) {
    if ((*it).completed_pomodoros < (*it).assigned_pomodoros) {
      it = result.erase(it);
    } else {
      ++it;
    }
  }
  return Success(result);
}

QList<Task> Database::TaskDeletedList() const {
  QMutexLocker lock(&mutex_);
  QList<item::Task> tasks_list = deleted_tasks_.ItemsList();
  CHECK_ERR(deleted_tasks_, QList<Task>());
  QList<Task> result;
  for (const item::Task &item : tasks_list) {
    result.append(TaskItemToTask(item));
    CHECK(QList<Task>());
  }
  return Success(result);
}

void Database::TaskAdd(Task &task, const QList<ID> &tags,
                       const ID &main_tag_id) {
  QMutexLocker lock(&mutex_);
  task.id = NO_ID;
  qint64 ts = QDateTime::currentMSecsSinceEpoch();
  // Task item
  item::Task new_task = {
    NO_ID,
    task.name,
    ts,
    task.description,
    ts,
    task.assigned_pomodoros,
    ts,
    task.completed_pomodoros,
    task.deadline,
    ts,
    false
  };
  tasks_.AddItem(new_task);
  CHECK_ERR(tasks_);
  // Tags
  for (const ID &tag_id : tags) {
    bool if_main_tag = (main_tag_id == tag_id);
    item::TaskTagRel rel = {
      new_task.id,
      tag_id,
      if_main_tag,
      ts
    };
    task_tag_rel_.AddItem(rel);
    CHECK_ERR(task_tag_rel_);
  }
  task = TaskItemToTask(new_task);
  CHECK();
  REFRESH();
  return Success();
}

void Database::TaskEdit(const Task &new_task) {
  QMutexLocker lock(&mutex_);
  item::Task old_task = tasks_.GetItem(new_task.id);
  CHECK_ERR(tasks_);

  qint64 ts = QDateTime::currentMSecsSinceEpoch();
  if (old_task.name != new_task.name) {
    old_task.name = new_task.name;
    old_task.name_ts = ts;
  }
  if (old_task.description != new_task.description) {
    old_task.description = new_task.description;
    old_task.description_ts = ts;
  }
  if (old_task.assigned_pomodoros
      != new_task.assigned_pomodoros) {
    old_task.assigned_pomodoros
        = new_task.assigned_pomodoros;
    old_task.assigned_pomodoros_ts = ts;
  }
  if (old_task.completed_pomodoros
      != new_task.completed_pomodoros) {
    old_task.completed_pomodoros
        = new_task.completed_pomodoros;
  }
  if (old_task.deadline != new_task.deadline) {
    old_task.deadline = new_task.deadline;
    old_task.deadline_ts = ts;
  }

  tasks_.EditItem(old_task);
  CHECK_ERR(tasks_);
  REFRESH();
  return Success();
}

void Database::TaskAddTag(const ID &task_id, const ID &tag_id) {
  QMutexLocker lock(&mutex_);
  tasks_.CheckIfItemExists(task_id);
  CHECK_ERR(tasks_);
  tags_.CheckIfItemExists(tag_id);
  CHECK_ERR(tags_);

  item::TaskTagRel rel = {
    task_id,
    tag_id,
    false,
    QDateTime::currentMSecsSinceEpoch()
  };
  task_tag_rel_.AddItem(rel);
  CHECK_ERR(task_tag_rel_);

  REFRESH();
  return Success();
}

void Database::TaskSetMainTag(const ID &task_id, const ID &main_tag_id) {
  QMutexLocker lock(&mutex_);
  task_tag_rel_.SetMainTag(task_id, main_tag_id);
  CHECK_ERR(task_tag_rel_);
  REFRESH();
  return Success();
}

void Database::TaskRemoveTag(const ID &task_id, const ID &tag_id) {
  QMutexLocker lock(&mutex_);
  tasks_.CheckIfItemExists(task_id);
  CHECK_ERR(tasks_);
  tags_.CheckIfItemExists(tag_id);
  CHECK_ERR(tags_);

  task_tag_rel_.RemoveItem(RelID(task_id, tag_id));
  CHECK_ERR(task_tag_rel_);

  REFRESH();
  return Success();
}

void Database::TaskRemove(const ID &id) {
  QMutexLocker lock(&mutex_);
  item::Task task = tasks_.GetItem(id);
  CHECK_ERR(tasks_);
  if (task.deleted) return Error("Already deleted task on not-deleted list");
  task.deleted = true;
  deleted_tasks_.AddItem(task);
  CHECK_ERR(deleted_tasks_);
  tasks_.RemoveItem(id);
  CHECK_ERR(tasks_);
  // Remove selected tasks
  selected_tasks_.RemoveItemsConnectedWithTask(id);
  CHECK_ERR(selected_tasks_);
  REFRESH();
  return Success();
}

void Database::TaskRestoreDeleted(const ID &id) {
  QMutexLocker lock(&mutex_);
  item::Task task = deleted_tasks_.GetItem(id);
  CHECK_ERR(deleted_tasks_);
  task.deleted = false;
  tasks_.AddItem(task);
  CHECK_ERR(tasks_);
  deleted_tasks_.RemoveItem(id);
  CHECK_ERR(deleted_tasks_);
  REFRESH();
  return Success();
}

void Database::TaskRemoveDeleted(const ID &id) {
  QMutexLocker lock(&mutex_);
  deleted_tasks_.RemoveItem(id);
  CHECK_ERR(deleted_tasks_);
  task_tag_rel_.RemoveTask(id);
  CHECK_ERR(task_tag_rel_);
  REFRESH();
  return Success();
}

void Database::TaskRemoveMany(const QList<ID> &ids) {
  QMutexLocker lock(&mutex_);
  for (const ID &id : ids) {
    TaskRemove(id);
    CHECK();
  }
  /*
  RemoveMany(ids, tasks_);
  CHECK();
  QList<item::SelectedTask> selected_tasks = selected_tasks_.ItemsList();
  CHECK_ERR(selected_tasks_);
  QList<ID> selected_to_remove;
  for (const item::SelectedTask &selected_task : selected_tasks) {
    if (!tasks_.IfItemExists(selected_task.task_id)) {
      selected_to_remove.append(selected_task.id);
    }
    CHECK_ERR(tasks_);
  }
  // Refresh there
  SelectedTaskRemoveMany(selected_to_remove);
  CHECK();
  */
  REFRESH();
  return Success();
}

void Database::TaskRemoveDeletedMany(const QList<ID> &ids) {
  QMutexLocker lock(&mutex_);
  RemoveMany(ids, deleted_tasks_);
  CHECK();
  REFRESH();
  return Success();
}

void Database::TaskRestoreDeletedMany(const QList<ID> &ids) {
  QMutexLocker lock(&mutex_);
  for (const ID &id : ids) {
    item::Task task = deleted_tasks_.GetItem(id);
    CHECK_ERR(deleted_tasks_);
    tasks_.AddItem(task, false, false);
    CHECK_ERR(tasks_);
  }
  tasks_.WriteXML();
  CHECK_ERR(tasks_);
  RemoveMany(ids, deleted_tasks_);
  CHECK();
  REFRESH();
  return Success();
}

Tag Database::TagGet(const ID &id) const {
  QMutexLocker lock(&mutex_);
  item::Tag tag = tags_.GetItem(id);
  CHECK_ERR(tags_, NO_TAG);
  return Success(TagItemToTag(tag));
}

QList<Tag> Database::TagList() const {
  QMutexLocker lock(&mutex_);
  QList<item::Tag> tags_list = tags_.ItemsList();
  CHECK_ERR(tags_, QList<Tag>());
  QList<Tag> result;
  for (const item::Tag &item : tags_list) {
    result.append(TagItemToTag(item));
  }
  return Success(result);
}

QList<Task> Database::TagTasks(const ID &id) const {
  QMutexLocker lock(&mutex_);
  tags_.CheckIfItemExists(id);
  CHECK_ERR(tags_, QList<Task>());
  QList<ID> result_ids = task_tag_rel_.TasksList(id);
  CHECK_ERR(task_tag_rel_, QList<Task>());
  QList<Task> result;
  for (const ID &id : result_ids) {
    result.append(TaskItemToTask(tasks_.GetItem(id)));
    CHECK_ERR(tasks_, QList<Task>());
    CHECK(QList<Task>());
  }
  return Success(result);
}

void Database::TagAdd(Tag &tag) {
  QMutexLocker lock(&mutex_);
  tag.id = NO_ID;
  qint64 ts = QDateTime::currentMSecsSinceEpoch();
  item::Tag new_tag = {
    NO_ID,
    tag.name,
    ts,
    tag.color,
    ts
  };
  tags_.AddItem(new_tag);
  CHECK_ERR(tags_);
  tag.id = new_tag.id;
  return Success();
}

void Database::TagEdit(const Tag &new_tag) {
  QMutexLocker lock(&mutex_);
  item::Tag old_tag = tags_.GetItem(new_tag.id);
  CHECK_ERR(tags_);

  qint64 ts = QDateTime::currentMSecsSinceEpoch();
  if (old_tag.name != new_tag.name) {
    old_tag.name = new_tag.name;
    old_tag.name_ts = ts;
  }
  if (old_tag.color != new_tag.color) {
    old_tag.color = new_tag.color;
    old_tag.color_ts = ts;
  }

  tags_.EditItem(old_tag);
  CHECK_ERR(tags_);
  REFRESH();
  return Success();
}

void Database::TagRemove(const ID &id) {
  QMutexLocker lock(&mutex_);
  tags_.RemoveItem(id);
  CHECK_ERR(tags_);
  task_tag_rel_.RemoveTag(id);
  CHECK_ERR(task_tag_rel_);
  REFRESH();
  return Success();
}

SelectedTask Database::SelectedTaskGet(const ID &id) const {
  QMutexLocker lock(&mutex_);
  item::SelectedTask selected_task = selected_tasks_.GetItem(id);
  CHECK_ERR(selected_tasks_, NO_SELECTED_TASK);
  return Success(SelectedTaskItemToSelectedTask(selected_task));
}

QList<SelectedTask> Database::SelectedTaskList() const {
  QMutexLocker lock(&mutex_);
  QList<item::SelectedTask> items_list = selected_tasks_.OrderedList();
  CHECK_ERR(selected_tasks_, QList<SelectedTask>());
  QList<SelectedTask> result;
  result.reserve(items_list.size());
  for (const item::SelectedTask &item : items_list) {
    result.append(SelectedTaskItemToSelectedTask(item));
  }
  return Success(result);
}

int Database::SelectedTaskFirstPos() const {
  QMutexLocker lock(&mutex_);
  int result = selected_tasks_.FirstPos();
  CHECK_ERR(selected_tasks_, 0);
  return Success(result);
}

int Database::SelectedTaskLastPos() const {
  QMutexLocker lock(&mutex_);
  int result = selected_tasks_.LastPos();
  CHECK_ERR(selected_tasks_, 0);
  return Success(result);
}

void Database::SelectedTaskAdd(SelectedTask &selected_task,
                               const qint32 position) {
  QMutexLocker lock(&mutex_);
  SelectedTaskAddWithoutSaveAndRefresh(selected_task, position);
  CHECK();
  selected_tasks_.WriteXML();
  CHECK_ERR(selected_tasks_);
  REFRESH();
  return Success();
}

void Database::SelectedTaskAddMany(QList<SelectedTask> &selected_tasks,
                                   const qint32 first_item_position) {
  QMutexLocker lock(&mutex_);
  int i = 0;
  for (SelectedTask &selected_task : selected_tasks) {
    SelectedTaskAddWithoutSaveAndRefresh(selected_task,
                                         first_item_position + i);
    CHECK();
    i++;
  }
  selected_tasks_.WriteXML();
  CHECK_ERR(selected_tasks_);
  REFRESH();
  return Success();
}

void Database::SelectedTaskEdit(const SelectedTask &selected_task) {
  QMutexLocker lock(&mutex_);
  item::SelectedTask old_selected_task
      = selected_tasks_.GetItem(selected_task.id);
  CHECK_ERR(selected_tasks_);

  if (old_selected_task.task_id != selected_task.task_id)
    return Error("Cannot edit task_id of selected task");

  if (old_selected_task.assigned_pomodoros
      != selected_task.assigned_pomodoros) {
    old_selected_task.assigned_pomodoros = selected_task.assigned_pomodoros;
    old_selected_task.assigned_pomodoros_ts
        = QDateTime::currentMSecsSinceEpoch();
  }

  old_selected_task.completed_pomodoros = selected_task.completed_pomodoros;

  selected_tasks_.EditItem(old_selected_task);
  CHECK_ERR(selected_tasks_);
  REFRESH();
  return Success();
}

void Database::SelectedTaskEditPosition(const ID &id, const qint32 new_pos) {
  QMutexLocker lock(&mutex_);
  selected_tasks_.EditPosition(id, new_pos);
  CHECK_ERR(selected_tasks_);
  REFRESH();
  return Success();
}

void Database::SelectedTaskRemove(const ID &id) {
  QMutexLocker lock(&mutex_);
  selected_tasks_.RemoveItem(id);
  CHECK_ERR(selected_tasks_);
  REFRESH();
  return Success();
}

void Database::SelectedTaskAddPomodoro(const ID &id) {
  QMutexLocker lock(&mutex_);
  item::SelectedTask selected_task = selected_tasks_.GetItem(id);
  CHECK_ERR(selected_tasks_);
  const ID task_id = selected_task.task_id;
  if (selected_task.completed_pomodoros == selected_task.assigned_pomodoros) {
    return Error(QString("Cannot add a pomodoro if the number of completed ")
                 + "pomodoros is equal to the number of assigned ones");
  }
  item::Task task = tasks_.GetItem(task_id);
  CHECK_ERR(tasks_);
  if (task.completed_pomodoros == task.assigned_pomodoros) {
    return Error(QString("Cannot add a pomodoro if the number of completed ")
                 + "pomodoros is equal to the number of assigned ones");
  }

  selected_task.completed_pomodoros++;
  SelectedTaskEdit(SelectedTaskItemToSelectedTask(selected_task));
  CHECK();
  task.completed_pomodoros++;
  TaskEdit(TaskItemToTask(task));
  CHECK();

  REFRESH();
  return Success();
}

void Database::SelectedTaskMoveUp(const ID &id) {
  QMutexLocker lock(&mutex_);
  return SelectedTaskMoveDownBy(id, -1);
}

void Database::SelectedTaskMoveDown(const ID &id) {
  QMutexLocker lock(&mutex_);
  return SelectedTaskMoveDownBy(id, 1);
}

void Database::SelectedTaskMoveDownBy(const ID &id, const qint32 modifier) {
  QMutexLocker lock(&mutex_);
  selected_tasks_.EditPosition(id, modifier);
  CHECK_ERR(selected_tasks_);
  REFRESH();
  return Success();
}

void Database::SelectedTaskRemoveMany(const QList<ID> &ids) {
  QMutexLocker lock(&mutex_);
  RemoveMany(ids, selected_tasks_);
  CHECK();
  REFRESH();
  return Success();
}

void Database::Synchronize() {
  QMutexLocker lock(&mutex_);

  // Check if its a first synchronization
  const DeviceId device_id = device_id_.GetDeviceId();
  CHECK_ERR(device_id_);
  if (device_id == NO_DEV_ID) {
    FirstSynch();
    CHECK();
  }

  OpenRemoteDatabase();
  CHECK();
  OpenLastSynchDatabase();
  CHECK();

  SynchronizeTasks();
  CHECK();
  SynchronizeTags();
  CHECK();
  SynchronizeTaskTagRel();
  CHECK();
  SynchronizeSelectedTasks();
  CHECK();

  if (!RemoveFile(DATABASE_PACK))
    return Error("Cannot remove " + DATABASE_PACK);
  if (!DoArchiveOperation(DATABASE_DIR, COMPRESS))
    return Error("Cannot create an archive " + DATABASE_DIR);

  SaveRemoteDatabase();
  CHECK();
  SaveLastSynchDatabase();
  CHECK();

  REFRESH();
  return Success();
}

bool Database::IsNotValid() const {
  return !last_operation_succeeded_;
}

QString Database::LastErrorDescription() const {
  return last_error_description_;
}

Task Database::TaskItemToTask(const item::Task &task) const {
  Task result_task = {
    task.id,
    task.name,
    task.description,
    task.assigned_pomodoros,
    task.completed_pomodoros,
    task.deadline,
    Task::Status(),
    Task::NORMAL
  };
  result_task.status = selected_tasks_.TaskStatus(task.id);
  CHECK_ERR(selected_tasks_, NO_TASK);
  if (task.deleted) result_task.status |= Task::DELETED;
  if (task.completed_pomodoros >= task.assigned_pomodoros)
    result_task.status |= Task::COMPLETED;
  result_task.priority = CalculateTaskPriority(task);
  return Success(result_task);
}

void Database::SelectedTaskAddWithoutSaveAndRefresh(SelectedTask &selected_task,
                                                    const qint32 position) {
  tasks_.CheckIfItemExists(selected_task.task_id);
  CHECK_ERR(tasks_);
  selected_task.id = NO_ID;
  item::SelectedTask new_selected_task = {
    NO_ID,
    selected_task.task_id,
    0,
    selected_task.assigned_pomodoros,
    QDateTime::currentMSecsSinceEpoch(),
    selected_task.completed_pomodoros
  };
  selected_tasks_.AddItem(new_selected_task, position, true, false);
  CHECK_ERR(selected_tasks_);
  selected_task.id = new_selected_task.id;
  return Success();
}

void Database::FirstSynch() {
  DeviceId FIRST_DEV_ID = 1;
  if (QFile::exists(DEVICE_ID_FILE)) {
    if (!QFile::remove(DEVICE_ID_FILE)) {
      return Error("Cannot remove " + DEVICE_ID_FILE + " file");
    }
  }
  Py_Errors::Error status = download_from_GD(DEVICE_ID_FILE,
      QFileInfo(DEVICE_ID_FILE).absoluteFilePath());
  if (status == Py_Errors::SUCCESS) {
    device_id_.Init(NO_DEV_ID);
    CHECK_ERR(device_id_);
    device_id_.IncreaseDeviceId();
    CHECK_ERR(device_id_);
  } else if (status == Py_Errors::NO_FILE) {
    device_id_.AddItem(FIRST_DEV_ID);
    CHECK_ERR(device_id_);
  } else {
    return Error("Cannot download a file from GoogleDrive");
  }
  DeviceId device_id = device_id_.GetDeviceId();
  CHECK_ERR(device_id_);
  ChangeDeviceIds(device_id);
  CHECK();
  status = upload_to_GD(QFileInfo(DEVICE_ID_FILE).absoluteFilePath(),
                        DEVICE_ID_FILE);
  if (status == Py_Errors::SUCCESS) return Success();
  return Error("Cannot upload a file to GoogleDrvie");
}

void Database::ChangeDeviceIds(const DeviceId &new_device_id) {
  tasks_.ChangeDeviceId(new_device_id);
  CHECK_ERR(tasks_);

  deleted_tasks_.ChangeDeviceId(new_device_id);
  CHECK_ERR(deleted_tasks_);

  tags_.ChangeDeviceId(new_device_id);
  CHECK_ERR(tags_);

  task_tag_rel_.ChangeDeviceId(new_device_id);
  CHECK_ERR(task_tag_rel_);

  selected_tasks_.ChangeDeviceId(new_device_id);
  CHECK_ERR(selected_tasks_);

  return Success();
}

void Database::OpenRemoteDatabase() const {
  if (!CreateEmptyDir(REMOTE_DATABASE_DIR)) {
    return Error("Cannot create empty dir/clear current dir: "
                 + REMOTE_DATABASE_DIR);
  }
  Py_Errors::Error status = download_from_GD(DATABASE_PACK,
      QFileInfo(REMOTE_DATABASE_PACK).absoluteFilePath());
  if (status == Py_Errors::SUCCESS) {
    if (!DoArchiveOperation(REMOTE_DATABASE_PACK, DECOMPRESS))
      return Error("Cannot decompress: " + REMOTE_DATABASE_PACK);
    return Success();
  } else if (status == Py_Errors::NO_FILE) {
    if (!CreateEmptyDir(REMOTE_DATABASE_FILES)) {
      return Error("Cannot create empty dir/clear current dir: "
                   + REMOTE_DATABASE_FILES);
    }
    return Success();
  }
  return Error("Cannot download a file from GoogleDrive");
}

void Database::OpenLastSynchDatabase() const {
  if (!QFile::exists(LAST_SYNCH_DATABASE_PACK)) {
    if (!CreateEmptyDir(LAST_SYNCH_DATABASE_FILES)) {
      return Error("Cannot create empty dir/clear current dir: "
                   + LAST_SYNCH_DATABASE_FILES);
    }
    return Success();
  }
  if (!RemoveDir(LAST_SYNCH_DATABASE_FILES)) {
    return Error("Cannot remove dir " + LAST_SYNCH_DATABASE_FILES);
  }
  if (!DoArchiveOperation(LAST_SYNCH_DATABASE_PACK, DECOMPRESS))
    return Error("Cannot decompress: " + LAST_SYNCH_DATABASE_PACK);
  return Success();
}

void Database::SaveRemoteDatabase() const {
  if (!RemoveDir(REMOTE_DATABASE_DIR)) {
    return Error("Cannot remove  dir: " + REMOTE_DATABASE_DIR);
  }
  if (!QFile::exists(DATABASE_PACK))
    return Error("Cannot send database to server (archive doesnt exist): "
                 + DATABASE_PACK);
  Py_Errors::Error status = upload_to_GD(
      QFileInfo(DATABASE_PACK).absoluteFilePath(), DATABASE_PACK);
  if (status == Py_Errors::SUCCESS) return Success();
  return Error("Cannot upload a file to GoogleDrvie");
}

void Database::SaveLastSynchDatabase() const {
  if (!CreateEmptyDir(LAST_SYNCH_DATABASE_DIR)) {
    return Error("Cannot create empty dir/clear current dir: "
                 + LAST_SYNCH_DATABASE_DIR);
  }
  if (!QFile::rename(DATABASE_PACK, LAST_SYNCH_DATABASE_PACK)) {
    return Error("Cannot move " + DATABASE_PACK + " archive file to "
                 + LAST_SYNCH_DATABASE_PACK);
  }
  return Success();
}

QHash<ID, item::Task> Database::GetAllTasksHash(Database::Source source) const {
  // Set dir
  QString file_dir = (source == REMOTE) ? REMOTE_DATABASE_FILES
                                        : LAST_SYNCH_DATABASE_FILES;
  // Get not deleted tasks
  QHash<ID, item::Task> tasks;
  if (source == LOCAL) {
    tasks = tasks_.ItemsHash();
    CHECK_ERR(tasks_, QHash<ID, item::Task>());
  } else {
    TaskData tasks_data(file_dir + TASKS_FILE);
    tasks = GetRemoteOrLastSynchItems(tasks_data);
    CHECK(QHash<ID, item::Task>());
  }
  // Get deleted tasks
  QHash<ID, item::Task> deleted_tasks;
  if (source == LOCAL) {
    deleted_tasks = deleted_tasks_.ItemsHash();
    CHECK_ERR(deleted_tasks_, QHash<ID, item::Task>());
  } else {
    DelTaskData deleted_tasks_data(file_dir + DELETED_TASKS_FILE);
    deleted_tasks = GetRemoteOrLastSynchItems(deleted_tasks_data);
    CHECK(QHash<ID, item::Task>());
  }
  // Check if any element was doubled and sum up results
  int result_size = tasks.size() + deleted_tasks.size();
  tasks.unite(deleted_tasks);
  if (result_size != tasks.size()) {
    QString error_source;
    switch (source) {
    case LOCAL:
      error_source = "local";
      break;
    case REMOTE:
      error_source = "remote";
      break;
    case LAST_SYNCH:
      error_source = "last synch";
      break;
    }
    return Error("One item on deleted list and not deleted list ("
                 + error_source + ")", QHash<ID, item::Task>());
  }
  return Success(tasks);
}

void Database::MergeTasksFields(item::Task &local_task,
                                const item::Task &remote_task,
                                const item::Task &last_synch_task) const {
  if (remote_task.name_ts > local_task.name_ts) {
    local_task.name = remote_task.name;
    local_task.name_ts = remote_task.name_ts;
  }
  if (remote_task.description_ts > local_task.description_ts) {
    local_task.description = remote_task.description;
    local_task.description_ts = remote_task.description_ts;
  }
  if (remote_task.assigned_pomodoros_ts > local_task.assigned_pomodoros_ts) {
    local_task.assigned_pomodoros = remote_task.assigned_pomodoros;
    local_task.assigned_pomodoros_ts = remote_task.assigned_pomodoros_ts;
  }
  if (remote_task.deadline_ts > local_task.deadline_ts) {
    local_task.deadline = remote_task.deadline;
    local_task.deadline_ts = remote_task.deadline_ts;
  }
  local_task.completed_pomodoros += remote_task.completed_pomodoros
                                    - last_synch_task.completed_pomodoros;
  local_task.deleted = ComputeIfTaskDeleted(local_task.deleted,
                                            remote_task.deleted,
                                            last_synch_task.deleted);
}

bool Database::ComputeIfTaskDeleted(bool local_deleted, bool remote_deleted,
                                    bool last_synch_deleted) const {
  // Task was deleted earlier
  if (last_synch_deleted) {
    // Was restored localy or on the other device
    if (!local_deleted || !remote_deleted) return false;
    // Wasnt restored
    return true;
  }
  // Hadnt been deleted and was removed somewhere
  if (local_deleted || remote_deleted) return true;
  // Wasnt removed
  return false;
}

Database::SynchMainTags Database::ComputeMainTags(
    const Relations &local_rels, const Relations &remote_rels,
    const Relations &last_synch_rels) const {
  // task_id --> (tag_id, if_main_tag_ts)
  SynchMainTags result;
  for (const item::TaskTagRel &rel : local_rels) {
    if (!rel.if_main_tag) continue;
    if (!tasks_.IfItemExists(rel.task_id) || !tags_.IfItemExists(rel.tag_id)) {
      continue;
    }
    RelID id(rel.task_id, rel.tag_id);
    if (last_synch_rels.contains(id)) {
      // If relations was deleted
      if (!local_rels.contains(id) || !remote_rels.contains(id)) continue;
    }
    result.insert(rel.task_id, qMakePair(rel.tag_id, rel.if_main_tag_ts));
  }

  for (const item::TaskTagRel &rel : remote_rels) {
    if (!tasks_.IfItemExists(rel.task_id) || !tags_.IfItemExists(rel.tag_id)) {
      continue;
    }
    RelID id(rel.task_id, rel.tag_id);
    if (last_synch_rels.contains(id)) {
      // If relations was deleted
      if (!local_rels.contains(id) || !remote_rels.contains(id)) continue;
    }
    if (rel.if_main_tag) {
      if (result.contains(rel.task_id)) {
        if (result[rel.task_id].second < rel.if_main_tag_ts) {
          result[rel.task_id] = qMakePair(rel.tag_id, rel.if_main_tag_ts);
        }
      } else {
        if (local_rels.contains(id)) {
          qint64 ts = local_rels[id].if_main_tag_ts;
          if (rel.if_main_tag_ts < ts) continue;
        }
        result.insert(rel.task_id, qMakePair(rel.tag_id, rel.if_main_tag_ts));
      }
    // !rel.if_main_tag
    } else {
      if (!result.contains(rel.task_id)) continue;
      qint64 ts = result[rel.task_id].second;
      if (rel.if_main_tag_ts <= ts) continue;
      result.remove(rel.task_id);
    }
  }

  return Success(result);
}

void Database::ComputeTaskTagRels(const Relations &local_rels,
                                  const Relations &remote_rels,
                                  const Relations &last_synch_rels) const {
  for (item::TaskTagRel rel : local_rels) {
    RelID id(rel.task_id, rel.tag_id);
    if (!tasks_.IfItemExists(rel.task_id) || !tags_.IfItemExists(rel.tag_id)) {
      task_tag_rel_.RemoveItem(id, false);
      CHECK_ERR(task_tag_rel_);
      continue;
    }
    if (remote_rels.contains(id)) {
      rel.if_main_tag = false;
      rel.if_main_tag_ts = qMax(rel.if_main_tag_ts,
                                remote_rels[id].if_main_tag_ts);
      task_tag_rel_.EditItem(rel, false);
      CHECK_ERR(task_tag_rel_);
    } else {
      if (last_synch_rels.contains(id)) {
        task_tag_rel_.RemoveItem(id, false);
        CHECK_ERR(task_tag_rel_);
      }
    }
  }

  for (item::TaskTagRel rel : remote_rels) {
    RelID id(rel.task_id, rel.tag_id);
    if (local_rels.contains(id)) continue;
    if (!tasks_.IfItemExists(rel.task_id) || !tags_.IfItemExists(rel.tag_id)) {
      continue;
    }
    if (!last_synch_rels.contains(id)) {
      rel.if_main_tag = false;
      task_tag_rel_.AddItem(rel, false, false);
      CHECK_ERR(task_tag_rel_);
    }
  }

  return Success();
}

void Database::SaveMainTags(const SynchMainTags &main_tags,
                            const Relations rels) const {
  SynchMainTags::const_iterator it = main_tags.begin();
  while (it != main_tags.end()) {
    RelID id(it.key(), it.value().first);
    if (!rels.contains(id)) continue;
    item::TaskTagRel rel = rels.value(id);
    rel.if_main_tag = true;
    rel.if_main_tag_ts = it.value().second;
    task_tag_rel_.EditItem(rel, false);
    CHECK_ERR(task_tag_rel_);
    ++it;
  }
  return Success();
}

void Database::SynchronizeTasks() {
  const QHash<ID, item::Task> remote_tasks = GetAllTasksHash(REMOTE);
  CHECK();
  const QHash<ID, item::Task> last_synch_tasks = GetAllTasksHash(LAST_SYNCH);
  CHECK();
  const QHash<ID, item::Task> tasks = GetAllTasksHash(LOCAL);
  CHECK();

  for (item::Task task : tasks) {
    if (remote_tasks.contains(task.id)) {
      if (!last_synch_tasks.contains(task.id)) {
        return Error(QString("Local and remote databases contain task but ")
                     + "last synch doesnt " + IDtoQString(task.id));
      }
      const item::Task remote_task = remote_tasks.value(task.id);
      const item::Task last_synch_task = last_synch_tasks.value(task.id);
      if (last_synch_task.completed_pomodoros
          > remote_task.completed_pomodoros) {
        return Error(QString("Last synch task has more completed pomodoros ")
                     + "then remote has " + IDtoQString(task.id));
      }
      bool previous_status = task.deleted;
      MergeTasksFields(task, remote_task, last_synch_task);
      if (previous_status) {
        deleted_tasks_.RemoveItem(task.id, false);
        CHECK_ERR(deleted_tasks_);
      } else {
        tasks_.RemoveItem(task.id, false);
        CHECK_ERR(tasks_);
      }
      if (task.deleted) {
        deleted_tasks_.AddItem(task, false, false);
        CHECK_ERR(deleted_tasks_);
      } else {
        tasks_.AddItem(task, false, false);
        CHECK_ERR(tasks_);
      }
    } else {
      if (last_synch_tasks.contains(task.id)) {
        if (task.deleted) {
          deleted_tasks_.RemoveItem(task.id, false);
          CHECK_ERR(deleted_tasks_);
        } else {
          tasks_.RemoveItem(task.id, false);
          CHECK_ERR(tasks_);
        }
        task_tag_rel_.RemoveTask(task.id);
        CHECK_ERR(task_tag_rel_);
      }
    }
  }

  for (item::Task remote_task : remote_tasks) {
    if (tasks.contains(remote_task.id)) continue;
    if (!last_synch_tasks.contains(remote_task.id)) {
      if (remote_task.deleted) {
        deleted_tasks_.AddItem(remote_task, false, false);
        CHECK_ERR(deleted_tasks_);
      } else {
        tasks_.AddItem(remote_task, false, false);
        CHECK_ERR(tasks_);
      }
    }
  }

  tasks_.WriteXML();
  CHECK_ERR(tasks_);
  deleted_tasks_.WriteXML();
  CHECK_ERR(deleted_tasks_);
  return Success();
}

void Database::SynchronizeTags() {
  // Remote tags
  TagData remote_data(REMOTE_DATABASE_FILES + TAGS_FILE);
  const QHash<ID, item::Tag> remote_tags
      = GetRemoteOrLastSynchItems(remote_data);
  CHECK();
  // Last synch tags
  TagData last_synch_data(LAST_SYNCH_DATABASE_FILES + TAGS_FILE);
  const QHash<ID, item::Tag> last_synch_tags
      = GetRemoteOrLastSynchItems(last_synch_data);
  CHECK();
  // Local tags
  const QHash<ID, item::Tag> tags = tags_.ItemsHash();
  CHECK_ERR(tasks_);

  for (item::Tag tag : tags) {
    if (remote_tags.contains(tag.id)) {
      const item::Tag remote_tag = remote_tags.value(tag.id);
      if (remote_tag.name_ts > tag.name_ts) {
        tag.name = remote_tag.name;
        tag.name_ts = remote_tag.name_ts;
      }
      if (remote_tag.color_ts > tag.color_ts) {
        tag.color = remote_tag.color;
        tag.color_ts = remote_tag.color_ts;
      }
      tags_.EditItem(tag, false);
      CHECK_ERR(tags_);
    } else {
      if (last_synch_tags.contains(tag.id)) {
        tags_.RemoveItem(tag.id, false);
        CHECK_ERR(tags_);
        task_tag_rel_.RemoveTag(tag.id);
        CHECK_ERR(task_tag_rel_);
      }
    }
  }

  for (item::Tag remote_tag : remote_tags) {
    if (tags.contains(remote_tag.id)) continue;
    if (!last_synch_tags.contains(remote_tag.id)) {
      tags_.AddItem(remote_tag, false, false);
      CHECK_ERR(tags_);
    }
  }

  tags_.WriteXML();
  CHECK_ERR(tags_);
  return Success();
}

void Database::SynchronizeTaskTagRel() {
  // Remote relations
  TaskTagRelData remote_data(REMOTE_DATABASE_FILES + TASK_TAG_REL_FILE);
  const Relations remote_rels = GetRemoteOrLastSynchItems(remote_data);
  CHECK();

  // Last synch relations
  TaskTagRelData last_synch_data(LAST_SYNCH_DATABASE_FILES + TASK_TAG_REL_FILE);
  const Relations last_synch_rels = GetRemoteOrLastSynchItems(last_synch_data);
  CHECK();

  // Local relations
  const Relations rels = task_tag_rel_.ItemsHash();
  CHECK_ERR(task_tag_rel_);

  // task_id --> (tag_id, if_main_tag_ts)
  SynchMainTags main_tags = ComputeMainTags(rels, remote_rels, last_synch_rels);
  CHECK();

  ComputeTaskTagRels(rels, remote_rels, last_synch_rels);
  CHECK();

  const Relations new_rels = task_tag_rel_.ItemsHash();
  CHECK_ERR(task_tag_rel_);
  SaveMainTags(main_tags, new_rels);
  CHECK();

  task_tag_rel_.WriteXML();
  CHECK_ERR(task_tag_rel_);
  return Success();
}

void Database::RemoveDeletedSelectedTasks(SelectedTaskData &last_synch_data,
                                          SelectedTaskData &remote_data) {
  for (const item::SelectedTask &selected_item : selected_tasks_.ItemsList()) {
    if (last_synch_data.IfItemExists(selected_item.id)
        && !remote_data.IfItemExists(selected_item.id)) {
      selected_tasks_.RemoveItem(selected_item.id, false);
      CHECK_ERR(selected_tasks_);
    }
  }
  selected_tasks_.WriteXML();
  CHECK_ERR(selected_tasks_);

  for (const item::SelectedTask &selected_item : remote_data.ItemsList()) {
    if (!tasks_.IfItemExists(selected_item.task_id)
        || (last_synch_data.IfItemExists(selected_item.id)
            && !selected_tasks_.IfItemExists(selected_item.id))) {
      remote_data.RemoveItem(selected_item.id, false);
      CHECK_ERR(remote_data);
    }
  }
  remote_data.WriteXML();
  CHECK_ERR(remote_data);

  return Success();
}

QList<Database::Node> Database::LoadSelectedTasksGraph(
    const SelectedTaskData &remote_data) const {
  QHash<ID, item::SelectedTask> remote = remote_data.ItemsHash();
  CHECK_ERR(remote_data, QList<Node>());
  QHash<ID, item::SelectedTask> local = selected_tasks_.ItemsHash();
  CHECK_ERR(selected_tasks_, QList<Node>());

  QList<ID> node_ids
      = QList<ID>::fromSet((local.keys() + remote.keys()).toSet());
  QList<Node> result;

  for (int i = 0; i < node_ids.size(); i++) {
    Node node(i, node_ids[i]);
    QList<Relation> neighbours;
    for (int j = 0; j < node_ids.size(); j++) {
      if (i == j) {
        neighbours.append(qMakePair(LACK, NO_TS));
        continue;
      }
      Order local_order = LACK;
      qint64 local_ts = NO_TS;
      if (local.contains(node_ids[i]) && local.contains(node_ids[j])) {
        if (local[node_ids[i]].order_position
            < local[node_ids[j]].order_position) {
          local_order = LT;
        } else {
          local_order = GT;
        }
        local_ts = selected_tasks_.GetRelTS(node_ids[i], node_ids[j]);
        CHECK_ERR(selected_tasks_, QList<Node>());
      }
      Order remote_order = LACK;
      qint64 remote_ts = NO_TS;
      if (remote.contains(node_ids[i]) && remote.contains(node_ids[j])) {
        if (remote[node_ids[i]].order_position
            < remote[node_ids[j]].order_position) {
          remote_order = LT;
        } else {
          remote_order = GT;
        }
        remote_ts = remote_data.GetRelTS(node_ids[i], node_ids[j]);
        CHECK_ERR(remote_data, QList<Node>());
      }
      if (local_order == LACK && remote_order == LACK) {
        neighbours.append(qMakePair(LACK, NO_TS));
      } else if (local_order == LACK) {
        neighbours.append(qMakePair(remote_order, remote_ts));
      } else if (remote_order == LACK) {
        neighbours.append(qMakePair(local_order, local_ts));
      } else if (local_ts < remote_ts) {
        neighbours.append(qMakePair(remote_order, remote_ts));
      } else {
        neighbours.append(qMakePair(local_order, local_ts));
      }
    }
    node.SetNeighbours(neighbours);
    result.append(node);
  }

  return Success(result);
}

QList<int> Database::ComputeOrder(QList<Database::Node> &nodes) const {
  // topsort with removing cycles
  QVector<bool> on_stack(nodes.size(), false);
  QVector<bool> ended_up(nodes.size(), false);
  QStack<int> dfs_stack;
  QList<int> result;
  result.reserve(nodes.size());
  // dfs for entire graph
  for (int i = 0; i < ended_up.size(); i++) {
    if (ended_up[i]) continue;
    dfs_stack.push(i);
    on_stack[i] = true;
    // start dfs from node i
    while (!dfs_stack.empty()) {
      const int current_node = dfs_stack.top();
      int neighbour;
      if (nodes[current_node].GetNextNeighbour(neighbour)) {
        if (ended_up[neighbour]) continue;
        // current node has at least one not visited neighbour node
        if (on_stack[neighbour]) {
          // cycle detected, we have to remove it
          int retrieved_node = dfs_stack.pop(); // current_node
          on_stack[retrieved_node] = false;
          if (retrieved_node == neighbour) {
            return Error(QString("Internal database error: loop edge in graph ")
                         + "(ComputeOrder function - topsort)", QList<int>());
          }
          qint64 min_ts = nodes[retrieved_node].NeighbourTS(neighbour);
          QPair<int, int> min_ts_edge(retrieved_node, neighbour);
          QStack<int> cycle_nodes;
          cycle_nodes.push(retrieved_node);
          while (retrieved_node != neighbour) {
            const int last_retrieved_node = retrieved_node;
            retrieved_node = dfs_stack.pop();
            on_stack[retrieved_node] = false;
            cycle_nodes.push(retrieved_node);
            qint64 edge_ts =
                nodes[retrieved_node].NeighbourTS(last_retrieved_node);
            if (edge_ts < min_ts) {
              min_ts = edge_ts;
              min_ts_edge = qMakePair(retrieved_node, last_retrieved_node);
            }
          }
          nodes[min_ts_edge.first].RemoveNeighbour(min_ts_edge.second);
          nodes[min_ts_edge.second].RemoveNeighbour(min_ts_edge.first);
          do {
            retrieved_node = cycle_nodes.pop();
            dfs_stack.push(retrieved_node);
            on_stack[retrieved_node] = true;
          } while (retrieved_node != min_ts_edge.first);
        } else {
          if (ended_up[neighbour]) continue;
          dfs_stack.push(neighbour);
          on_stack[neighbour] = true;
        }
      } else {
        // we ended up visiting this node
        dfs_stack.pop();
        ended_up[current_node] = true;
        result.prepend(current_node);
      }
    }
  }
  return Success(result);
}

QHash<ID, item::SelectedTask> Database::MergeSelectedTasks(
    const SelectedTaskData &remote_data,
    const SelectedTaskData &last_synch_data,
    const QList<Database::Node> &nodes) const {
  QHash<ID, item::SelectedTask> selected_list;
  for (const Node &node : nodes) {
    ID id = node.GetSelectedId();
    item::SelectedTask selected_task;
    if (selected_tasks_.IfItemExists(id)) {
      selected_task = selected_tasks_.GetItem(id);
      CHECK_ERR(selected_tasks_, QHash<ID, item::SelectedTask>());
      if (remote_data.IfItemExists(id)) {
        item::SelectedTask remote_selected_task = remote_data.GetItem(id);
        CHECK_ERR(remote_data, QHash<ID, item::SelectedTask>());
        if (remote_selected_task.assigned_pomodoros_ts
            > selected_task.assigned_pomodoros_ts) {
          selected_task.assigned_pomodoros
              = remote_selected_task.assigned_pomodoros;
          selected_task.assigned_pomodoros_ts
              = remote_selected_task.assigned_pomodoros_ts;
        }
        qint32 last_synch_completed
            = last_synch_data.GetItem(id).completed_pomodoros;
        CHECK_ERR(last_synch_data, QHash<ID, item::SelectedTask>());
        if (last_synch_completed > remote_selected_task.completed_pomodoros) {
          return Error(QString("Last synch task has more completed pomodoros ")
                       + "then remote has, id: " + IDtoQString(id)
                       + " (Selected tasks list)",
                       QHash<ID, item::SelectedTask>());
        }
        selected_task.completed_pomodoros
            += remote_selected_task.completed_pomodoros - last_synch_completed;
      }
    } else {
      selected_task = remote_data.GetItem(id);
      CHECK_ERR(remote_data, QHash<ID, item::SelectedTask>());
    }
    selected_list.insert(id, selected_task);
  }
  return Success(selected_list);
}

void Database::SaveNewOrderedSelectedTasks(
    const QList<int> &order, const QList<Database::Node> &nodes,
    const QHash<ID, item::SelectedTask> new_selected_tasks) {
  qint64 ts = QDateTime::currentMSecsSinceEpoch();
  QList<item::SelectedTask> new_selected_tasks_list;
  QList<item::SelectedOrder> new_selected_orders;
  new_selected_tasks_list.reserve(order.size());
  new_selected_orders.reserve(order.size() * (order.size() - 1) / 2);
  for (const int order_id : order) {
    if (order_id < 0 || order_id >= nodes.size()) {
      return Error(QString("Error while saving selected tasks"));
    }
    const ID item_id = nodes[order_id].GetSelectedId();
    if (!new_selected_tasks.contains(item_id)) {
      return Error(QString("Error while saving selected tasks"));
    }
    new_selected_tasks_list.append(new_selected_tasks[item_id]);
    QList<Relation> neighbours = nodes[order_id].GetNeighbours();
    for (int i = 0; i < order_id; i++) {
      const ID item2_id = nodes[i].GetSelectedId();
      item::SelectedOrder order_item = {
        qMin(item_id, item2_id),
        qMax(item_id, item2_id),
        neighbours[i].first == LACK ? ts : neighbours[i].second
      };
      new_selected_orders.append(order_item);
    }
  }

  selected_tasks_.ClearAndLoadNewData(new_selected_tasks_list,
                                      new_selected_orders);
  CHECK_ERR(selected_tasks_);
  return Success();
}

void Database::SynchronizeSelectedTasks() {
  SelectedTaskData last_synch_data(
      LAST_SYNCH_DATABASE_FILES + SELECTED_TASKS_FILE,
      LAST_SYNCH_DATABASE_FILES + SELECTED_TASKS_ORDER_FILE);
  last_synch_data.Init();
  CHECK_ERR(last_synch_data);
  SelectedTaskData remote_data(
      REMOTE_DATABASE_FILES + SELECTED_TASKS_FILE,
      REMOTE_DATABASE_FILES + SELECTED_TASKS_ORDER_FILE);
  remote_data.Init();
  CHECK_ERR(remote_data);
  RemoveDeletedSelectedTasks(last_synch_data, remote_data);
  CHECK();
  QList<Node> loaded_data = LoadSelectedTasksGraph(remote_data);
  CHECK();
  const QList<int> new_order = ComputeOrder(loaded_data);
  CHECK();
  const QHash<ID, item::SelectedTask> new_selected_tasks
      = MergeSelectedTasks(remote_data, last_synch_data, loaded_data);
  CHECK();
  SaveNewOrderedSelectedTasks(new_order, loaded_data, new_selected_tasks);
  CHECK();
  return Success();
}

void Database::Error(const QString &error_desc) const {
  last_operation_succeeded_ = false;
  last_error_description_ = error_desc;
  //qDebug() << "Database Error: " << last_error_description_;
}

void Database::Success() const {
  last_operation_succeeded_ = true;
}

Tag Database::TagItemToTag(const item::Tag &tag) {
  Tag result_tag = {
    tag.id,
    tag.name,
    tag.color
  };
  return result_tag;
}

SelectedTask Database::SelectedTaskItemToSelectedTask(
    const item::SelectedTask &selected_task) {
  SelectedTask result_selected_task = {
    selected_task.id,
    selected_task.task_id,
    selected_task.assigned_pomodoros,
    selected_task.completed_pomodoros
  };
  return result_selected_task;
}

QVariant Database::IDToQVariant(const ID &id) {
  QPoint id_point(id.first, id.second);
  return QVariant(id_point);
}

ID Database::QVariantToID(const QVariant &qv) {
  QPoint id_point = qv.toPoint();
  return ID(id_point.x(), id_point.y());
}

QString Database::IDtoQString(const ID &id) {
  return "(" + QString::number(id.first) +
      "," + QString::number(id.second) + ")";
}

bool Database::CompareTags(const Tag &tag1, const Tag &tag2) {
  return tag1.name < tag2.name;
}

Task::Priority Database::CalculateTaskPriority(const item::Task &task) {
  QDateTime deadline = QDateTime::fromMSecsSinceEpoch(task.deadline);
  int days_to_deadline = QDateTime::currentDateTime().daysTo(deadline);
  qint32 pomodoros_left = task.assigned_pomodoros - task.completed_pomodoros;
  if (days_to_deadline < pomodoros_left) return Task::HIGH;
  if (days_to_deadline > 2 * pomodoros_left) return Task::LOW;
  return Task::NORMAL;
}

Database::Node::Node(const int id, const ID &selected_id)
    : id_(id), selected_id_(selected_id) {
  last_computed_neighbour_ = -1;
}

ID Database::Node::GetSelectedId() const {
  return selected_id_;
}

QList<Database::Relation> Database::Node::GetNeighbours() const {
  return neighbours_;
}

void Database::Node::SetNeighbours(
    const QList<Relation> &neighbours) {
  neighbours_ = neighbours;
}

bool Database::Node::RemoveNeighbour(const int id) {
  if (id < 0 || id >= neighbours_.size()) return false;
  if (neighbours_[id].first == LACK) return false;
  neighbours_[id].first = LACK;
  return true;
}

bool Database::Node::NeighbourTS(const int id) const {
  if (id < 0 || id >= neighbours_.size()) return false;
  if (neighbours_[id].first == LACK) return false;
  return neighbours_[id].second;
}

bool Database::Node::GetNextNeighbour(int &result_id) {
  if (last_computed_neighbour_ == neighbours_.size()) return false;
  last_computed_neighbour_++;
  while (last_computed_neighbour_ < neighbours_.size()) {
    if (neighbours_[last_computed_neighbour_].first != LT) {
      last_computed_neighbour_++;
      continue;
    }
    result_id = last_computed_neighbour_;
    return true;
  }
  return false;
}

} /* namespace db */

#undef CHECK
#undef CHECK_ERR
