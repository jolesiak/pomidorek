#ifndef STRUCTS_H
#define STRUCTS_H

#include <QString>
#include <QPair>

namespace db {

typedef qint32 DeviceId;
typedef qint64 ItemId;
typedef QPair<DeviceId,ItemId> ID;
// (task_id, tag_id)
typedef QPair<ID,ID> RelID;

// Tag, Task structures designed to communicate
struct Task {
  ID id;
  QString name;
  QString description;
  qint32 assigned_pomodoros;
  qint32 completed_pomodoros;
  qint64 deadline;

  enum State {
    COMPLETED = 0x1,
    DELETED = 0x2,
    SELECTED = 0x4,
    SESSION_COMPLETED = 0x8
  };
  Q_DECLARE_FLAGS(Status, State)
  Status status;

  enum Priority {
    LOW,
    NORMAL,
    HIGH
  } priority;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Task::Status)

struct Tag {
  ID id;
  QString name;
  QString color;
};

struct SelectedTask {
  ID id;
  ID task_id;
  qint32 assigned_pomodoros;
  qint32 completed_pomodoros;
};

// Consts that represent no value
const DeviceId NO_DEV_ID = -1;
const ItemId NO_ITEM_ID = -1;
const ID NO_ID(NO_DEV_ID, NO_ITEM_ID);
const qint64 NO_TS(0);
const qint64 NO_DEADLINE(NO_TS);
const Tag NO_TAG = {
  NO_ID,
  "",
  ""
};
const Task NO_TASK = {
  NO_ID,
  "",
  "",
  0,
  0,
  NO_DEADLINE,
  Task::Status(),
  Task::NORMAL
};
const SelectedTask NO_SELECTED_TASK = {
  NO_ID,
  NO_ID,
  0,
  0
};

// Structures for Database and Data classes
namespace item {

struct Task {
  ID id;
  QString name;
  qint64 name_ts;
  QString description;
  qint64 description_ts;
  qint32 assigned_pomodoros;
  qint64 assigned_pomodoros_ts;
  qint32 completed_pomodoros;
  qint64 deadline;
  qint64 deadline_ts;
  bool deleted;
};

struct Tag {
  ID id;
  QString name;
  qint64 name_ts;
  QString color;
  qint64 color_ts;
};

struct TaskTagRel {
  ID task_id;
  ID tag_id;
  bool if_main_tag;
  qint64 if_main_tag_ts;
};

struct SelectedTask {
  ID id;
  ID task_id;
  qint32 order_position;
  qint32 assigned_pomodoros;
  qint64 assigned_pomodoros_ts;
  qint32 completed_pomodoros;
};

// Order between selected tasks
struct SelectedOrder {
  ID task1_id;
  ID task2_id;
  qint64 ts;
};

} /* namespace item */

} /* namespace db */


#endif // STRUCTS_H
