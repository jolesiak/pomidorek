#include <QString>
#include <QProcess>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>

#include "file_operations.h"

bool DoArchiveOperation(const QString &path, const ArchiveOperation &op) {
  const int timeout = 120000; //ms
  if (op == DECOMPRESS) {
    if (!QFile::exists(path)) return false;
  }
  QProcess process;
  const QString dir = QFileInfo(path).dir().path();
  const QString file_name = QFileInfo(path).fileName();
  process.setWorkingDirectory(dir);
  switch (op) {
  case COMPRESS:
    process.start("zip -r " + file_name + " " + file_name);
    break;
  case DECOMPRESS:
    process.start("unzip " + file_name);
    break;
  }
  if (!process.waitForFinished(timeout)) return false;
  return true;
}

bool RemoveDir(QString path) {
  bool result = true;
  path = QFileInfo(path).absoluteFilePath();
  QDir dir(path);
  if (!dir.exists()) return true;
  for (const QFileInfo &info : dir.entryInfoList(QDir::NoDotAndDotDot
                                                 | QDir::System | QDir::Hidden
                                                 | QDir::AllDirs | QDir::Files,
                                                 QDir::DirsFirst)) {
    if (info.isDir()) {
      result = RemoveDir(info.absoluteFilePath());
    } else {
      result = QFile::remove(info.absoluteFilePath());
    }
    if (!result) return false;
  }
  return dir.rmdir(path);
}

bool CreateEmptyDir(const QString &path) {
  if (!RemoveDir(path)) return false;
  return QDir().mkpath(path);
}


bool RemoveFile(const QString &path) {
  if (!QFile::exists(path)) return true;
  if (!QFile::remove(path)) return false;
  return true;
}
