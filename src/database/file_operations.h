#ifndef FILE_OPERATIONS_H
#define FILE_OPERATIONS_H

#include <QString>

enum ArchiveOperation {
  COMPRESS,
  DECOMPRESS
};

bool DoArchiveOperation(const QString &path, const ArchiveOperation &op);
bool RemoveDir(QString path);
bool CreateEmptyDir(const QString &path);
bool RemoveFile(const QString &path);

#endif // FILE_OPERATIONS_H
