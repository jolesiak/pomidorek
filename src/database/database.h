#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QMutex>
#include <QList>
#include <QHash>
#include <QString>

#include <QDebug>

#include "structs.h"
#include "data.h"

namespace db {

class Database {
public:
  void Init();

  // Tasks getters
  Task TaskGet(const ID &id) const;
  Tag TaskMainTag(const ID &id) const;
  QList<Tag> TaskTags(const ID &id) const;
  QList<Task> TaskList() const;
  QList<Task> TaskActiveList() const;
  QList<Task> TaskCompletedList() const;
  QList<Task> TaskDeletedList() const;

  // Tasks setters
  void TaskAdd(Task &task, const QList<ID> &tags = QList<ID>(),
               const ID &main_tag_id = NO_ID);
  void TaskEdit(const Task &new_task);
  void TaskAddTag(const ID &task_id, const ID &tag_id);
  void TaskSetMainTag(const ID &task_id, const ID &main_tag_id);
  void TaskRemoveTag(const ID &task_id, const ID &tag_id);
  void TaskRemove(const ID &id);
  void TaskRestoreDeleted(const ID &id);
  void TaskRemoveDeleted(const ID &id);
  void TaskRemoveMany(const QList<ID> &ids);
  void TaskRemoveDeletedMany(const QList<ID> &ids);
  void TaskRestoreDeletedMany(const QList<ID> &ids);

  // Tags getters
  Tag TagGet(const ID &id) const;
  QList<Tag> TagList() const;
  QList<Task> TagTasks(const ID &id) const;

  // Tags setters
  void TagAdd(Tag &new_tag);
  void TagEdit(const Tag &tag);
  void TagRemove(const ID &id);

  // Selected tasks getters
  SelectedTask SelectedTaskGet(const ID &id) const;
  QList<SelectedTask> SelectedTaskList() const;
  int SelectedTaskFirstPos() const;
  int SelectedTaskLastPos() const;

  // Selected tasks setters
  void SelectedTaskAdd(SelectedTask &selected_task, const qint32 position);
  void SelectedTaskAddMany(QList<SelectedTask> &selected_tasks,
                           const qint32 first_item_position);
  void SelectedTaskEdit(const SelectedTask &selected_task);
  void SelectedTaskEditPosition(const ID &id, const qint32 new_pos);
  void SelectedTaskRemove(const ID &id);
  void SelectedTaskAddPomodoro(const ID &id);
  void SelectedTaskMoveUp(const ID &id);
  void SelectedTaskMoveDown(const ID &id);
  void SelectedTaskMoveDownBy(const ID &id, const qint32 modifier);
  void SelectedTaskRemoveMany(const QList<ID> &ids);

  void Synchronize();

  // Validation
  bool IsNotValid() const;
  QString LastErrorDescription() const;

  static QVariant IDToQVariant(const ID &id);
  static ID QVariantToID(const QVariant &qv);

private:
  mutable bool last_operation_succeeded_ = true;
  mutable QString last_error_description_;

  static QMutex mutex_;

  static TaskData tasks_;
  static DelTaskData deleted_tasks_;
  static TagData tags_;
  static TaskTagRelData task_tag_rel_;
  static SelectedTaskData selected_tasks_;
  static DeviceIdData device_id_;

  Task TaskItemToTask(const item::Task &task) const;
  template <class T, bool B>
  void RemoveMany(const QList<ID> &ids, Data<ID,T,B> &data);
  void SelectedTaskAddWithoutSaveAndRefresh(SelectedTask &selected_task,
                                            const qint32 position);

  // Synchronization (files)
  void FirstSynch();
  void ChangeDeviceIds(const DeviceId &new_device_id);
  void OpenRemoteDatabase() const;
  void OpenLastSynchDatabase() const;
  void SaveRemoteDatabase() const;
  void SaveLastSynchDatabase() const;
  // Synchronization (data)
  template <class I, class T, bool B>
  QHash<I,T> GetRemoteOrLastSynchItems(Data<I,T,B> &data) const;
  // Tasks
  enum Source { LOCAL, REMOTE, LAST_SYNCH };
  QHash<ID, item::Task> GetAllTasksHash(Source source) const;
  void MergeTasksFields(item::Task &local_task, const item::Task &remote_task,
                        const item::Task &last_synch_task) const;
  // Compute if task is deleted or not
  bool ComputeIfTaskDeleted(bool local_deleted, bool remote_deleted,
                            bool last_synch_deleted) const;
  void SynchronizeTasks();
  // Tags
  void SynchronizeTags();
  // TaskTagRels
  typedef QHash< RelID, item::TaskTagRel> Relations;
  typedef QHash< ID, QPair<ID, qint64> > SynchMainTags;
  SynchMainTags ComputeMainTags(const Relations &local_rels,
                                const Relations &remote_rels,
                                const Relations &last_synch_rels) const;
  void ComputeTaskTagRels(const Relations &local_rels,
                          const Relations &remote_rels,
                          const Relations &last_synch_rels) const;
  void SaveMainTags(const SynchMainTags &main_tags, const Relations rels) const;
  void SynchronizeTaskTagRel();
  // Selected tasks list
  enum Order { LACK, GT, LT };
  typedef QPair<Order, qint64> Relation;
  class Node {
  private:
    int id_;
    ID selected_id_;
    int last_computed_neighbour_;
    QList<Relation> neighbours_;
  public:
    Node(const int id, const ID &selected_id);
    ID GetSelectedId() const;
    QList<Relation> GetNeighbours() const;
    void SetNeighbours(const QList<Relation> &neighbours);
    bool RemoveNeighbour(const int id);
    bool NeighbourTS(const int id) const;
    bool GetNextNeighbour(int &result_id);
  };
  void RemoveDeletedSelectedTasks(SelectedTaskData &last_synch_data,
                                  SelectedTaskData &remote_data);
  QList<Node> LoadSelectedTasksGraph(const SelectedTaskData &remote_data) const;
  QList<int> ComputeOrder(QList<Node> &nodes) const;
  QHash<ID, item::SelectedTask> MergeSelectedTasks(
      const SelectedTaskData &remote_data,
      const SelectedTaskData &last_synch_data,
      const QList<Node> &nodes) const;
  void SaveNewOrderedSelectedTasks(const QList<int> &order,
      const QList<Node> &nodes,
      const QHash<ID, item::SelectedTask> new_selected_tasks);
  void SynchronizeSelectedTasks();

  void Error(const QString &error_desc) const;
  template <class T>
  T Error(const QString &error_desc, const T &result) const;

  void Success() const;
  template <class T>
  T Success(const T &result) const;

  static Tag TagItemToTag(const item::Tag &tag);
  static SelectedTask SelectedTaskItemToSelectedTask(
      const item::SelectedTask &selected_task);
  static QString IDtoQString(const ID &id);
  static bool CompareTags(const Tag &tag1, const Tag &tag2);
  static Task::Priority CalculateTaskPriority(const item::Task &task);
}; /* Database class */

template <class T, bool B>
void Database::RemoveMany(const QList<ID> &ids, Data<ID,T,B> &data) {
  for (const ID &id : ids) {
    data.RemoveItem(id, false);
    if (data.IsNotValid()) return Error(data.LastErrorDesc());
  }
  data.WriteXML();
  if (data.IsNotValid()) return Error(data.LastErrorDesc());
  return Success();
}

template <class I, class T, bool B>
QHash<I,T> Database::GetRemoteOrLastSynchItems(Data<I,T,B> &data) const {
  const QString ERROR_MSG = "Error while synchronizing, ";
  data.Init();
  if (data.IsNotValid())
    return Error(ERROR_MSG + data.LastErrorDesc(), QHash<I,T>());
  QHash<I,T> result = data.ItemsHash();
  if (data.IsNotValid())
    return Error(ERROR_MSG + data.LastErrorDesc(), QHash<I,T>());
  return Success(result);
}

template <class T>
inline T Database::Error(const QString &error_desc, const T &result) const {
  Error(error_desc);
  return result;
}

template <class T>
inline T Database::Success(const T &result) const {
  Success();
  return result;
}

/******************************************************************************/
/*                           DatabaseRefresher                                */
/******************************************************************************/
class DatabaseRefresher : public QObject {
  Q_OBJECT
public:
  static DatabaseRefresher& Instance() {
    static DatabaseRefresher instance;
    return instance;
  }
  static void Refresh() {
    emit Instance().RefreshData();
  }
private:
  DatabaseRefresher() {}
  Q_DISABLE_COPY(DatabaseRefresher)
signals:
  void RefreshData();
};

} /* namespace db */

#endif // DATABASE_H
