#ifndef DATA_H
#define DATA_H

#include <QString>
#include <QList>
#include <QSet>
#include <QHash>
#include <QMap>
#include <QMapIterator>
#include <QFile>
#include <QDateTime>
#include <QDomDocument>
#include <QTextStream>

#include "structs.h"

#define CHECK(args...) if (IsNotValid()) return args
#define CHECK_ERR(object, args...) if ((object).IsNotValid()) \
    return Error((object).LastErrorDesc(), ##args)
#define TEMP_CHECK(args...) if (this->IsNotValid()) return args

namespace db {

/******************************************************************************/
/*                         Data Class Header                                  */
/******************************************************************************/
// I - id type
// T - item type
// B - if generate next id for each new item
template <class I, class T, bool B>
class Data {
public:
  Data(const QString &file_path);
  virtual void Init(const DeviceId &device_id = NO_DEV_ID);
  virtual void AddItem(T &item, const bool generate_id = true,
                       const bool if_write_xml = true);
  virtual void RemoveItem(const I &id, const bool if_write_xml = true);
  virtual void EditItem(const T &item, const bool if_write_xml = true);
  virtual void ChangeDeviceId(const DeviceId &new_device_id);
  T GetItem(const I &id) const;
  QList<T> ItemsList() const;
  QHash<I,T> ItemsHash() const;
  virtual void WriteXML();

  bool IfItemExists(const I &id) const;
  void CheckIfItemExists(const I &id) const;
  bool IsNotValid() const;
  QString LastErrorDesc() const;
protected:
  struct STRUCT_XML_SETTINGS {
    const int INDENT;
    const QString ID_ATTR;
    const QString NEXT_ID_ATTR;
    const QString BOOL_TRUE;
    const QString BOOL_FALSE;
    const QString XML_ROOT;
    const DeviceId DEFAULT_DEVICE_ID;
    const ItemId FIRST_ITEM_ID;
  } const XML_SETTINGS = {2, "id", "next_id", "1", "0", "Items", -1, 1};

  struct STRUCT_XML_ERRORS {
    const QString CREATE;
    const QString APPEND;
    const QString OPEN;
    const QString PARSE;
    const QString REMOVE;
    const QString CONVERT;
  } const XML_ERRORS = {
    "XML: Create node failed",
    "XML: Append child failed",
    "XML: Cannot open a file",
    "XML: Cannot parse a file",
    "XML: Cannot remove a child",
    "XML: Cannot convert"
  };

  const bool GENERATE_NEXT_ID = B;

  QFile file_;
  QDomDocument xml_;
  QHash<I,T> items_;
  DeviceId device_id_;
  QString xml_item_tag_;

  mutable bool last_operation_succeeded_ = true;
  mutable QString last_error_description_;

  void Error(const QString &error_desc) const;
  template <class S>
  S Error(const QString &error_desc, const S &result) const;

  void Success() const;
  template <class S>
  S Success(const S &result) const;

  void ParseXML();
  void LoadXMLData();
  void AppendNode(QDomElement &parent, const QString &new_node_name,
                  const QString &new_node_value);
  QDomElement ElementById(const QString &id) const;
  ItemId NextId();

  virtual void GenerateItemId(T &item) = 0;
  virtual I GetItemId(const T &item) const = 0;
  virtual QString GenerateXMLId(const I &id) const = 0;
  virtual void LoadItemFromXML(const QDomElement &xml_item) = 0;
  virtual QDomElement ItemToNode(const T &item) = 0;
  virtual void EditItemDeviceId(T &item) = 0;
};

/******************************************************************************/
/*                      Task Data Class Header                                */
/******************************************************************************/
template <bool B>
class TaskAndDelTaskData : public Data<ID, item::Task, B> {
public:
  TaskAndDelTaskData(const QString &file_path);
private:
  struct STRUCT_XML_TASKS {
    const QString TAG_NAME;
    const QString DEVICE_ID;
    const QString ITEM_ID;
    const QString NAME;
    const QString NAME_TS;
    const QString DESC;
    const QString DESC_TS;
    const QString AP;
    const QString AP_TS;
    const QString CP;
    const QString DEADLINE;
    const QString DEADLINE_TS;
    const QString DELETED;
  } const XML_TASKS = {
    "Task",
    "Device_id",
    "Task_id",
    "Name",
    "Name_ts",
    "Description",
    "Description_ts",
    "Assigned_pomodoros",
    "Assigned_pomodoros_ts",
    "Completed_pomodoros",
    "Deadline",
    "Deadline_ts",
    "Deleted"
  };
  void GenerateItemId(item::Task &item);
  ID GetItemId(const item::Task &item) const;
  QString GenerateXMLId(const ID &id) const;
  void LoadItemFromXML(const QDomElement &xml_item);
  QDomElement ItemToNode(const item::Task &item);
  void EditItemDeviceId(item::Task &item);
};

typedef TaskAndDelTaskData<true> TaskData;
typedef TaskAndDelTaskData<false> DelTaskData;

/******************************************************************************/
/*                      Tag Data Class Header                                 */
/******************************************************************************/
class TagData : public Data<ID, item::Tag, true> {
public:
  TagData(const QString &file_path);
private:
  struct STRUCT_XML_TAGS {
    const QString TAG_NAME;
    const QString DEVICE_ID;
    const QString ITEM_ID;
    const QString NAME;
    const QString NAME_TS;
    const QString COLOR;
    const QString COLOR_TS;
  } const XML_TAGS = {
    "Tag",
    "Device_id",
    "Tag_id",
    "Name",
    "Name_ts",
    "Color",
    "Color_ts"
  };
  void GenerateItemId(item::Tag &item);
  ID GetItemId(const item::Tag &item) const;
  QString GenerateXMLId(const ID &id) const;
  void LoadItemFromXML(const QDomElement &xml_item);
  QDomElement ItemToNode(const item::Tag &item);
  void EditItemDeviceId(item::Tag &item);
};

/******************************************************************************/
/*                       TaskTagRel Data Class Header                         */
/******************************************************************************/
class TaskTagRelData : public Data<RelID, item::TaskTagRel, false> {
public:
  TaskTagRelData(const QString &file_path);
  void Init(const DeviceId &device_id = NO_DEV_ID);
  void AddItem(item::TaskTagRel &item, const bool generate_id = true,
               const bool if_write_xml = true);
  void RemoveItem(const RelID &id, const bool if_write_xml = true);
  QList<ID> TagsList(const ID &task_id) const;
  QList<ID> TasksList(const ID &tag_id) const;
  ID MainTag(const ID &task_id) const;
  void RemoveTask(const ID &task_id);
  void RemoveTag(const ID &tag_id);
  void SetMainTag(const ID &task_id, const ID &tag_id,
                  const bool if_write_xml = true);
private:
  struct STRUCT_XML_TASK_TAG_REL {
    const QString TAG_NAME;
    const QString TASK_DEVICE_ID;
    const QString TAKS_ITEM_ID;
    const QString TAG_DEVICE_ID;
    const QString TAG_ITEM_ID;
    const QString IF_MAIN_TAG;
    const QString TS;
  } const XML_TASK_TAG_REL = {
    "Task_tag_relation",
    "Task_device_id",
    "Task_item_id",
    "Tag_device_id",
    "Tag_item_id",
    "If_main_tag",
    "If_main_tag_ts"
  };
  QHash<ID,QSet<ID> > task_tags_;
  QHash<ID,QSet<ID> > tag_tasks_;
  QHash<ID,ID> task_main_tag_;

  void GenerateItemId(item::TaskTagRel &item);
  RelID GetItemId(const item::TaskTagRel &item) const;
  QString GenerateXMLId(const RelID &id) const;
  void LoadItemFromXML(const QDomElement &xml_item);
  QDomElement ItemToNode(const item::TaskTagRel &item);
  void EditItemDeviceId(item::TaskTagRel &item);
};

/******************************************************************************/
/*                     SelectedOrder Data Class Header                        */
/******************************************************************************/
class SelectedOrderData : public Data<RelID, item::SelectedOrder, false> {
public:
  SelectedOrderData(const QString &file_path);
  void EditOrderTSWithoutSave(const ID &id_1, const ID &id_2);
  void LoadNewOrders(QList<item::SelectedOrder> &new_orders);
private:
  struct STRUCT_XML_SELECTED_ORDER {
    const QString TAG_NAME;
    const QString TASK1_DEVICE_ID;
    const QString TAKS1_ITEM_ID;
    const QString TASK2_DEVICE_ID;
    const QString TAKS2_ITEM_ID;
    const QString TS;
  } const XML_SELECTED_ORDER = {
    "Selected_tasks_order",
    "Task1_device_id",
    "Task1_item_id",
    "Task2_device_id",
    "Task2_item_id",
    "TS"
  };
  void GenerateItemId(item::SelectedOrder &item);
  RelID GetItemId(const item::SelectedOrder &item) const;
  QString GenerateXMLId(const RelID &id) const;
  void LoadItemFromXML(const QDomElement &xml_item);
  QDomElement ItemToNode(const item::SelectedOrder &item);
  void EditItemDeviceId(item::SelectedOrder &item);
};

/******************************************************************************/
/*                      SelectedTask Data Class Header                        */
/******************************************************************************/
class SelectedTaskData : public Data<ID, item::SelectedTask, true> {
public:
  SelectedTaskData(const QString &file_path, const QString &order_path);
  void Init(const DeviceId &device_id = NO_DEV_ID);
  void AddItem(item::SelectedTask &item, qint32 position,
               const bool generate_id = true, const bool if_write_xml = true);
  void RemoveItem(const ID &id, const bool if_write_xml = true);
  // Dont use it to change position (use EditPosition instead)
  void EditItem(const item::SelectedTask &item, const bool if_write_xml = true);
  void ChangeDeviceId(const DeviceId &new_device_id);
  void EditPosition(const ID &id, int modifier);
  int FirstPos() const;
  int LastPos() const;
  QList<item::SelectedTask> OrderedList() const;
  void RemoveItemsConnectedWithTask(const ID &task_id);
  Task::Status TaskStatus(const ID &task_id) const;
  qint64 GetRelTS(const ID &id1, const ID &id2) const;
  void ClearAndLoadNewData(QList<item::SelectedTask> &selected_tasks,
                           QList<item::SelectedOrder> &orders);
  void WriteXML();
private:
  // Default interval between positions (when normalized)
  const qint32 INTERVAL_SIZE = 1000;
  struct STRUCT_XML_SELECTED_TASKS {
    const QString TAG_NAME;
    const QString DEVICE_ID;
    const QString ITEM_ID;
    const QString TASK_DEVICE_ID;
    const QString TASK_ITEM_ID;
    const QString ORDER_POSITION;
    const QString AP;
    const QString AP_TS;
    const QString CP;
  } const XML_SELECTED_TASKS = {
    "Selected_task",
    "Device_id",
    "Item_id",
    "Task_device_id",
    "Task_item_id",
    "Order_position",
    "Assigned_pomodoros",
    "Assigned_pomodoros_ts",
    "Completed_pomodoros"
  };
  QMap<qint32, ID> selected_;
  SelectedOrderData order_;

  qint32 GetItemPosition(const ID &id) const;
  qint32 GetOrderPosition(const qint32 real_position);
  void NormalizePositions();
  void NormalizePosition(qint32 &position) const;

  void GenerateItemId(item::SelectedTask &item);
  ID GetItemId(const item::SelectedTask &item) const;
  QString GenerateXMLId(const ID &id) const;
  void LoadItemFromXML(const QDomElement &xml_item);
  QDomElement ItemToNode(const item::SelectedTask &item);
  void EditItemDeviceId(item::SelectedTask &item);
};

/******************************************************************************/
/*                    DeviceID Data Class Header                              */
/******************************************************************************/
class DeviceIdData : public Data<DeviceId, DeviceId, false> {
public:
  DeviceIdData(const QString &file_path);
  DeviceId GetDeviceId() const;
  void IncreaseDeviceId();
private:
  struct STRUCT_XML_DEVICE_ID {
    const QString TAG_NAME;
    const QString DEVICE_ID;
  } const XML_DEVICE_ID = {
    "Device_id_item",
    "Device_id"
  };

  void GenerateItemId(DeviceId &item);
  DeviceId GetItemId(const DeviceId &item) const;
  QString GenerateXMLId(const DeviceId &id) const;
  void LoadItemFromXML(const QDomElement &xml_item);
  QDomElement ItemToNode(const DeviceId &item);
  void EditItemDeviceId(DeviceId &item);
};

/******************************************************************************/
/*                         Data Class implementation                          */
/******************************************************************************/

template <class I, class T, bool B>
Data<I,T,B>::Data(const QString &file_path)
  : file_(file_path) {}

template <class I, class T, bool B>
void Data<I,T,B>::Error(const QString &error_desc) const {
  last_error_description_ = error_desc;
  last_operation_succeeded_ = false;
}

template <class I, class T, bool B>
template <class S>
S Data<I,T,B>::Error(const QString &error_desc, const S &result) const {
  Error(error_desc);
  return result;
}

template <class I, class T, bool B>
void Data<I,T,B>::Success() const {
  last_operation_succeeded_ = true;
}

template <class I, class T, bool B>
template <class S>
S Data<I,T,B>::Success(const S &result) const {
  Success();
  return result;
}

template <class I, class T, bool B>
bool Data<I,T,B>::IsNotValid() const {
  return !last_operation_succeeded_;
}

template <class I, class T, bool B>
QString Data<I,T,B>::LastErrorDesc() const {
  return last_error_description_;
}

template <class I, class T, bool B>
void Data<I,T,B>::Init(const DeviceId &device_id) {
  device_id_ = device_id;
  ParseXML();
  CHECK();
  LoadXMLData();
  CHECK();
  return Success();
}

template <class I, class T, bool B>
void Data<I,T,B>::ParseXML() {
  if (!file_.exists()) {
    QDomElement xml_root = xml_.createElement(XML_SETTINGS.XML_ROOT);
    if (GENERATE_NEXT_ID) {
      xml_root.setAttribute(XML_SETTINGS.NEXT_ID_ATTR,
                            XML_SETTINGS.FIRST_ITEM_ID);
    }
    if (xml_root.isNull()) return Error(XML_ERRORS.CREATE);
    if (xml_.appendChild(xml_root).isNull()) return Error(XML_ERRORS.APPEND);
    return Success();
  }
  // File already exists
  if (!file_.open(QIODevice::ReadOnly)) return Error(XML_ERRORS.OPEN);
  QString error_msg;
  int error_line, error_column;
  if (!xml_.setContent(&file_, &error_msg, &error_line, &error_column)) {
    file_.close();
    return Error(XML_ERRORS.PARSE + "\nMessage: " + error_msg + "\nLine: "
                 + QString::number(error_line) + "\nColumn: "
                 + QString::number(error_column));
  }
  file_.close();
  return Success();
}

template <class I, class T, bool B>
void Data<I,T,B>::LoadXMLData() {
  QDomElement xml_root = xml_.documentElement();
  if (xml_root.isNull() || xml_root.tagName() != XML_SETTINGS.XML_ROOT)
    return Error(XML_ERRORS.PARSE);

  QDomElement node = xml_root.firstChildElement(xml_item_tag_);
  while (!node.isNull()) {
    LoadItemFromXML(node);
    CHECK();
    node = node.nextSiblingElement(xml_item_tag_);
  }

  return Success();
}

template <class I, class T, bool B>
T Data<I,T,B>::GetItem(const I &id) const {
  CheckIfItemExists(id);
  CHECK(T());
  return Success(items_[id]);
}

template <class I, class T, bool B>
bool Data<I,T,B>::IfItemExists(const I &id) const {
  return items_.contains(id);
}

template <class I, class T, bool B>
void Data<I,T,B>::CheckIfItemExists(const I &id) const {
  if (IfItemExists(id)) return Success();
  return Error("Id (" + GenerateXMLId(id) + ") doesnt exist ("
               + xml_item_tag_ + ")");
}

template <class I, class T, bool B>
void Data<I,T,B>::AddItem(T &item, const bool generate_id,
                          const bool if_write_xml) {
  if (generate_id) {
    GenerateItemId(item);
    CHECK();
  }
  QDomElement xml_item = ItemToNode(item);
  CHECK();
  if (xml_.documentElement().appendChild(xml_item).isNull())
    return Error(XML_ERRORS.APPEND);
  if (if_write_xml) {
    WriteXML();
    CHECK();
  }
  I item_id = GetItemId(item);
  CHECK();
  items_.insert(item_id, item);
  return Success();
}

template <class I, class T, bool B>
void Data<I,T,B>::WriteXML() {
  if (!file_.open(QIODevice::WriteOnly))
    return Error(XML_ERRORS.OPEN);
  QTextStream file_stream(&file_);
  xml_.save(file_stream, XML_SETTINGS.INDENT);
  file_.close();
  return Success();
}

template <class I, class T, bool B>
void Data<I,T,B>::AppendNode(QDomElement &parent,
                             const QString &new_node_name,
                             const QString &new_node_value) {
  QDomElement new_node = xml_.createElement(new_node_name);
  if (new_node.isNull())
    return Error(XML_ERRORS.CREATE);

  QDomText text_node = xml_.createTextNode(new_node_value);
  if (text_node.isNull())
    return Error(XML_ERRORS.CREATE);

  if (new_node.appendChild(text_node).isNull())
    return Error(XML_ERRORS.APPEND);

  if (parent.appendChild(new_node).isNull())
    return Error(XML_ERRORS.APPEND);

  return Success();
}

template <class I, class T, bool B>
void Data<I,T,B>::RemoveItem(const I &id, const bool if_write_xml) {
  CheckIfItemExists(id);
  CHECK();
  QString xml_id = GenerateXMLId(id);
  CHECK();
  QDomElement removed_node = ElementById(xml_id);
  CHECK();
  if (xml_.documentElement().removeChild(removed_node).isNull())
    return Error(XML_ERRORS.REMOVE);
  if (if_write_xml) {
    WriteXML();
    CHECK();
  }
  items_.remove(id);
  return Success();
}

template <class I, class T, bool B>
QDomElement Data<I,T,B>::ElementById(const QString &id) const {
  QDomElement node = xml_.documentElement().firstChildElement();
  while (!node.isNull()) {
    if (node.attribute(XML_SETTINGS.ID_ATTR) == id)
      return Success(node);
    node = node.nextSiblingElement();
  }
  return Error(XML_ERRORS.REMOVE + "\nCannot find element, id = " + id,
               QDomElement());
}

template <class I, class T, bool B>
void Data<I,T,B>::EditItem(const T &item, const bool if_write_xml) {
  I id = GetItemId(item);
  CHECK();
  RemoveItem(id, false);
  CHECK();
  T item_copy = item;
  AddItem(item_copy, false, if_write_xml);
  CHECK();
  return Success();
}

template <class I, class T, bool B>
void Data<I,T,B>::ChangeDeviceId(const DeviceId &new_device_id) {
  device_id_ = new_device_id;
  QHash<I, T> items_copy = items_;
  for (T item : items_copy) {
    RemoveItem(GetItemId(item), false);
    CHECK();
    EditItemDeviceId(item);
    CHECK();
    AddItem(item, false);
    CHECK();
  }
  WriteXML();
  CHECK();
  return Success();
}

template <class I, class T, bool B>
ItemId Data<I,T,B>::NextId() {
  if (!GENERATE_NEXT_ID)
    return Error("LoadNextId called for class which doesnt support it", 0);
  QDomElement root = xml_.documentElement();
  if (root.isNull()) return Error(XML_ERRORS.PARSE, NO_ITEM_ID);
  bool ok;
  ItemId next_id = root.attribute(XML_SETTINGS.NEXT_ID_ATTR).toLongLong(&ok);
  if (!ok) return Error(XML_ERRORS.CONVERT + "\n" +
                        root.attribute(XML_SETTINGS.NEXT_ID_ATTR), NO_ITEM_ID);
  root.setAttribute(XML_SETTINGS.NEXT_ID_ATTR, next_id + 1);
  return Success(next_id);
}

template <class I, class T, bool B>
QList<T> Data<I,T,B>::ItemsList() const {
  return Success(items_.values());
}

template <class I, class T, bool B>
QHash<I,T> Data<I,T,B>::ItemsHash() const {
  return Success(items_);
}

/******************************************************************************/
/*                     Task Data Class implementation                         */
/******************************************************************************/
template <bool B>
TaskAndDelTaskData<B>::TaskAndDelTaskData(const QString &file_path)
    : Data<ID, item::Task, B>(file_path) {
  this->xml_item_tag_ = XML_TASKS.TAG_NAME;
}

template <bool B>
void TaskAndDelTaskData<B>::GenerateItemId(item::Task &item) {
  if (!this->GENERATE_NEXT_ID) return this->Success();
  ItemId item_id = this->NextId();
  TEMP_CHECK();
  item.id = ID(this->device_id_, item_id);
  return this->Success();
}

template <bool B>
ID TaskAndDelTaskData<B>::GetItemId(const item::Task &item) const {
  return this->Success(item.id);
}

template <bool B>
QString TaskAndDelTaskData<B>::GenerateXMLId(const ID &id) const {
  return this->Success(QString::number(id.first) + "_"
                       + QString::number(id.second));
}

template <bool B>
void TaskAndDelTaskData<B>::LoadItemFromXML(
    const QDomElement &xml_item) {
  QDomElement node = xml_item.firstChildElement();
  item::Task task;
  ID id;
  while (!node.isNull()) {
    const QString tag_name = node.tagName();
    bool ok = true;
    if (tag_name == XML_TASKS.DEVICE_ID)
      id.first = node.text().toLong(&ok);
    else if (tag_name == XML_TASKS.ITEM_ID)
      id.second = node.text().toLongLong(&ok);
    else if (tag_name == XML_TASKS.NAME)
      task.name = node.text();
    else if (tag_name == XML_TASKS.NAME_TS)
      task.name_ts = node.text().toLongLong(&ok);
    else if (tag_name == XML_TASKS.DESC)
      task.description = node.text();
    else if (tag_name == XML_TASKS.DESC_TS)
      task.description_ts = node.text().toLongLong(&ok);
    else if (tag_name == XML_TASKS.AP)
      task.assigned_pomodoros = node.text().toLong(&ok);
    else if (tag_name == XML_TASKS.AP_TS)
      task.assigned_pomodoros_ts = node.text().toLongLong(&ok);
    else if (tag_name == XML_TASKS.CP)
      task.completed_pomodoros = node.text().toLong(&ok);
    else if (tag_name == XML_TASKS.DEADLINE)
      task.deadline = node.text().toLongLong(&ok);
    else if (tag_name == XML_TASKS.DEADLINE_TS)
      task.deadline_ts = node.text().toLongLong(&ok);
    else if (tag_name == XML_TASKS.DELETED)
      task.deleted = node.text() == this->XML_SETTINGS.BOOL_TRUE ? true : false;
    else
      return this->Error(this->XML_ERRORS.PARSE
                         + "\nUnknown Task property tag: " + tag_name);

    if (!ok)
      return this->Error(this->XML_ERRORS.CONVERT + "\n" + node.text());
    node = node.nextSiblingElement();
  }
  task.id = id;
  this->items_.insert(id, task);
  return this->Success();
}

template <bool B>
QDomElement TaskAndDelTaskData<B>::ItemToNode(const item::Task &item) {
  QDomElement new_task = this->xml_.createElement(XML_TASKS.TAG_NAME);
  if (new_task.isNull()) return this->Error(this->XML_ERRORS.CREATE,
                                            QDomElement());
  QString xml_id = GenerateXMLId(item.id);
  TEMP_CHECK(QDomElement());
  new_task.setAttribute(this->XML_SETTINGS.ID_ATTR, xml_id);

  // Task id
  this->AppendNode(new_task, XML_TASKS.DEVICE_ID,
             QString::number(item.id.first));
  TEMP_CHECK(QDomElement());
  this->AppendNode(new_task, XML_TASKS.ITEM_ID,
             QString::number(item.id.second));
  TEMP_CHECK(QDomElement());
  // Name
  this->AppendNode(new_task, XML_TASKS.NAME,
             item.name);
  TEMP_CHECK(QDomElement());
  this->AppendNode(new_task, XML_TASKS.NAME_TS,
             QString::number(item.name_ts));
  TEMP_CHECK(QDomElement());
  // Description
  this->AppendNode(new_task, XML_TASKS.DESC,
             item.description);
  TEMP_CHECK(QDomElement());
  this->AppendNode(new_task, XML_TASKS.DESC_TS,
             QString::number(item.description_ts));
  TEMP_CHECK(QDomElement());
  // Number of assigned pomodoros
  this->AppendNode(new_task, XML_TASKS.AP,
             QString::number(item.assigned_pomodoros));
  TEMP_CHECK(QDomElement());
  this->AppendNode(new_task, XML_TASKS.AP_TS,
             QString::number(item.assigned_pomodoros_ts));
  TEMP_CHECK(QDomElement());
  // Number of completed pomodoros
  this->AppendNode(new_task, XML_TASKS.CP,
             QString::number(item.completed_pomodoros));
  TEMP_CHECK(QDomElement());
  // Deadline
  this->AppendNode(new_task, XML_TASKS.DEADLINE,
             QString::number(item.deadline));
  TEMP_CHECK(QDomElement());
  this->AppendNode(new_task, XML_TASKS.DEADLINE_TS,
             QString::number(item.deadline_ts));
  TEMP_CHECK(QDomElement());
  // If deleted
  this->AppendNode(new_task, XML_TASKS.DELETED,
             item.deleted ? this->XML_SETTINGS.BOOL_TRUE
                          : this->XML_SETTINGS.BOOL_FALSE);
  TEMP_CHECK(QDomElement());

  return this->Success(new_task);
}

template <bool B>
void TaskAndDelTaskData<B>::EditItemDeviceId(item::Task &item) {
  if (item.id.first == NO_DEV_ID) item.id.first = this->device_id_;
  return this->Success();
}

/******************************************************************************/
/*                      Tag Data Class implementation                         */
/******************************************************************************/
inline TagData::TagData(const QString &file_path)
    : Data<ID, item::Tag, true>(file_path) {
  xml_item_tag_ = XML_TAGS.TAG_NAME;
}

inline void TagData::GenerateItemId(item::Tag &item) {
  ItemId item_id = NextId();
  CHECK();
  item.id = ID(device_id_, item_id);
  return Success();
}

inline ID TagData::GetItemId(const item::Tag &item) const {
  return Success(item.id);
}

inline QString TagData::GenerateXMLId(const ID &id) const {
  return Success(QString::number(id.first) + "_"
                 + QString::number(id.second));
}

inline void TagData::LoadItemFromXML(const QDomElement &xml_item) {
  QDomElement node = xml_item.firstChildElement();
  item::Tag tag;
  ID id;
  while (!node.isNull()) {
    const QString tag_name = node.tagName();
    bool ok = true;
    if (tag_name == XML_TAGS.DEVICE_ID)
      id.first = node.text().toLong(&ok);
    else if (tag_name == XML_TAGS.ITEM_ID)
      id.second = node.text().toLongLong(&ok);
    else if (tag_name == XML_TAGS.NAME)
      tag.name = node.text();
    else if (tag_name == XML_TAGS.NAME_TS)
      tag.name_ts = node.text().toLongLong(&ok);
    else if (tag_name == XML_TAGS.COLOR)
      tag.color = node.text();
    else if (tag_name == XML_TAGS.COLOR_TS)
      tag.color_ts = node.text().toLongLong(&ok);
    else
      return Error(XML_ERRORS.PARSE + "\nUnknown Tag property tag: "
                   + tag_name);

    if (!ok)
      return Error(XML_ERRORS.PARSE + "\nCannot convert: " + node.text());
    node = node.nextSiblingElement();
  }
  tag.id = id;
  items_.insert(id, tag);
  return Success();
}

inline QDomElement TagData::ItemToNode(const item::Tag &item) {
  QDomElement new_tag = xml_.createElement(XML_TAGS.TAG_NAME);
  if (new_tag.isNull()) return Error(XML_ERRORS.CREATE, QDomElement());
  QString xml_id = GenerateXMLId(item.id);
  CHECK(QDomElement());
  new_tag.setAttribute(XML_SETTINGS.ID_ATTR, xml_id);

  AppendNode(new_tag, XML_TAGS.DEVICE_ID, QString::number(item.id.first));
  CHECK(QDomElement());
  AppendNode(new_tag, XML_TAGS.ITEM_ID, QString::number(item.id.second));
  CHECK(QDomElement());
  AppendNode(new_tag, XML_TAGS.NAME, item.name);
  CHECK(QDomElement());
  AppendNode(new_tag, XML_TAGS.NAME_TS, QString::number(item.name_ts));
  CHECK(QDomElement());
  AppendNode(new_tag, XML_TAGS.COLOR, item.color);
  CHECK(QDomElement());
  AppendNode(new_tag, XML_TAGS.COLOR_TS, QString::number(item.color_ts));
  CHECK(QDomElement());

  return Success(new_tag);
}

inline void TagData::EditItemDeviceId(item::Tag &item) {
  if (item.id.first == NO_DEV_ID) item.id.first = device_id_;
  return Success();
}

/******************************************************************************/
/*                  TaskTagRel Data Class implementation                      */
/******************************************************************************/
inline TaskTagRelData::TaskTagRelData(const QString &file_path)
    : Data<RelID, item::TaskTagRel, false>(file_path) {
  xml_item_tag_ = XML_TASK_TAG_REL.TAG_NAME;
}

inline void TaskTagRelData::Init(const DeviceId &device_id) {
  Data<RelID, item::TaskTagRel, false>::Init(device_id);
  CHECK();
  for (const item::TaskTagRel &item : items_) {
    task_tags_[item.task_id].insert(item.tag_id);
    tag_tasks_[item.tag_id].insert(item.task_id);
    if (item.if_main_tag) task_main_tag_[item.task_id] = item.tag_id;
  }
}

inline void TaskTagRelData::AddItem(item::TaskTagRel &item,
                                    const bool generate_id,
                                    const bool if_write_xml) {
  if (items_.contains(GetItemId(item))) return EditItem(item, if_write_xml);
  Data<RelID, item::TaskTagRel, false>::AddItem(item, generate_id,
                                                if_write_xml);
  task_tags_[item.task_id].insert(item.tag_id);
  tag_tasks_[item.tag_id].insert(item.task_id);
  if (item.if_main_tag) {
    if (task_main_tag_.contains(item.task_id))
      return Error(QString("Cannot add a main tag relation when task already ")
                   + "has a main tag. Use SetMainTag instead.");
    task_main_tag_[item.task_id] = item.tag_id;
  }
  return Success();
}

inline void TaskTagRelData::RemoveItem(const RelID &id,
                                       const bool if_write_xml) {
  Data<RelID, item::TaskTagRel, false>::RemoveItem(id, if_write_xml);
  CHECK();
  task_tags_[id.first].remove(id.second);
  tag_tasks_[id.second].remove(id.first);
  if (task_main_tag_.contains(id.first)) {
    if (task_main_tag_[id.first] == id.second)
      task_main_tag_.remove(id.first);
  }
  return Success();
}

inline void TaskTagRelData::EditItemDeviceId(item::TaskTagRel &item) {
  if (item.task_id.first == NO_DEV_ID) item.task_id.first = device_id_;
  if (item.tag_id.first == NO_DEV_ID) item.tag_id.first = device_id_;
  return Success();
}

inline QList<ID> TaskTagRelData::TagsList(const ID &task_id) const {
  return Success(task_tags_[task_id].toList());
}

inline QList<ID> TaskTagRelData::TasksList(const ID &tag_id) const {
  return Success(tag_tasks_[tag_id].toList());
}

inline ID TaskTagRelData::MainTag(const ID &task_id) const {
  if (task_main_tag_.contains(task_id))
    return Success(task_main_tag_[task_id]);
  return Success(NO_ID);
}

inline void TaskTagRelData::RemoveTask(const ID &task_id) {
  QList<ID> tags_to_remove = task_tags_[task_id].toList();
  for (const ID &tag_id : tags_to_remove) {
    RemoveItem(RelID(task_id, tag_id));
    CHECK();
  }
  return Success();
}

inline void TaskTagRelData::RemoveTag(const ID &tag_id) {
  QList<ID> tasks_to_remove = tag_tasks_[tag_id].toList();
  for (const ID &task_id : tasks_to_remove) {
    RemoveItem(RelID(task_id, tag_id));
    CHECK();
  }
  return Success();
}

inline void TaskTagRelData::SetMainTag(const ID &task_id, const ID &tag_id,
                                       const bool if_write_xml) {
  CheckIfItemExists(RelID(task_id, tag_id));
  if (IsNotValid()) return Error(QString("Before you may set tag as a main ")
                                 + "tag you have to add this tag");
  if (task_main_tag_.contains(task_id)) {
    if (task_main_tag_[task_id] == tag_id) return Success();
    item::TaskTagRel item = GetItem(RelID(task_id, task_main_tag_[task_id]));
    CHECK();
    item.if_main_tag = false;
    item.if_main_tag_ts = QDateTime::currentMSecsSinceEpoch();
    EditItem(item, false);
    CHECK();
    task_main_tag_.remove(task_id);
  }
  item::TaskTagRel item = {
    task_id,
    tag_id,
    true,
    QDateTime::currentMSecsSinceEpoch()
  };
  EditItem(item, if_write_xml);
  CHECK();
  return Success();
}

inline void TaskTagRelData::GenerateItemId(item::TaskTagRel &item) {
  Q_UNUSED(item);
  return Success();
}

inline RelID TaskTagRelData::GetItemId(const item::TaskTagRel &item) const {
  return Success(RelID(item.task_id, item.tag_id));
}

inline QString TaskTagRelData::GenerateXMLId(const RelID &id) const {
  return Success(QString::number(id.first.first) + "_"
                 + QString::number(id.first.second) + "_"
                 + QString::number(id.second.first) + "_"
                 + QString::number(id.second.second));
}

inline void TaskTagRelData::LoadItemFromXML(const QDomElement &xml_item) {
  QDomElement node = xml_item.firstChildElement();
  item::TaskTagRel rel;
  ID task_id, tag_id;
  while (!node.isNull()) {
    const QString tag_name = node.tagName();
    bool ok = true;
    // Task id
    if (tag_name == XML_TASK_TAG_REL.TASK_DEVICE_ID)
      task_id.first = node.text().toLong(&ok);
    else if (tag_name == XML_TASK_TAG_REL.TAKS_ITEM_ID)
      task_id.second = node.text().toLongLong(&ok);
    // Tag id
    else if (tag_name == XML_TASK_TAG_REL.TAG_DEVICE_ID)
      tag_id.first = node.text().toLong(&ok);
    else if (tag_name == XML_TASK_TAG_REL.TAG_ITEM_ID)
      tag_id.second = node.text().toLongLong(&ok);
    // If main tag
    else if (tag_name == XML_TASK_TAG_REL.IF_MAIN_TAG)
      rel.if_main_tag = node.text() == XML_SETTINGS.BOOL_TRUE ? true : false;
    // Timestamp
    else if (tag_name == XML_TASK_TAG_REL.TS)
      rel.if_main_tag_ts = node.text().toLongLong(&ok);
    else
      return Error(XML_ERRORS.PARSE + "\nUnknown TaskTagRel property tag: "
                   + tag_name);

    if (!ok)
      return Error(XML_ERRORS.PARSE + "\nCannot convert: " + node.text());
    node = node.nextSiblingElement();
  }
  rel.task_id = task_id;
  rel.tag_id = tag_id;
  items_.insert(RelID(task_id, tag_id), rel);
  return Success();
}

inline QDomElement TaskTagRelData::ItemToNode(const item::TaskTagRel &item) {
  QDomElement new_rel = xml_.createElement(XML_TASK_TAG_REL.TAG_NAME);
  if (new_rel.isNull()) return Error(XML_ERRORS.CREATE, QDomElement());
  QString xml_id = GenerateXMLId(RelID(item.task_id, item.tag_id));
  CHECK(QDomElement());
  new_rel.setAttribute(XML_SETTINGS.ID_ATTR, xml_id);

  // Task id
  AppendNode(new_rel, XML_TASK_TAG_REL.TASK_DEVICE_ID,
             QString::number(item.task_id.first));
  CHECK(QDomElement());
  AppendNode(new_rel, XML_TASK_TAG_REL.TAKS_ITEM_ID,
             QString::number(item.task_id.second));
  CHECK(QDomElement());
  // Tag id
  AppendNode(new_rel, XML_TASK_TAG_REL.TAG_DEVICE_ID,
             QString::number(item.tag_id.first));
  CHECK(QDomElement());
  AppendNode(new_rel, XML_TASK_TAG_REL.TAG_ITEM_ID,
             QString::number(item.tag_id.second));
  CHECK(QDomElement());
  // If main tag
  AppendNode(new_rel, XML_TASK_TAG_REL.IF_MAIN_TAG,
             item.if_main_tag ? XML_SETTINGS.BOOL_TRUE
                              : XML_SETTINGS.BOOL_FALSE);
  CHECK(QDomElement());
  // Timestamp
  AppendNode(new_rel, XML_TASK_TAG_REL.TS,
             QString::number(item.if_main_tag_ts));
  CHECK(QDomElement());

  return Success(new_rel);
}

/******************************************************************************/
/*                 SelectedOrder Data Class implementation                    */
/******************************************************************************/
inline SelectedOrderData::SelectedOrderData(const QString &file_path)
    : Data<RelID, item::SelectedOrder, false>(file_path) {
  xml_item_tag_ = XML_SELECTED_ORDER.TAG_NAME;
}

inline void SelectedOrderData::EditOrderTSWithoutSave(const ID &id_1,
                                                      const ID &id_2) {
  item::SelectedOrder order = {
    qMin(id_1, id_2),
    qMax(id_1, id_2),
    QDateTime::currentMSecsSinceEpoch()
  };
  EditItem(order, false);
  CHECK();
  return Success();
}

inline void SelectedOrderData::LoadNewOrders(
    QList<item::SelectedOrder> &new_orders) {
  for (item::SelectedOrder &item : new_orders) {
    AddItem(item, false, false);
    CHECK();
  }
  return Success();
}

inline void SelectedOrderData::GenerateItemId(item::SelectedOrder &item) {
  Q_UNUSED(item);
  return Success();
}

inline RelID SelectedOrderData::GetItemId(
    const item::SelectedOrder &item) const {
  return Success(RelID(item.task1_id, item.task2_id));
}

inline QString SelectedOrderData::GenerateXMLId(const RelID &id) const {
  return Success(QString::number(id.first.first) + "_"
                 + QString::number(id.first.second) + "_"
                 + QString::number(id.second.first) + "_"
                 + QString::number(id.second.second));
}

inline void SelectedOrderData::LoadItemFromXML(const QDomElement &xml_item) {
  QDomElement node = xml_item.firstChildElement();
  item::SelectedOrder rel;
  ID task1_id, task2_id;
  while (!node.isNull()) {
    const QString tag_name = node.tagName();
    bool ok = true;
    // Task1 id
    if (tag_name == XML_SELECTED_ORDER.TASK1_DEVICE_ID) {
      task1_id.first = node.text().toLong(&ok);
    } else if (tag_name == XML_SELECTED_ORDER.TAKS1_ITEM_ID) {
      task1_id.second = node.text().toLongLong(&ok);
    // Task2 id
    } else if (tag_name == XML_SELECTED_ORDER.TASK2_DEVICE_ID) {
      task2_id.first = node.text().toLong(&ok);
    } else if (tag_name == XML_SELECTED_ORDER.TAKS2_ITEM_ID) {
      task2_id.second = node.text().toLongLong(&ok);
    // Timestamp
    } else if (tag_name == XML_SELECTED_ORDER.TS) {
      rel.ts = node.text().toLongLong(&ok);
    } else {
      return Error(XML_ERRORS.PARSE + "\nUnknown SelectedOrder property tag: "
                   + tag_name);
    }

    if (!ok)
      return Error(XML_ERRORS.PARSE + "\nCannot convert: " + node.text());
    node = node.nextSiblingElement();
  }
  rel.task1_id = task1_id;
  rel.task2_id = task2_id;
  items_.insert(RelID(task1_id, task2_id), rel);
  return Success();
}

inline QDomElement SelectedOrderData::ItemToNode(
    const item::SelectedOrder &item) {
  QDomElement new_rel = xml_.createElement(XML_SELECTED_ORDER.TAG_NAME);
  if (new_rel.isNull()) return Error(XML_ERRORS.CREATE, QDomElement());
  QString xml_id = GenerateXMLId(RelID(item.task1_id, item.task2_id));
  CHECK(QDomElement());
  new_rel.setAttribute(XML_SETTINGS.ID_ATTR, xml_id);

  // Task1 id
  AppendNode(new_rel, XML_SELECTED_ORDER.TASK1_DEVICE_ID,
             QString::number(item.task1_id.first));
  CHECK(QDomElement());
  AppendNode(new_rel, XML_SELECTED_ORDER.TAKS1_ITEM_ID,
             QString::number(item.task1_id.second));
  CHECK(QDomElement());
  // Task2 id
  AppendNode(new_rel, XML_SELECTED_ORDER.TASK2_DEVICE_ID,
             QString::number(item.task2_id.first));
  CHECK(QDomElement());
  AppendNode(new_rel, XML_SELECTED_ORDER.TAKS2_ITEM_ID,
             QString::number(item.task2_id.second));
  CHECK(QDomElement());
  // Timestamp
  AppendNode(new_rel, XML_SELECTED_ORDER.TS, QString::number(item.ts));
  CHECK(QDomElement());

  return Success(new_rel);
}

inline void SelectedOrderData::EditItemDeviceId(item::SelectedOrder &item) {
  if (item.task1_id.first == NO_DEV_ID) item.task1_id.first = device_id_;
  if (item.task2_id.first == NO_DEV_ID) item.task2_id.first = device_id_;
  return Success();
}

/******************************************************************************/
/*                SelectedTask Data Class implementation                      */
/******************************************************************************/
inline SelectedTaskData::SelectedTaskData(const QString &file_path,
                                          const QString &order_path)
    : Data<ID, item::SelectedTask, true>(file_path),
      order_(order_path) {
  xml_item_tag_ = XML_SELECTED_TASKS.TAG_NAME;
}

inline void SelectedTaskData::Init(const DeviceId &device_id) {
  Data<ID, item::SelectedTask, true>::Init(device_id);
  CHECK();
  for (const item::SelectedTask &item : items_.values()) {
    selected_.insert(item.order_position, item.id);
  }
  order_.Init(device_id);
  CHECK_ERR(order_);
  return Success();
}

inline void SelectedTaskData::AddItem(item::SelectedTask &item,
                                      qint32 position,
                                      const bool generate_id,
                                      const bool if_write_xml) {
  NormalizePosition(position);
  item.order_position = GetOrderPosition(position);
  CHECK();
  Data<ID, item::SelectedTask, true>::AddItem(item, generate_id, false);
  CHECK();
  selected_.insert(item.order_position, item.id);
  // Add order items
  qint64 ts = QDateTime::currentMSecsSinceEpoch();
  for (const ID &id : selected_) {
    if (id != item.id) {
      item::SelectedOrder new_order = {
        qMin(id, item.id),
        qMax(id, item.id),
        ts
      };
      order_.AddItem(new_order, false, false);
      CHECK_ERR(order_);
    }
  }
  if (if_write_xml) {
    WriteXML();
    CHECK();
  }
  return Success();
}

inline void SelectedTaskData::RemoveItem(const ID &id,
                                         const bool if_write_xml) {
  CheckIfItemExists(id);
  CHECK();
  qint32 order_position = items_[id].order_position;
  Data<ID, item::SelectedTask, true>::RemoveItem(id, false);
  CHECK();
  selected_.remove(order_position);
  // Remove order items
  for (const ID &item_id : selected_) {
    order_.RemoveItem(RelID(qMin(item_id, id), qMax(item_id, id)), false);
    CHECK_ERR(order_);
  }
  if (if_write_xml) {
    WriteXML();
    CHECK();
  }
  return Success();
}

inline void SelectedTaskData::EditItem(const item::SelectedTask &item,
                                       const bool if_write_xml) {
  CheckIfItemExists(item.id);
  CHECK();
  Data<ID, item::SelectedTask, true>::RemoveItem(item.id, false);
  CHECK();
  item::SelectedTask item_copy = item;
  Data<ID, item::SelectedTask, true>::AddItem(item_copy, false, if_write_xml);
  CHECK();
  return Success();
}

inline void SelectedTaskData::ChangeDeviceId(const DeviceId &new_device_id) {
  order_.ChangeDeviceId(new_device_id);
  CHECK_ERR(order_);

  device_id_ = new_device_id;
  QHash<ID, item::SelectedTask> items_copy = items_;
  for (item::SelectedTask item : items_copy) {
    Data<ID, item::SelectedTask, true>::RemoveItem(item.id, false);
    CHECK();
    EditItemDeviceId(item);
    CHECK();
    Data<ID, item::SelectedTask, true>::AddItem(item, false, false);
    CHECK();
  }
  WriteXML();
  CHECK();
  return Success();
}

inline void SelectedTaskData::EditPosition(const ID &id, int modifier) {
  CheckIfItemExists(id);
  CHECK();
  qint32 new_pos = GetItemPosition(id);
  CHECK();
  if (new_pos == 0 && modifier <= 0) return Success();
  if (new_pos == selected_.size() - 1 && modifier >= 0) return Success();
  new_pos += modifier;
  NormalizePosition(new_pos);

  selected_.remove(items_[id].order_position);
  const qint32 new_order_pos = GetOrderPosition(new_pos);
  CHECK();
  selected_.insert(items_[id].order_position, id);
  QMapIterator<qint32, ID> it(selected_);
  it.findNext(id);

  if (it.key() < new_order_pos) {
    while (it.hasNext() && it.peekNext().key() < new_order_pos) {
      ID it_id = it.next().value();
      order_.EditOrderTSWithoutSave(id, it_id);
      CHECK_ERR(order_);
    }
  } else {
    it.previous();
    while (it.hasPrevious() && it.peekPrevious().key() > new_order_pos) {
      ID it_id = it.previous().value();
      order_.EditOrderTSWithoutSave(id, it_id);
      CHECK_ERR(order_);
    }
  }

  item::SelectedTask item = items_[id];
  qint32 previous_order_pos = item.order_position;
  item.order_position = new_order_pos;
  EditItem(item, false);
  CHECK();
  selected_.remove(previous_order_pos);
  selected_.insert(new_order_pos, id);

  WriteXML();
  CHECK();

  return Success();
}

inline int SelectedTaskData::FirstPos() const {
  return Success(0);
}

inline int SelectedTaskData::LastPos() const {
  return Success(selected_.size());
}

inline QList<item::SelectedTask> SelectedTaskData::OrderedList() const {
  QList<item::SelectedTask> result;
  result.reserve(selected_.size());
  for (const ID &id : selected_) {
    result.append(items_[id]);
  }
  return Success(result);
}

inline void SelectedTaskData::RemoveItemsConnectedWithTask(const ID &task_id) {
  QList<item::SelectedTask> selected_tasks_list = ItemsList();
  CHECK();
  for (const item::SelectedTask &selected_task : selected_tasks_list) {
    if (selected_task.task_id == task_id) {
      RemoveItem(selected_task.id, false);
      CHECK();
    }
  }
  WriteXML();
  CHECK();
  return Success();
}

inline Task::Status SelectedTaskData::TaskStatus(const ID &task_id) const {
  bool present = false;
  Task::Status result;
  for (const item::SelectedTask &selected_task : items_) {
    if (selected_task.task_id != task_id) continue;
    if (selected_task.completed_pomodoros < selected_task.assigned_pomodoros) {
      result |= Task::SELECTED;
      return Success(result);
    }
    present = true;
  }
  if (!present) return Success(result);
  result |= Task::SELECTED;
  result |= Task::SESSION_COMPLETED;
  return Success(result);
}

inline qint64 SelectedTaskData::GetRelTS(const ID &id1, const ID &id2) const {
  item::SelectedOrder item
      = order_.GetItem(RelID(qMin(id1, id2), qMax(id1, id2)));
  CHECK_ERR(order_, NO_TS);
  return Success(item.ts);
}

inline void SelectedTaskData::ClearAndLoadNewData(
    QList<item::SelectedTask> &selected_tasks,
    QList<item::SelectedOrder> &orders) {
  QList<ID> ids = items_.keys();
  for (const ID &id : ids) {
    RemoveItem(id, false);
    CHECK();
  }
  order_.LoadNewOrders(orders);
  CHECK_ERR(order_);
  int pos = 0;
  for (item::SelectedTask &selected_task : selected_tasks) {
    selected_task.order_position = pos;
    Data<ID, item::SelectedTask, true>::AddItem(selected_task, false, false);
    CHECK();
    selected_.insert(selected_task.order_position, selected_task.id);
    pos += INTERVAL_SIZE;
  }
  WriteXML();
  CHECK();
  return Success();
}

inline void SelectedTaskData::WriteXML() {
  order_.WriteXML();
  CHECK_ERR(order_);
  Data<ID, item::SelectedTask, true>::WriteXML();
  CHECK();
  return Success();
}

inline qint32 SelectedTaskData::GetItemPosition(const ID &id) const {
  qint32 result = 0;
  for (const ID &selected_id : selected_) {
    if (selected_id == id) break;
    result++;
  }
  return Success(result);
}

inline qint32 SelectedTaskData::GetOrderPosition(const qint32 real_position) {
  if (selected_.empty()) return Success(0);
  if (real_position < 0) {
    return Error("Not valid real_position (should be >= 0): "
                 + QString::number(real_position), 0);
  }
  if (real_position == 0)
    return Success(selected_.begin().key() - INTERVAL_SIZE);
  QMap<qint32, ID>::const_iterator it = selected_.begin();
  for (qint32 i = 1; i < real_position; i++) {
    if (it == selected_.end())
      return Error("Not valid real_position (out of bound): "
                   + QString::number(real_position), 0);
    ++it;
  }

  QMap<qint32, ID>::const_iterator next_it = it;
  ++next_it;
  const qint32 start = it.key();
  if (next_it == selected_.end()) return Success(start + INTERVAL_SIZE);
  const qint32 end = next_it.key();
  if (end - start < 2) {
    NormalizePositions();
    return GetOrderPosition(real_position);
  }
  return Success((start + end) / 2);
}

inline void SelectedTaskData::NormalizePositions() {
  qint32 current_pos = 0;
  QMap<qint32, ID> new_selected;
  QMap<qint32, ID>::const_iterator it;
  for (it = selected_.begin(); it != selected_.end(); ++it) {
    item::SelectedTask task = items_[it.value()];
    task.order_position = current_pos;
    EditItem(task, false);
    CHECK();
    new_selected.insert(current_pos, it.value());
    current_pos += INTERVAL_SIZE;
  }
  WriteXML();
  CHECK();
  selected_ = new_selected;
  return Success();
}

inline void SelectedTaskData::NormalizePosition(qint32 &position) const {
  if (position < 0) position = 0;
  if (position > items_.size()) position = items_.size();
}

inline void SelectedTaskData::GenerateItemId(item::SelectedTask &item) {
  ItemId item_id = NextId();
  CHECK();
  item.id = ID(device_id_, item_id);
  return Success();
}

inline ID SelectedTaskData::GetItemId(const item::SelectedTask &item) const {
  return Success(item.id);
}

inline QString SelectedTaskData::GenerateXMLId(const ID &id) const {
  return Success(QString::number(id.first) + "_"
                 + QString::number(id.second));
}

inline void SelectedTaskData::LoadItemFromXML(const QDomElement &xml_item) {
  QDomElement node = xml_item.firstChildElement();
  item::SelectedTask task;
  ID id, task_id;
  while (!node.isNull()) {
    const QString tag_name = node.tagName();
    bool ok = true;
    if (tag_name == XML_SELECTED_TASKS.DEVICE_ID)
      id.first = node.text().toLong(&ok);
    else if (tag_name == XML_SELECTED_TASKS.ITEM_ID)
      id.second = node.text().toLongLong(&ok);
    else if (tag_name == XML_SELECTED_TASKS.TASK_DEVICE_ID)
      task_id.first = node.text().toLong(&ok);
    else if (tag_name == XML_SELECTED_TASKS.TASK_ITEM_ID)
      task_id.second = node.text().toLongLong(&ok);
    else if (tag_name == XML_SELECTED_TASKS.ORDER_POSITION)
      task.order_position = node.text().toLong(&ok);
    else if (tag_name == XML_SELECTED_TASKS.AP)
      task.assigned_pomodoros = node.text().toLong(&ok);
    else if (tag_name == XML_SELECTED_TASKS.AP_TS)
      task.assigned_pomodoros_ts = node.text().toLongLong(&ok);
    else if (tag_name == XML_SELECTED_TASKS.CP)
      task.completed_pomodoros = node.text().toLong(&ok);
    else
      return Error(XML_ERRORS.PARSE + "\nUnknown SelectedTask property tag: "
                   + tag_name);

    if (!ok)
      return Error(XML_ERRORS.CONVERT + "\n" + node.text());
    node = node.nextSiblingElement();
  }
  task.id = id;
  task.task_id = task_id;
  items_.insert(id, task);
  return Success();
}

inline QDomElement SelectedTaskData::ItemToNode(
    const item::SelectedTask &item) {
  QDomElement new_task = xml_.createElement(XML_SELECTED_TASKS.TAG_NAME);
  if (new_task.isNull()) return Error(XML_ERRORS.CREATE, QDomElement());
  QString xml_id = GenerateXMLId(item.id);
  CHECK(QDomElement());
  new_task.setAttribute(XML_SETTINGS.ID_ATTR, xml_id);

  // Selected task id
  AppendNode(new_task, XML_SELECTED_TASKS.DEVICE_ID,
             QString::number(item.id.first));
  CHECK(QDomElement());
  AppendNode(new_task, XML_SELECTED_TASKS.ITEM_ID,
             QString::number(item.id.second));
  CHECK(QDomElement());
  // Task id
  AppendNode(new_task, XML_SELECTED_TASKS.TASK_DEVICE_ID,
             QString::number(item.task_id.first));
  CHECK(QDomElement());
  AppendNode(new_task, XML_SELECTED_TASKS.TASK_ITEM_ID,
             QString::number(item.task_id.second));
  CHECK(QDomElement());
  // Position
  AppendNode(new_task, XML_SELECTED_TASKS.ORDER_POSITION,
             QString::number(item.order_position));
  CHECK(QDomElement());
  // Number of assigned pomodoros
  AppendNode(new_task, XML_SELECTED_TASKS.AP,
             QString::number(item.assigned_pomodoros));
  CHECK(QDomElement());
  AppendNode(new_task, XML_SELECTED_TASKS.AP_TS,
             QString::number(item.assigned_pomodoros_ts));
  CHECK(QDomElement());
  // Number of completed pomodoros
  AppendNode(new_task, XML_SELECTED_TASKS.CP,
             QString::number(item.completed_pomodoros));
  CHECK(QDomElement());

  return Success(new_task);
}

inline void SelectedTaskData::EditItemDeviceId(item::SelectedTask &item) {
  if (item.id.first == NO_DEV_ID) item.id.first = device_id_;
  if (item.task_id.first == NO_DEV_ID) item.task_id.first = device_id_;
  return Success();
}

/******************************************************************************/
/*                  DeviceID Data Class Implementation                        */
/******************************************************************************/
inline DeviceIdData::DeviceIdData(const QString &file_path)
    : Data<DeviceId, DeviceId, false>(file_path) {
  xml_item_tag_ = XML_DEVICE_ID.TAG_NAME;
}

inline DeviceId DeviceIdData::GetDeviceId() const {
  if (items_.size() == 0) return Success(NO_DEV_ID);
  if (items_.size() == 1) return Success(items_.begin().value());
  return Error("Too many device ids", NO_DEV_ID);
}

inline void DeviceIdData::IncreaseDeviceId() {
  if (items_.size() == 1) {
    DeviceId old_device_id = items_.begin().value();
    DeviceId new_device_id = old_device_id + 1;
    RemoveItem(old_device_id, false);
    CHECK();
    AddItem(new_device_id);
    CHECK();
    return Success();
  }
  if (items_.size() == 0) {
    return Error("Cannot increase device id (dont have any)");
  }
  return Error("Cannot increase device id (have more than 1 item)");
}

inline void DeviceIdData::GenerateItemId(DeviceId &item) {
  Q_UNUSED(item);
  return Success();
}

inline DeviceId DeviceIdData::GetItemId(const DeviceId &item) const {
  return Success(item);
}

inline QString DeviceIdData::GenerateXMLId(const DeviceId &id) const {
  return Success(QString::number(id));
}

inline void DeviceIdData::LoadItemFromXML(const QDomElement &xml_item) {
  QDomElement node = xml_item.firstChildElement();
  if (node.tagName() != XML_DEVICE_ID.DEVICE_ID) {
    return Error(XML_ERRORS.PARSE + "\nUnknown DeviceId property tag: "
                 + node.tagName());
  }
  bool ok;
  DeviceId dev_id = node.text().toLong(&ok);
  if (!ok) return Error(XML_ERRORS.PARSE + "\nCannot convert: " + node.text());
  items_.insert(dev_id, dev_id);
  return Success();
}

inline QDomElement DeviceIdData::ItemToNode(const DeviceId &item) {
  QDomElement dev_id_xml = xml_.createElement(XML_DEVICE_ID.TAG_NAME);
  if (dev_id_xml.isNull()) return Error(XML_ERRORS.CREATE, QDomElement());
  QString xml_id = GenerateXMLId(item);
  CHECK(QDomElement());
  dev_id_xml.setAttribute(XML_SETTINGS.ID_ATTR, xml_id);

  AppendNode(dev_id_xml, XML_DEVICE_ID.DEVICE_ID, QString::number(item));
  CHECK(QDomElement());

  return Success(dev_id_xml);
}

inline void DeviceIdData::EditItemDeviceId(DeviceId &item) {
  Q_UNUSED(item);
  return Success();
}

} /* namespace db */

#undef CHECK
#undef CHECK_ERR
#undef TEMP_CHECK

#endif // DATA_H
